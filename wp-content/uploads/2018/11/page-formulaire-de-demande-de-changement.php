<?php // Template Name: Page Formulaire de demande de changement?>
<?php
if($_POST){

// $userdata = compact($_POST['account_display_name'], $_POST['account_email'], $_POST['password_1'], $_POST['account_first_name'], $_POST['account_last_name'] );
// wp_insert_user( $userdata );

	if(!get_user_by_email($_POST['account_email'])){

		$account_first_name = $_POST['account_first_name'];
		$account_last_name = $_POST['account_last_name'];
		$account_email = $_POST['account_email'];
		$number_OMV = $_POST['number_OMV'];
		$Entreprise = $_POST['Entreprise'];
		$site_internet = $_POST['Site_internet'];
		$Brève_présentation = $_POST['Brève_présentation'];
		$Horaires_de_consultation = $_POST['Horaires_de_consultation'];
		$Pôles_d’intérêts = $_POST['Pôles_d’intérêts'];

		$headers= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";

		$message =  '<hr>
						<p>Une demande de changement pour la fiche /'.  $account_first_name.' '.  $account_last_name.' / '.$Entreprise. 'a été demandée.</p>
						<p><br /> </p>
						<p>Voici les informations transmises:</p>
						<p><br /> </p>
						<p>Prénom: '.$first_name.'</p>
						<p><br /> </p>
						<p>Nom: '.$last_name.'</p>
						<p><br /> </p>
						<p>Email: '.$account_email.'</p>
						<p>Numéro d’OMV: '.$number_OMV.'</p>
						<p>Entreprise: '.$Entreprise.'</p>
						<p>Site internet: '.$site_internet.'</p>
						<p>Brève présentation: '.$Brève_présentation.'</p>
						<p>Horaires de consultation: '.$Horaires_de_consultation.'</p>
						<p>Pôles d’intérêts: '.$Pôles_d’intérêts.'</p>
						<p>Ceci est un message automatique, merci de ne pas y répondre.</p>
						</hr>';

		//mail('upv@upv.be', "Demande de changement sur unefiche", $message,$headers);

	}
}

?>
<?php get_header();?>

<?php 
global $data;
$sidebar_show = true;

if( $data['en_sidebar'] == 'no' ) {
    $sidebar_show = false;    
}
else {  
    if(get_post_meta($post->ID, 'pyre_en_sidebar', true) == 'no'){      
        $sidebar_show = false;   
        
    }
}	
?>
<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>
<?php

if(isset($_GET['contact']) && intval($_GET['contact'])){
    $user_id = $_GET['contact'];
  //  echo '<pre>==> '.$user_id.'</pre>';
}

      
$info = get_teamleader_info($user_id);

?>
<div style="display: flex; align-items: center; justify-content: center; width: 100vw">
<div style="width: 40%; ">

<form class="woocommerce-EditAccountForm edit-account" action="" method="post" style="padding: 50px 0">

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php esc_html_e( 'Prénom', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text"  class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo $info['first_name']; ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php esc_html_e( 'Nom', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo $info['last_name']; ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Adresse email', 'woocommerce' ); ?>&nbsp;</label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo $info['email']; ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="number_OMV"><?php esc_html_e( 'Numéro d’OMV', 'woocommerce' ); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="number_OMV" id="number_OMV" autocomplete="off" value="<?php echo $_POST['number_OMV'];?>" />
	</p>
	<div class="clear"></div>

    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="Entreprise"><?php esc_html_e( 'Entreprise', 'woocommerce' ); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="Entreprise" id="Entreprise" autocomplete="off" value="<?php echo $_POST['Entreprise'];?>" />
	</p>
    <div class="clear"></div>

<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="Site_internet"><?php esc_html_e( 'Site internet', 'woocommerce' ); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="Site_internet" id="Site_internet" autocomplete="off" value="<?php echo $_POST['Site_internet'];?>" />
</p>
<div class="clear"></div>

<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="Brève_présentation"><?php esc_html_e( 'Brève présentation', 'woocommerce' ); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="Brève_présentation" id="Brève_présentation" autocomplete="off" value="<?php echo $_POST['Brève_présentation'];?>" />
</p>
<div class="clear"></div>

<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="Horaires_de_consultation"><?php esc_html_e( 'Horaires de consultation', 'woocommerce' ); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="Horaires_de_consultation" id="Horaires_de_consultation" autocomplete="off" value="<?php echo $_POST['Horaires_de_consultation'];?>" />
</p><div class="clear"></div>

<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="Pôles_d’intérêts"><?php esc_html_e( 'Pôles d’intérêts (veuillez séparer chaque intérêt par une virgule)', 'woocommerce' ); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="Pôles_d’intérêts" id="Pôles_d’intérêts" placeholder="Exp: Chats,Chiens,Nutrition" autocomplete="off" value="<?php echo $_POST['Pôles_d’intérêts'];?>" />
</p>


	<div style="width: 302px; margin: 0 auto">
   		 <?php echo do_shortcode('[bws_google_captcha]'); ?>
	</div>

	<p style="justify-content: center; display: flex;">	
		<!-- <button id="knopka" type="submit"  name="save_account_details" value="<?php esc_attr_e( 'Envoyer', 'woocommerce' ); ?>"><?php esc_html_e( 'Envoyer', 'woocommerce' ); ?></button> -->
        <input style="width:100%;
		justify-content: center; 
		display: flex;    
		border: 1px solid #ccc;
		background-color: #ffffff;
		border-color: #999999;
		color: #3d3d3d;
		font-size: 12px;
  		font-weight: 600;" type="submit" value="Envoyer" class="wpcf7-form-control wpcf7-submit wpcf7-form-submit-button">		
	</p>


</form>
    </div>
    </div>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" id="successful_send" style="margin-top: 20px; display: flex; justify-content: center; display: none">
		<span style="margin-top: 20px; display: flex; justify-content: center; ">Votre demande a bien été enregistrée. Après vérification, nous mettrons les changements en ligne.&nbsp;</span>
	</p>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" id="sending_failed" style=" text-align:justify; display: flex; justify-content: center; display: none">
		<span style="margin-top: 20px; display: flex; justify-content: center;  ">Erreur, veuillez réessayer plus tard.&nbsp;</span>
	</p>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>


       

	
<?php get_footer(); ?>



<?php
	if(isset($_POST['account_first_name']) && isset($_POST['account_last_name']) && isset($_POST['account_email'])){
		if($_POST['account_first_name']&&$_POST['account_last_name']&&$_POST['account_email']){
		?>
			<script>
				var successful = document.getElementById("successful_send");
				var failed = document.getElementById("sending_failed");
					console.log("111");
					if(successful)
					{
						successful.style.display = "block";
						failed.style.display = "none";
					}
			</script>
		<?php
		}
		else{
		?>
			<script>
				var successful = document.getElementById("successful_send");
				var failed = document.getElementById("sending_failed");
					console.log("222");
					if(successful)
					{
						successful.style.display = "none";
						failed.style.display = "block";
					}
			</script>
		<?php
		}
	}
	?>
<script>
document.addEventListener("DOMContentLoaded", function() {
  var chechTag = document.getElementById("password_2");
  if (chechTag) {
    document
      .getElementById("number_OMV")
      .addEventListener("change", function() {
		  var num_omv = document.getElementById("number_OMV").value;
		  if(num_omv<10)
		  {
			  console.log("1111");
		  }
		  else 
		  console.log("2222");
		  
        // var formname = document.getElementById("account_first_name").value;
        // var formsurname = document.getElementById("account_last_name").value;
        // var formemail = document.getElementById("account_email").value;
        // // console.log(formname);
        // // console.log(formsurname);
        // // console.log(formemail);
        // document.getElementById("forename-teamleader").value = formname;
        // document.getElementById("surname-teamleader").value = formsurname;
        // document.getElementById("email-teamleader").value = formemail;
        // $("#knopka").click(function() {
        //   $("#knopka-teamleader").trigger("click");
        // });
      });
  }
});
</script>


<?php
function get_teamleader_info($id){

global $wpdb;


$host = 'biglab.mysql.tools';  
$database = 'biglab_wpupv'; 
$user = 'biglab_wpupv'; 
$password = 'wlx4uggf'; 

// require_once 'connection.php'; 
$link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
//$query = "SELECT * FROM  'wp_teamleader_contacts'";
$result = mysqli_query($link, "SELECT * FROM  `wp_teamleader_contacts` WHERE `id` = '$id' ") or die("Ошибка " . mysqli_error($link)); 

if($result)
{
	$row = mysqli_fetch_row($result);
	echo "<tr>";
		for ($j = 0 ; $j <count($row)  ; ++$j) 
		{

		//echo $j ."<td>". ')  ' ."$row[$j] </td>";
	   // echo '<pre> </pre>';
		if($j == 2) {$first_name = $row[$j]; }
		if($j == 3) {$last_name = $row[$j]; }
		if($j == 4) {$email = $row[$j]; }
		if($j == 5) {$tel1 = $row[$j]; }
		if($j == 6) {$tel2 = $row[$j]; }
		if($j == 8) {$website = $row[$j]; }
		if($j == 9) {$street = $row[$j]; }
		if($j == 10) {$number = $row[$j]; }
		if($j == 11) {$zipcode = $row[$j]; }
		if($j == 12) {$city = $row[$j]; }
		if($j == 13) {$country = $row[$j]; }
		if($j == 20) {$omv_number = $row[$j]; }
		if($j == 21) {$bic = $row[$j]; }
		if($j == 34) {$member_type = $row[$j]; }
		// echo $first_name;
		}
	echo "</tr>";
}

$args = [
	'first_name' => $first_name,
	'last_name' => $last_name,
	'email' => $email,
	'tel1' => $tel1,
	'tel2' => $tel2,
	'website' => $website,
	'street' => $street,
	'number' => $number,
	'zipcode' => $zipcode,
	'city' => $city,
	'country' => $country,
	'omv_number' => $omv_number,
	'bic' => $bic,
	'member_type' => $member_type];

//     echo '<pre>==> '.$result['forename'].'</pre>';

mysqli_close($link);
return $args;

}?>

<script>
$(document).ready(function(){
	$(".wpcf7-form-submit-button").on("click", function (event) {
		event.preventDefault();
			var id  = $(this),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 1000);
		});
});
</script>