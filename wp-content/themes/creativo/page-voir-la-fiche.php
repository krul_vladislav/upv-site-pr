<?php // Template Name: Page Voir la fiche?>
<?php get_header();?>
<?php
defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); 



      
if(isset($_GET['contact']) && intval($_GET['contact'])){
    $user_id = $_GET['contact'];
  //  echo '<pre>==> '.$user_id.'</pre>';
}
$user_info            = get_userdata( $user_id );
require_once 'wp-includes/user.php';
$user_data = get_current_user_id(); 

?>
<div style="margin-left: 150px">
<?php
$info = get_teamleader_info($user_id, $user_data);
//print_r($info);
?>
</div>

<div style="display:flex; justify-content:center; width:100%">
<form class="woocommerce-EditAccountForm edit-account" action="" method="post" style="padding: 50px 0; width:50%">
<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" style="color:#64779e; text-align:center; font-weight:bold; font-size: 20px; margin-top: 20px">
		<span >Formulaire de demande de changement&nbsp;</span>
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" style=" text-align:justify">
		<span>Veuillez compléter le formulaire ci-dessous pour nous informer d’une modification à
apporter à votre fiche.&nbsp;</a></span>
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php esc_html_e( 'Prénom Nom', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo $info['first_name'] . '  ' . $info['last_name']; ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="address"><?php esc_html_e( 'Adress', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="address" id="address"  
        value="<?php echo $info['zipcode'] . ' ' . $info['country']  . ' ' . $info['city'] . ' ' . $info['street'] . ' ' . $info['number']; ?>" /> 
  
	</p>
    <?php 
        if(!empty($info['tel1']))
            { ?>
            <div class="clear"></div>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="telephone1"><?php esc_html_e( 'Un téléphone', 'woocommerce' ); ?>&nbsp;</label>
                    <input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--email input-text" name="telephone1" id="telephone1" autocomplete="number" value="<?php echo $info['tel1']; ?>" />
                </p>
            <?php }
    
        if(!empty($info['tel2']))
        { ?>
        <div class="clear"></div>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="telephone2"><?php esc_html_e( 'Un téléphone', 'woocommerce' ); ?>&nbsp;</label>
                <input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="telephone2" id="telephone2" autocomplete="number" value="<?php echo $info['tel2']; ?>" />
            </p>
        <?php }
    ?>
	<div class="clear"></div>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Adresse email', 'woocommerce' ); ?>&nbsp;</label>
		<input type="email" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo $info['email']; ?>" />
	</p>
	<div class="clear"></div>
    <p >
        <label ><?php esc_html_e( 'Infrastructure', 'woocommerce' ); ?>&nbsp;</label>
        <pre style="display:none"> </pre>
        <?php
    if($info['member_type']=='client'){?>
	    <img style="margin-top:-20px" src="http://www.word.bild.com/files/library_upv/logos-pictos/structures/cabinet.gif" alt="альтернативный текст">
    <?php }
    else if($info['member_type']=='company'){?>
	    <img style="margin-top:-20px" src="http://www.word.bild.com/files/library_upv/logos-pictos/structures/clinique.gif" alt="альтернативный текст">
    <?php } ?>
	</p>

	<div class="clear"></div>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="Entreprise"><?php esc_html_e( "Centre d'intérêts", 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly"  class="woocommerce-Input woocommerce-Input--email input-text" name="Entreprise" id="Entreprise" value="<?php ?>" />
	</p>
	<div class="clear"></div>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="site_internet"><?php esc_html_e( 'Site internet', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly"  class="woocommerce-Input woocommerce-Input--email input-text" name="site_internet" id="site_internet"  value="<?php echo $info['website']; ?>" />
	</p>
	<div class="clear"></div>


    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="brève_présentation"><?php esc_html_e( 'Brève présentation', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text"  readonly="readonly" class="woocommerce-Input woocommerce-Input--email input-text" name="brève_présentation" id="brève_présentation"  value="<?php  ?>" />
	</p>
	<div class="clear"></div>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="horaires_de_consultation"><?php esc_html_e( 'Horaires de consultation', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text"  readonly="readonly" class="woocommerce-Input woocommerce-Input--email input-text" name="horaires_de_consultation" id="horaires_de_consultation"  value="<?php ?>" />
	</p>

    <?php if($info['user_member_upv'] == 'company') { ?>
    	<p style="justify-content: center; display: flex;">	
             <a id="<?php echo $user_id ?>" href="http://word.bild.com/formulaire-de-demande-de-changement/" style="background-color: #44b6df"  class="wpcf7-form-control wpcf7-submit wpcf7-form-submit-button customIdClick" > Demander un changement</a>
            </p>
    <?php }
    else {} ?>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>
</div>
<?php get_footer(); 

function get_teamleader_info($id, $id_user){

    global $wpdb;

    $host = 'localhost';  
    $database = 'clevermint'; 
    $user = 'clevermint'; 
    $password = 'n%78Nr3o'; 

    // require_once 'connection.php'; 
    $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
    //$query = "SELECT * FROM  'wp_teamleader_contacts'";
    $result = mysqli_query($link, "SELECT * FROM  `wp_teamleader_contacts` WHERE `id` = '$id' ") or die("Ошибка " . mysqli_error($link)); 

    $result2 = mysqli_query($link, "SELECT * FROM  `wp_usermeta` WHERE `user_id` = '$id_user' AND `meta_key` = 'member_upv' ") or die("Ошибка " . mysqli_error($link)); 


   
    if($result)
    {
        $row = mysqli_fetch_row($result);
        echo "<tr>";
            for ($j = 0 ; $j <count($row)  ; ++$j) 
            {

            //echo $j ."<td>". ')  ' ."$row[$j] </td>";
           // echo '<pre> </pre>';
            if($j == 2) {$first_name = $row[$j]; }
            if($j == 3) {$last_name = $row[$j]; }
            if($j == 4) {$email = $row[$j]; }
            if($j == 5) {$tel1 = $row[$j]; }
            if($j == 6) {$tel2 = $row[$j]; }
            if($j == 8) {$website = $row[$j]; }
            if($j == 9) {$street = $row[$j]; }
            if($j == 10) {$number = $row[$j]; }
            if($j == 11) {$zipcode = $row[$j]; }
            if($j == 12) {$city = $row[$j]; }
            if($j == 13) {$country = $row[$j]; }
            if($j == 20) {$omv_number = $row[$j]; }
            if($j == 21) {$bic = $row[$j]; }
            if($j == 34) {$member_type = $row[$j]; }
            // echo $first_name;
            }
        echo "</tr>";
    }
    if($result2)
    {
        $row2 = mysqli_fetch_row($result2);
        for ($j = 0 ; $j <count($row2)  ; ++$j) 
            {
if($j == 3) { $memb_upv = $row2[$j]; }
            }
    }
    $args = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'email' => $email,
        'tel1' => $tel1,
        'tel2' => $tel2,
        'website' => $website,
        'street' => $street,
        'number' => $number,
        'zipcode' => $zipcode,
        'city' => $city,
        'country' => $country,
        'omv_number' => $omv_number,
        'bic' => $bic,
        'member_type' => $member_type,
        'user_member_upv' => $memb_upv];

//     echo '<pre>==> '.$result['forename'].'</pre>';

mysqli_close($link);
   return $args;

}?>

<script>
    jQuery(document).ready(function ($) {
        $(document).on('click','.customIdClick',function(e){
            e.preventDefault();
            
            url = $(this).attr('href')+'?contact='+$(this).attr("id");
            console.log(url);
            window.location.replace(url);
        });
    });
    </script>