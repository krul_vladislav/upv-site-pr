<?php // Template Name: Page registration  ?>

<?php

defined('ABSPATH') || exit;

do_action('upv_new_account_create_page');

global $upvNewAccontIsset;

?>

<?php get_header(); ?>

<?php

global $data;

$sidebar_show = true;

if($data['en_sidebar'] == 'no') {
    $sidebar_show = false;
}else{
    if(get_post_meta($post->ID, 'pyre_en_sidebar', true) == 'no'){
        $sidebar_show = false;
    }
}

?>

<div style="display: flex; align-items: center; justify-content: center; width: 100vw">
    <div style="width: 40%; ">
        <form class="woocommerce-EditAccountForm edit-account" id="form-submit" action="" method="post">
            
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" style="color:#64779e; text-align:center; font-weight:bold; font-size: 20px; margin-top: 20px">
                <span >Formulaire d’inscription&nbsp;</span>
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" style=" text-align:justify">
                <span>Veuillez compléter le formulaire ci-dessous pour vous inscrire sur le site et accéder à l’espace vétérinaire.
                    Une inscription n’est validée que si vous êtes enregistré dans notre base de données comme vétérinaire.
                    Pour devenir membre de l’UPV, merci de compléter<a href="word.bild.com/wp-content/uploads/2019/05/2019_formulaire%20adh%D0%92sion%20et%20tarifs%20UPV.pdf" style="text-decoration: underline"> notre formulaire d’adhésion.&nbsp;</a></span>
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                <label for="account_first_name"><?php esc_html_e('Prénom', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="account_first_name" id="account_first_name" autocomplete="off" value="<?php echo $upvNewAccontIsset['account_first_name']; ?>" />
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                <label for="account_last_name"><?php esc_html_e('Nom', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="account_last_name" id="account_last_name" autocomplete="off" value="<?php echo $upvNewAccontIsset['account_last_name']; ?>" />
            </p>
            <div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="account_email"><?php esc_html_e('Adresse email', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
                <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" required name="account_email" id="account_email" autocomplete="off" value="<?php echo $upvNewAccontIsset['account_email']; ?>" />
            </p>

            <fieldset>
                <legend><?php esc_html_e('', 'woocommerce'); ?></legend>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="password_1"><?php esc_html_e('Mot de passe', 'woocommerce'); ?><span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" required name="password_1" id="password_1" autocomplete="off" />
                </p>
                
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="password_2"><?php esc_html_e('Confirmer le nouveau mot de passe', 'woocommerce'); ?><span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" required name="password_2" id="password_2" autocomplete="off" />
                </p>
            </fieldset>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="number_OMV"><?php esc_html_e('Numéro d’OMV', 'woocommerce'); ?>&nbsp; </label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="number_OMV" id="number_OMV" autocomplete="off" value="<?php echo $upvNewAccontIsset['number_OMV']; ?>" />
            </p>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="member_upv"><?php esc_html_e('Membre UPV', 'woocommerce'); ?>
                    &nbsp;<span class="required">*</span>
                    Cocher oui seulement si votre adhésion est valide.<a href="word.bild.com/wp-content/uploads/2019/05/2019_formulaire adhВsion et tarifs UPV.pdf" style="text-decoration: underline"> Devenir membre</a>
                </label>
                <div class="wrapper">
                    <div class="switch_box box_1">
                        <input type="checkbox" class="switch_1" name="member_upv" id="member_upv" <?php echo $upvNewAccontIsset['member_upv']; ?> >
                        <p id="textMember" style="display:none;  color: white; margin: 0">OUI</p>
                    </div>
                </div>
            </p>
            <div class="clear"></div>

            <p>
                <?php echo do_shortcode('[bws_google_captcha]'); ?>
            </p>

            <p>
                <?php wp_nonce_field('create_new_account', 'create-new-account-nonce'); ?>
                <button id="upv_create_new_account" type="submit" class="button button_default small" name="upv_create_new_account" value="<?php esc_attr_e('Save changes', 'woocommerce'); ?>"><?php esc_html_e('S’inscrire', 'woocommerce'); ?></button>
                <input type="hidden" name="action" value="upv_create_new_account" />
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" id="errorr_send_mess" style="margin-top: 20px; opacity:0">
                <span>Désolé. Vos informations ne correspondent à aucune fiche vétérinaire de notre système. Veuillez
                    <a href="word.bild.com/contact-2/" style="text-decoration: underline"> prendre
                        contact avec le secrétariat de l’UPV
                    </a>
                    pour obtenir vos accès.&nbsp;
                </span>
            </p>
            
        </form>
    </div>
</div>

<?php get_footer();