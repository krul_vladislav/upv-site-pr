<?php // Template Name: Page view My Account?>
<?php get_header();?>

<?php 
global $data;
$sidebar_show = true;

if( $data['en_sidebar'] == 'no' ) {
    $sidebar_show = false;    
}
else {  
    if(get_post_meta($post->ID, 'pyre_en_sidebar', true) == 'no'){      
        $sidebar_show = false;   
        
    }
}	
?>
<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */


defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>
<?php
$user_id = get_current_user_id(); 
$user_info            = get_userdata( $user_id );
      


?>
<div style="display: flex; align-items: center; justify-content: center; width: 100vw">
<div style="width: 40%; ">

<form class="woocommerce-EditAccountForm edit-account" action="" method="post" style="padding: 50px 0">

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php esc_html_e( 'Prénom', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo $user_info->first_name; ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php esc_html_e( 'Nom', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo $user_info->last_name; ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_display_name"><?php esc_html_e( 'Nom d’utilisateur', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" readonly="readonly" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo $user_info->display_name; ?>" /> <span><em><?php esc_html_e( '', 'woocommerce' ); ?></em></span>
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Adresse email', 'woocommerce' ); ?>&nbsp;</label>
		<input type="email" readonly="readonly" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo $user_info->user_email; ?>" />
	</p>
        
        <div class="wrapper" style="margin-top: 50px;">
            <label for="account_email">Membre UPV &nbsp;</label>
            <div class="switch_box box_1">
                <input type="checkbox" class="switch_1" name="member_upv" disabled <?php echo $user_info->member_upv == 'company' ? 'checked' : ''; ?> style="background: #ddd!important; cursor: text;">
                <p id="textMember" style="color: white; margin: 0">OUI</p>
            </div>
        </div>	
	<div class="clear"></div>
	
</form>
    </div>
    </div>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>


       

	
<?php get_footer(); ?>