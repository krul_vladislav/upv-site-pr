<?php

defined('ABSPATH') or die('No script kiddies please!!');
/*
  Plugin Name:  Everest Business Directory
  Plugin URI:   http://access-keys.com/wordpress-plugins/everest-business-directory/
  Description:  An ultimate business directory plugin for WordPress
  Version:      1.1.2
  Author:       Access Keys
  Author URI:   https://access-keys.com
  Text Domain:  everest-business-directory
  Domain Path:  /languages
 */

if ( !class_exists('EBD_Class') ) {

    class EBD_Class {

        /**
         * Plugin initialization
         *
         * @since 1.0.0
         */
        function __construct() {
            $this->define_constants();
            $this->includes();
        }

        /**
         * Define all the necessary constants
         *
         * @since 1.0.0
         */
        function define_constants() {
            defined('EBD_URL') or define('EBD_URL', plugin_dir_url(__FILE__));
            defined('EBD_PATH') or define('EBD_PATH', plugin_dir_path(__FILE__));
            defined('EBD_IMG_URL') or define('EBD_IMG_URL', EBD_URL . 'images/');
            defined('EBD_JS_URL') or define('EBD_JS_URL', EBD_URL . 'js/');
            defined('EBD_CSS_URL') or define('EBD_CSS_URL', EBD_URL . 'css/');
            defined('EBD_VERSION') or define('EBD_VERSION', '1.1.2');

            /**
             * Default Markup
             */
            $default_template_markup = '
<div id="ebd-primary" class="ebd-content-area">
    <main id="ebd-main" class="ebd-site-main" role="main">
    #template_content
    </main>
</div>';
            $default_sidebar_markup = '<div class="ebd-sidebar-wrap">#sidebar_content</div>';
            defined('EBD_DEFAULT_TEMPLATE_MARKUP') or define('EBD_DEFAULT_TEMPLATE_MARKUP', $default_template_markup);
            defined('EBD_DEFAULT_SIDEBAR_MARKUP') or define('EBD_DEFAULT_SIDEBAR_MARKUP', $default_sidebar_markup);
        }

        /**
         * Includes all the necessary files
         *
         * @since 1.0.0
         */
        function includes() {
            include(EBD_PATH . 'inc/classes/class-library.php');
            include(EBD_PATH . 'inc/cores/ebd-functions.php');
            include(EBD_PATH . 'inc/classes/class-email-notifications.php');
            include(EBD_PATH . 'inc/classes/class-init.php');
            include(EBD_PATH . 'inc/classes/class-activation.php');
            include(EBD_PATH . 'inc/classes/class-metabox.php');
            include(EBD_PATH . 'inc/classes/class-admin.php');
            include(EBD_PATH . 'inc/classes/class-enqueue.php');
            include(EBD_PATH . 'inc/classes/class-admin-ajax.php');
            include(EBD_PATH . 'inc/classes/class-settings.php');
            include(EBD_PATH . 'inc/classes/class-file-uploader.php');
            include(EBD_PATH . 'inc/classes/class-shortcode.php');
            include(EBD_PATH . 'inc/classes/class-directory-display.php');
            include(EBD_PATH . 'inc/classes/class-featured-widget.php');
            include(EBD_PATH . 'inc/classes/class-categories-list-widget.php');
            include(EBD_PATH . 'inc/classes/class-tags-list-widget.php');
        }

    }

    new EBD_Class();
    $ebd_settings = $GLOBALS[ 'ebd_settings' ] = get_option('ebd_settings');

    $GLOBALS[ 'ebd_directory_expiry' ] = (!empty($ebd_settings[ 'general' ][ 'directory_expiry' ])) ? true : false;
}


