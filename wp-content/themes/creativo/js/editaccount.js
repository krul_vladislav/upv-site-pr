document.addEventListener("DOMContentLoaded", function() {
  var editForm = document.getElementById("edit-account");
  if (editForm) {
    var firstInput = document.getElementById("password_1");
    var secondInput = document.getElementById("password_2");
    secondInput.addEventListener("input", function() {
      if (firstInput.value !== secondInput.value) {
        firstInput.classList.add("password-error-red-line");
        secondInput.classList.add("password-error-red-line");
      } else {
        firstInput.classList.remove("password-error-red-line");
        secondInput.classList.remove("password-error-red-line");
      }
    });
  }
});
