var fileList = [];
document.addEventListener("DOMContentLoaded", function() {
  var fileInput = document.getElementById("files");
  var divChild = document.getElementById("wpcf7-form-textarea-child");
  if (fileInput) {
    fileInput.addEventListener("change", function() {
      document.getElementById("wpcf7-form-textarea-child").innerHTML = "";
      fileList = [];
      // console.log(fileList, "after click");
      if (fileInput.files.length >= 0 && fileInput.files.length < 3) {
        for (var i = 0; i < fileInput.files.length; i++) {
          fileList.push(fileInput.files[i]);
        }
        var concatArray = [];
        for (var i = 0; i < fileList.length; i++) {
          if (fileList[i].name.length > 10) {
            // console.log(fileList[i].name, "more than 10 chars");
            concatArray.push(fileList[i].name);
            // console.log(concatArray[i], "Length -> ", concatArray[i].length);
            concatArray[i] = `${concatArray[i].substr(0, 10)}....pdf`;
            // console.log(concatArray[i], "after chages");
            divChild.innerHTML += `<div class="wpcf7-form-textarea-child-file"><div class="wpcf7-form-textarea-child-file-wrapper"><img class="wpcf7-form-textarea-child-file-wrapper-img" src="../wp-content/themes/creativo/images/pdf.png">${
              concatArray[i]
            } ${
              fileList[i].size
            } KB</div><div class="remove-button" id="${i}" ><i class="fa fa-times"></i></div></div>`;
          } else {
            divChild.innerHTML += `<div class="wpcf7-form-textarea-child-file"><div class="wpcf7-form-textarea-child-file-wrapper"><img class="wpcf7-form-textarea-child-file-wrapper-img" src="../wp-content/themes/creativo/images/pdf.png">${
              fileList[i].name
            } ${
              fileList[i].size
            } KB</div><div class="remove-button" id="${i}" ><i class="fa fa-times"></i></div></div>`;
          }
        }
        // console.log("[0-2] files");
      } else {
        // console.log(fileList);
        document.querySelector(".wpcf7-form").addEventListener("submit", function(event) {
          event.preventDefault();
          // console.log("Please, select less than 3 files");
        });
      }
      // console.log(fileList, "после цикла");
      var container = document.querySelectorAll(".remove-button");
      for (var i = 0; i < container.length; i++) {
        container[i].setAttribute("onclick", "davaidavai(this)");
      }
    });
  }
});

function davaidavai(el) {
  el.parentElement.style.display = "none";
  if (el.getAttribute("id") == 0) {
    delete fileList[0];
  }
  if (el.getAttribute("id") == 1) {
    delete fileList[1];
  }
}

//  Annuaire page - divs under map
jQuery(document).ready(function($) {
  var showmore_button = $("#showmore-button");
  if (showmore_button.length != 0) {
    var categorie_block = $(".ebd-map-info2");
    var categorie_block_length = categorie_block.length;

    if (categorie_block_length != 0) {
      if (categorie_block_length > 12) {
        show_12_block_in_page(categorie_block, categorie_block_length, 12);
        $("#showmore-button").css("display", "block");
        add_block_product(categorie_block, categorie_block_length, 12);
      } else {
        show_12_block_in_page(categorie_block, categorie_block_length, 12);
      }
    }
  }
});

function show_12_block_in_page(block, block_total, block_shown) {
  for (var i = 0; i < block_total; i++) {
    if (i < block_shown) {
      $(block[i]).css("display", "block");
    } else {
      $(block[i]).css("display", "none");
    }
  }
}
function add_block_product(block, block_total, block_shown) {
  $("#showmore-button").click(function() {
    if (block_shown < block_total) {
      for (var i = 1; i <= 12; i++) {
        $(block[block_shown]).css("display", "block");
        block_shown++;
        if (block_shown === block_total) {
          document.querySelector("#showmore-button").style.display = "none";
        }
      }
    }
  });
}
jQuery(document).ready(function($) {
  $("#menu-item-click").click(function() {
    $(this)
      .closest(".menu-item.custom-login-box")
      .find(".sub-menu")
      .css("display", "block")
      .css("opacity", "1");
  });
});
