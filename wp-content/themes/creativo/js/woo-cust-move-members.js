(function ($) {
    $(document).ready(function () {
        var plansList = JSON.parse(cust_move_vars.plans);

        var listPlans = '<select name="action-move_members" class="single-action-move_member" style="margin-left:10px;">>';
        listPlans += '<option value="-1">Choisir un plan</option>';

        $.each(plansList, function (key, value) {
            listPlans += '<option value="' + key + '">' + value + '</option>';
        });

        listPlans += '</select>';
        listPlans += '<input type="button" class="button action doaction-custom-single-move" value="Déplacer les" style="margin-left:5px;">';

        $(listPlans).insertAfter('.rpwcm_membership_plan_remove_item');

        $(document).on('click', '.doaction-custom-single-move', function (e) {
            e.preventDefault();
            
            var userRow = $(this);

            var planTo = $(this).closest('.rpwcm_membership_plan_item_list_expires').find('.single-action-move_member').val();
            var planFrom = $(this).closest('.rpwcm_membership_plan_item_list_expires').find('input[name=rpwcm_plan_id]').val();
            var userId = $(this).closest('.rpwcm_membership_plan_item_list_expires').find('input[name=rpwcm_user_id]').val();

            if (planTo < 0) {
                alert('Aucun plan de destination sélectionné!');
                return false;
            }

            var jqxhr = $.ajax({
                url: cust_move_vars.url,
                type: 'post',
                data: {
                    _wpnonce    : cust_move_vars._wpnonce,
                    action      : "custom_single_move_members",
                    to          : planTo,
                    from        : planFrom,
                    user        : userId
                },
                beforeSend: function () {
                    $('body').append('<div class="cust-move-members-loader" style="position: fixed;width: 100%;height: 100%;top: 0;z-index: 999999;background-color: rgba(0,0,0,0.5);display: flex;align-items: center;justify-content: center;"><img src="/wp-admin/images/loader.gif" style="width: 50px;height: 50px;margin: auto;" /></div>');
                },
                success: function (data) {
                    if (data.success == 'ok') {
                        userRow.closest('tr').remove();
                    } else {
                        alert("Quelque chose s'est mal passé!");
                    }
                }
            });
            jqxhr.always(function () {
                setTimeout(function () {
                    $('.cust-move-members-loader').remove();
                }, 500);
            });
        });

    });
})(jQuery);  