document.addEventListener("DOMContentLoaded", function() {
  var btnLoginForm = document.getElementById("menu-item-click");
  var wooLoginForm = document.getElementById("woo_login_form");
  if (btnLoginForm) {
    btnLoginForm.addEventListener("click", function() {
      wooLoginForm.classList.toggle("woo_login_form_toggle");
    });
  }
});
