document.addEventListener("DOMContentLoaded", function() {
  var select_ad = $(".awpcp-dropdown-disabled");
  if (select_ad) {
    select_ad.attr("disabled", "disabled");
  }

  var page_petites_annonces = $(".awpcp-classifieds-menu--menu-item-link");
  if (page_petites_annonces) {
    $('a:contains("Recherche Avancée")').removeAttr("href");
    $('a:contains("Place Ad")').css("display", "none");
    $('a:contains("Browse Ads")').css("display", "none");
  }

  var page_payment = $("#order_comments");
  if (page_payment) {
    page_payment.attr(
      "placeholder",
      "Veuillez détailler votre produit personnalisé ici"
    );
    $(page_payment).blur(function() {
      page_payment.attr(
        "placeholder",
        "Veuillez détailler votre produit personnalisé ici"
      );
    });

    $(page_payment).focus(function() {
      page_payment.removeAttr("placeholder");
    });
  }

  var img_for_page_shop = document.getElementById("page_shop");
  if (img_for_page_shop) {
    img_for_page_shop.classList.add("page-shop-img");
    $(
      ".woocommerce-form-row.woocommerce-form-row--first.form-row.form-row-first"
    ).css("display", "none");
  }

//  var chechTag = document.getElementById("password_2");
//  if (chechTag) {
//    var all_num_omv = $(".test_test")
//      .find("textarea")
//      .val();
//    console.log(all_num_omv);
//    document
//      .getElementById("number_OMV")
//      .addEventListener("change", function() {
//        //  var formname = document.getElementById("account_first_name").value;
//        //  var formsurname = document.getElementById("account_last_name").value;
//        //   var formemail = document.getElementById("account_email").value;
//        var num_omv = document.getElementById("number_OMV").value;
//        // console.log(formname);
//        // console.log(formsurname);
//        // console.log(formemail);
//        //console.log(num_omv);
//
//        if (all_num_omv.includes(num_omv) == false || num_omv == "") {
//          var failed = document.getElementById("errorr_send_mess");
//          failed.style.opacity = 1;
//          document.getElementById("knopka").disabled = true;
//          return;
//        } else {
//          var failed = document.getElementById("errorr_send_mess");
//          failed.style.opacity = 0;
//          document.getElementById("knopka").disabled = false;
//
//          //    document.getElementById("forename-teamleader").value = formname;
//          //  document.getElementById("surname-teamleader").value = formsurname;
//          //    document.getElementById("email-teamleader").value = formemail;
//          //    document.getElementById("telephone-teamleader").value = num_omv;
//          // $("#knopka").click(function() {
//          //   $("#knopka-teamleader").trigger("click");
//          // });
//        }
//      });
//  }
});
