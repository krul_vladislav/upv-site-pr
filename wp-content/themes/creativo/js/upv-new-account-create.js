(function ($) {
    $(document).ready(function () {
        var OMVs = JSON.parse(newAccountCreate.OMVs);
        var errorrs = JSON.parse(newAccountCreate.errors);
//        console.log(OMVs);
        isCheckedMemberUPV();
        
        if(Object.keys(errorrs).length > 0){
            $('#errorr_send_mess').css({'opacity':1});
            $.each(errorrs, function (key, value) {
                $('#errorr_send_mess').prepend('<p style="color:red;"><b>' + key + '</b> - ' + value + '</p>');
            });
            $('html, body').animate({scrollTop: $('#errorr_send_mess').offset().top-150}, 1500);
        }
        
        $(document).on('change', '#member_upv', function(){
            isCheckedMemberUPV();
        });
        
        $(document).on('keyup', '#password_1, #password_2', function(){
            $('#password_1').removeClass('password-error-red-line');
            $('#password_2').removeClass('password-error-red-line');
        });
        
        $(document).on('keyup', '#account_email, #number_OMV, #account_first_name, #account_last_name', function(){
            $(this).removeClass('password-error-red-line');
        });
                
        $(document).on('change', '#number_OMV', function(){
            isExistNumberOMV();
        });
        
        $(document).on('click', '#upv_create_new_account', function(e){
            e.preventDefault();
            
            if(!$('#account_first_name').val()){
                $('#account_first_name').addClass('password-error-red-line');
                $('html, body').animate({scrollTop: $('#account_first_name').offset().top-150}, 500);
                return false;
            }
            
            if(!$('#account_last_name').val()){
                $('#account_last_name').addClass('password-error-red-line');
                $('html, body').animate({scrollTop: $('#account_last_name').offset().top-150}, 500);
                return false;
            }
            
            if(!isValidEmail()){
                $('html, body').animate({scrollTop: $('#account_email').offset().top-150}, 500);
                return false;
            }
            
            if(!isCorrectPassword()){
                $('html, body').animate({scrollTop: $('#password_1').offset().top-150}, 500);
                return false;
            }
            
            if(!isExistNumberOMV()){
                $('html, body').animate({scrollTop: $('#number_OMV').offset().top-150}, 500);
                return false;
            }
            
//            if(!isCheckedMemberUPV()){
//                $('#member_upv').addClass('password-error-red-line').css({'border':'1px solid'});
//                $('html, body').animate({scrollTop: $('#member_upv').offset().top-150}, 500);
//                return false;
//            }
            
            $(this).closest('form').submit();
        });

        function isCorrectPassword() {
            var pass1 = $('#password_1');
            var pass2 = $('#password_2');
            console.log(pass1);
            if( !pass1.val() || pass1.val() !== pass2.val() ){
                pass1.addClass('password-error-red-line');
                pass2.addClass('password-error-red-line');
                return false;
            }else{
                pass1.removeClass('password-error-red-line');
                pass2.removeClass('password-error-red-line');
                return true;
            }
        }

        function isCheckedMemberUPV() {
            if($('#member_upv').prop('checked')){
                $('#member_upv').removeClass('password-error-red-line').css({'border':'unset'});
                $('#textMember').show();
                return true;
            }else{
                $('#textMember').hide();                
                return false;
            }
        }
        
        function isExistNumberOMV() {
            var numberOMV = $('#number_OMV').val();          
            
            if(OMVs.indexOf(numberOMV) < 0){
                $('#number_OMV').addClass('password-error-red-line');
                return false;
            }else{
                $('#number_OMV').removeClass('password-error-red-line');
                return true;                
            }            
        }
        
        function isValidEmail() {
            var email = $('#account_email').val();          
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            
            if(re.test(email)){
                $('#account_email').removeClass('password-error-red-line');
                return true;
            }else{
                $('#account_email').addClass('password-error-red-line');
                return false;                
            }            
        }
        
    });
})(jQuery);  