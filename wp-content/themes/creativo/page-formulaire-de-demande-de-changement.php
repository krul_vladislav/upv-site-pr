<?php // Template Name: Page Formulaire de demande de changement ?>

<?php

    if(isset($_GET['contact']) && intval($_GET['contact'])){
        $user_id = $_GET['contact'];        
    }
    
    $info = get_teamleader_info($user_id);
    
    function get_teamleader_info($id) {        
        global $wpdb;

        $result = $wpdb->get_row("SELECT * FROM  `wp_teamleader_contacts` WHERE `id` = '$id' ");

        $customFieldList = $wpdb->get_results("SELECT DISTINCT CUSTOM_FIELD_225073 as CUSTOM_FIELD FROM {$wpdb->prefix}teamleader_contacts WHERE CUSTOM_FIELD_225073 IS NOT NULL");
        $resultsKeys = [];
        foreach ($customFieldList as $resultsRow) {
            $resultsKeys = array_merge($resultsKeys, explode(',', $resultsRow->CUSTOM_FIELD));
        }
        $resultsKeys = array_unique($resultsKeys);
        asort($resultsKeys);

        $trans = get_html_translation_table(HTML_ENTITIES);
        $field_225073 = str_replace('\\\'', '&#039;', strtr(sanitize_text_field($result && !empty($result->CUSTOM_FIELD_225073) ? $result->CUSTOM_FIELD_225073 : ''), $trans));

        $preSetVals = !empty($field_225073) ? explode(',', $field_225073) : [];

        $args = [
            'account_first_name'=> $result ? $result->forename : '',
            'account_last_name' => $result ? $result->surname : '',
            'account_email'     => $result ? $result->email : '',
            'first_name'        => $result ? $result->forename : '',
            'last_name'         => $result ? $result->surname : '',
            'email'             => $result ? $result->email : '',
            'tel1'              => $result ? $result->telephone : '',
            'tel2'              => $result ? $result->gsm : '',
            'website'           => $result ? $result->website : '',
            'street'            => $result ? $result->street : '',
            'number'            => $result ? $result->number : '',
            'zipcode'           => $result ? $result->zipcode : '',
            'city'              => $result ? $result->city : '',
            'country'           => $result ? $result->country : '',
            'omv_number'        => '',
            'bic'               => '',
            'member_type'       => $result ? $result->member_type : '',
            'poles_all'         => $resultsKeys,
            'poles'             => $preSetVals,
            'entreprise'        => '',
            'presentation'      => '',
            'horaires'          => ''
        ];
        return $args;
    }

    if ($_POST) {
        $trans = get_html_translation_table(HTML_ENTITIES);
        $field_225073 = str_replace('\\\'', '&#039;', strtr(sanitize_text_field(isset($_POST['poles']) && !empty($_POST['poles']) ? $_POST['poles'] : ''), $trans));
        $preSetPoles = !empty($field_225073) ? explode(',', $field_225073) : [];        
        
        $info['first_name'] = $_POST['account_first_name'];
        $info['last_name'] = $_POST['account_last_name'];
        $info['email'] = $_POST['account_email'];
        $info['omv_number'] = $_POST['number_OMV'];
        $info['entreprise'] = $_POST['entreprise'];
        $info['website'] = $_POST['site_internet'];
        $info['presentation'] = $_POST['presentation'];
        $info['horaires'] = $_POST['horaires'];
        $info['poles'] = $preSetPoles;
        $info['tel1'] = $_POST['tel1'];
        $info['tel2'] = $_POST['tel2'];

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $message = '<hr>
                        <p>Une demande de changement pour la fiche /'. $info['account_first_name'].' '. $info['account_last_name'].' / '.$info['entreprise']. 'a été demandée.</p>
                        <p><br /> </p>
                        <p>Voici les informations transmises:</p>
                        <p><br /> </p>
                        <p>Prénom: '.$info['first_name'].'</p>
                        <p><br /> </p>
                        <p>Nom: '.$info['last_name'].'</p>
                        <p><br /> </p>
                        <p>Email: '.$info['email'].'</p>                        
                        <p>Numéro de gsm: '.$info['tel2'].'</p>
                        <p>Téléphone fixe: '.$info['tel1'].'</p>
                        <p>Numéro d’OMV: '.$info['omv_number'].'</p>
                        <p>Entreprise: '.$info['entreprise'].'</p>
                        <p>Site internet: '.$info['website'].'</p>
                        <p>Brève présentation: '.$info['presentation'].'</p>
                        <p>Horaires de consultation: '.$info['horaires'].'</p>
                        <p>Pôles d’intérêts: '.implode(',', $info['poles']).'</p>
                        <p>Ceci est un message automatique, merci de ne pas y répondre.</p>
                        </hr>';

        mail('upv@word.bild.com', "Demande de changement sur une fiche", $message, $headers);
}

?>

<?php get_header();?>

<?php 
    global $data;

    $sidebar_show = true;
    if( $data['en_sidebar'] == 'no' ) {
        $sidebar_show = false;    
    }
    else {  
        if(get_post_meta($post->ID, 'pyre_en_sidebar  ', true) == 'no'){      
            $sidebar_show = false;  
        }
    }	
?>

<?php
    /**
     * Edit account form
     *
     * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
     *
     * HOWEVER, on occasion WooCommerce will need to update template files and you
     * (the theme developer) will need to copy the new files to your theme to
     * maintain compatibility. We try to do this as little as possible, but it does
     * happen. When this occurs the version of the template file will be bumped and
     * the readme will list any important changes.
     *
     * @see     https://docs.woocommerce.com/document/template-structure/
     * @package WooCommerce/Templates
     * @version 3.4.0
     */

    defined( 'ABSPATH' ) || exit;

    do_action( 'woocommerce_before_edit_account_form' ); 
?>


<div style="display: flex; align-items: center; justify-content: center; width: 100vw">
    <div style="width: 40%; ">
        <style>            
            .woocommerce-EditAccountForm .ms-parent.select2-poles{
                width: -webkit-fill-available!important;
                width: -moz-available!important;
            }
            .woocommerce-EditAccountForm .ms-drop input[type=radio], .ms-drop input[type=checkbox]{
                display: none;
            }
            .woocommerce-EditAccountForm .ms-drop ul{
                padding-left: 5px!important;
            }
            .woocommerce-EditAccountForm .ms-parent.select2-poles li.selected span{
                background-color: #208ef0;
                color: #fff;
                display: block;
            }
            .woocommerce-EditAccountForm .ms-parent.select2-poles li span{                        
                line-height: 1.5em;
                display: inline-block;
                margin: 3px 0;
            }
            .woocommerce-EditAccountForm .ms-choice{
                border-left: 1px solid #ccc;
                border-right: 1px solid #ccc;
                border-top: 1px solid #ccc;
                border-radius: 0;
                height: 40px;                
                color: #000 !important;             
            }
            .woocommerce-EditAccountForm .ms-drop{
               width: auto; 
            }
            .woocommerce-EditAccountForm .ms-choice > span {
                padding: 10px!important;
                font-weight: normal;
            }
            .woocommerce-EditAccountForm .ms-choice > span.placeholder {
                color: unset;
            }
            .woocommerce-EditAccountForm .ms-choice > div {        
                background: url(../images/down-icon-3.png) left top no-repeat;
            }
            .woocommerce-EditAccountForm .ms-choice{
                outline: unset;
            }
        </style>

        <form class="woocommerce-EditAccountForm edit-account" action="" method="post" style="padding: 50px 0">

            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                    <label for="account_first_name"><?php esc_html_e( 'Prénom ', 'woocommerce' ); ?>&nbsp;</label>
                    <input type="text"  class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo $info['first_name']; ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                    <label for="account_last_name"><?php esc_html_e( 'Nom ', 'woocommerce' ); ?>&nbsp;</label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo $info['last_name']; ?>" />
            </p>
            <div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="account_email"><?php esc_html_e( 'Adresse email ', 'woocommerce' ); ?>&nbsp;</label>
                    <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo $info['email']; ?>" />
            </p>
            <div class="clear"></div>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="tel2"><?php esc_html_e( 'Numéro de gsm', 'woocommerce' ); ?>&nbsp;</label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="tel2" id="tel2" autocomplete="tel2" value="<?php echo $info['tel2']; ?>" />
            </p>
            <div class="clear"></div>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="tel1"><?php esc_html_e( 'Téléphone fixe ', 'woocommerce' ); ?>&nbsp;</label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="tel1" id="tel1" autocomplete="tel1" value="<?php echo $info['tel1']; ?>" />
            </p>
            <div class="clear"></div>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="number_OMV"><?php esc_html_e( 'Numéro d’OMV' , 'woocommerce');?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="number_OMV" id="number_OMV" autocomplete="off" value="<?php echo $info['omv_number']; ?>" />
            </p>
            <div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="entreprise"><?php esc_html_e('Entreprise', 'woocommerce'); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="entreprise" id="entreprise" autocomplete="off" value="<?php echo $info['entreprise']; ?>" />
            </p>
            <div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="site_internet"><?php esc_html_e('Site internet', 'woocommerce'); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="site_internet" id="site_internet" autocomplete="off" value="<?php echo $info['site_internet']; ?>" />
            </p>
            <div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="presentation"><?php esc_html_e('Brève présentation', 'woocommerce'); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="presentation" id="presentation" autocomplete="off" maxlength="140" placeholder="Maximum 140 caractères" value="<?php echo $info['presentation']; ?>" />
            </p>
            <div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="horaires"><?php esc_html_e('Horaires de consultation', 'woocommerce'); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" required name="horaires" id="horaires" autocomplete="off" maxlength="140" placeholder="Maximum 140 caractères" value="<?php echo $info['horaires']; ?>" />
            </p><div class="clear"></div>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="poles"><?php esc_html_e('Pôles d’intérêts (veuillez séparer chaque intérêt par une virgule)', 'woocommerce'); ?>&nbsp;<span class="required" style="margin-right: 20px;">*</span> </label>    
                <select id="select2-poles" class="select2-poles" multiple style="width:-webkit-fill-available;" placeholder="Pôles d'intérêts">
                    <?php foreach ($info['poles_all'] as $key => $resultRow) { ?>
                        <option id="field_poles_<?php echo $key; ?>" value="<?php echo $resultRow ?>" <?php echo (in_array($resultRow, $info['poles']) ? 'selected' : ''); ?> >
                            <?php echo $resultRow ?>
                        </option>
                    <?php } ?>
                </select>
                <input id="poles" type="hidden" name="poles" value="<?php echo implode(',',$info['poles']); ?>">
            </p>


            <div style="width: 302px; margin: 0 auto">
                <?php echo do_shortcode('[bws_google_captcha]'); ?>
            </div>

            <p style="justify-content: center; display: flex;">	
                    <!-- <button id="knopka" type="submit"  name="save_account_details" value="<?php esc_attr_e('Envoyer', 'woocommerce'); ?>"><?php esc_html_e('Envoyer', 'woocommerce'); ?></button> -->
                <input style="width:100%;
                       justify-content: center; 
                       display: flex;    
                       border: 1px solid #ccc;
                       background-color: #ffffff;
                       border-color: #999999;
                       color: #3d3d3d;
                       font-size: 12px;
                       font-weight: 600;" type="submit" value="Envoyer" class="wpcf7-form-control wpcf7-submit wpcf7-form-submit-button" id="ex2">		
            </p>


        </form>
    </div>
</div>

<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" id="successful_send" style="margin-top: 20px; display: flex; justify-content: center; display: none">
    <span style="margin-top: 20px; display: flex; justify-content: center; ">Votre demande a bien été enregistrée. Après vérification, nous mettrons les changements en ligne.&nbsp;</span>
    <section class="ex2"></section>
</p>

<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" id="sending_failed" style=" text-align:justify; display: flex; justify-content: center; display: none">
    <span style="margin-top: 20px; display: flex; justify-content: center;  ">Erreur, veuillez réessayer plus tard.&nbsp;</span>
    <section class="ex2"></section>
</p>

<?php do_action('woocommerce_after_edit_account_form'); ?>

<?php get_footer(); ?>

<?php
            if (isset($_POST['account_first_name']) && isset($_POST['account_last_name']) && isset($_POST['account_email'])) {
                if ($_POST['account_first_name'] && $_POST['account_last_name'] && $_POST['account_email']) {
                    ?>
                    <script>
                        var successful = document.getElementById("successful_send");
                        var failed = document.getElementById("sending_failed");
                        if (successful)
                        {
                            successful.style.display = "block";
                            failed.style.display = "none";
                        }
                    </script>
                    <?php
                } else {
                    ?>
                    <script>
                        var successful = document.getElementById("successful_send");
                        var failed = document.getElementById("sending_failed");
                        if (successful)
                        {
                            successful.style.display = "none";
                            failed.style.display = "block";
                        }
                    </script>
                    <?php
                }
            }
            
?>
    <script>
        $(document).ready(function () {
            <?php if($_POST){ ?>
                $('html,body').animate({scrollTop:$('#ex2').offset().top}, 1500);
            <?php } ?>
                
            $('#select2-poles').multipleSelect({
                displayTitle: true,
                selectAll: false,
                displayDelimiter: ',',
                minimumCountSelected: 8,
                formatCountSelected(count, total) {
                    return count + ' de ' + total + ' sélectionné';
                }
            });
                        
            $(document).on('click', '#ex2', function(e){
                e.preventDefault();
                
                $('input[name=poles]').val($('#select2-poles').multipleSelect('getSelects').join(','));                
                $(this).closest('form').submit();
            });
                        
        });
    </script>

<?php
    