<?php // Template Name: Page view information customer?>
<?php get_header();?>

<?php 
global $data;
$sidebar_show = true;

if( $data['en_sidebar'] == 'no' ) {
    $sidebar_show = false;    
}
else {  
    if(get_post_meta($post->ID, 'pyre_en_sidebar', true) == 'no'){      
        $sidebar_show = false;   
        
    }
}	
?>
<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */


defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>
<?php
$user_id = get_current_user_id(); 
$user_info            = get_userdata( $user_id );
   
//print_r($user_info);


    echo $user_info->edit_products . "\n";


?>
<div>
<div class="product-table-container">


<?php 
    get_info_customer($user_id);
  ?>

    </div>
    </div>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>



	
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	jQuery(function($){
		$('#primary table').stacktable();
	});
});
</script>


<?php
function get_info_customer($id){

    global $wpdb;

    $host = 'localhost';  
    $database = 'clevermint'; 
    $user = 'clevermint'; 
    $password = 'n%78Nr3o'; 

    // require_once 'connection.php'; 
    $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
    $query ="SELECT * FROM  `customer_order_info` WHERE `user_id` = '$id'";
    //$query = "SELECT * FROM  'wp_teamleader_contacts'";
    $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
if($result)
{
  
   $rows = mysqli_num_rows($result); // количество полученных строк
    if($rows != 0){
     echo "<div id='primary' style='padding: 50px 0px 50px 0px;'>";
    echo "<table style=' border-collapse: collapse;'>";
    echo "<thead> <tr><th></th><th>Nom du produit</th><th>Prix</th><th>Prix total</th><th>Date</th></tr></thead>";
    for ($i = 0 ; $i < $rows ; ++$i)
    {
        $row = mysqli_fetch_row($result);
        echo "<tr class='border-in-tb style-class'>";
            for ($j = 0 ; $j < 6 ; ++$j) 
            {
                if($j==0) {  echo "<td class='style-class'>$row[$j]</td>";}
                else if($j==1) {}
                else if($j == 2){
                    $prod_name = explode(',', $row[$j]);
                    $prod_price = explode('€',$row[$j+1]);

                    if(count($prod_name)<=1){
                        echo "<td class='style-class'>$prod_name[0]</td>";
                    }
                    else{
                        $l = 1;
                        $count_prod = count($prod_name);
                        for ($k = 0 ; $k <count($prod_name) ; $k++) {
                            if($k == 0)
                            {
                                echo "<td class='style-class'>$prod_name[$k]</td>";
                                echo "<td class='style-class'>€$prod_price[$l]</td>";
                                echo "<td class='style-class'></td>";
                                echo "<td class='style-class'></td>";
                                echo "</tr>";
                            }
                            else{
                                $tot_price = 4;
                                $date_false = 5;
                                echo "<td class='style-class'></td>";
                                echo "<td class='style-class'>$prod_name[$k]</td>";
                                echo "<td class='style-class'>€$prod_price[$l]</td>";
                                    if($k == $count_prod-1){
                                        echo "<td class='style-class'>$row[$tot_price]</td>";
                                        echo "<td class='style-class'>$row[$date_false]</td>";
                                    }
                                    else{
                                        echo "<td class='style-class'></td>";
                                        echo "<td class='style-class'></td>";
                                    }
                                echo "</tr>";
                            }
                            $l++;
                        }
                        $j+=3;
                    }
                }  
                
                else if($j==3) {  echo "<td class='style-class'>$row[$j]</td>";}
                else if($j==4) {  echo "<td class='style-class'>$row[$j]</td>";}
                else if($j==5) {  echo "<td class='style-class'>$row[$j]</td>";}
            }
        echo "</tr>";
    }
    echo "</table>";
    echo '</div>';
}
else echo 'Vous n`avez aucun achat';
    // очищаем результат
    mysqli_free_result($result);
}
 
mysqli_close($link);
}