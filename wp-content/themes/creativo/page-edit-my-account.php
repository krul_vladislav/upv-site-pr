<?php // Template Name: Page Edit My Account ?>

<?php get_header();?>

<?php 
    defined( 'ABSPATH' ) || exit;

    global $data;
    global $wpdb;
    
    $sidebar_show = true;
    if( $data['en_sidebar'] == 'no' ) {
        $sidebar_show = false;    
    }else {  
        if(get_post_meta($post->ID, 'pyre_en_sidebar', true) == 'no'){      
            $sidebar_show = false;   
        }
    }	
    if(!isset($_POST['action']) || $_POST['action'] != 'save_account_details'){
        wc_clear_notices();
    }else{
        if ( wc_notice_count( 'error' ) !== 0 ) {
            ?>
            <div style="display: flex; align-items: center; justify-content: center; width: 100vw">
                <div style="width: 40%; ">
                    <?php wc_print_notices(); ?>                    
                </div>
            </div>
            <?php
            wc_clear_notices();
        }
    }
    
    $user_info = get_userdata( get_current_user_id());
    $contacts = array();
    
    $number_omv = get_user_meta( $user_info->ID, 'number_omv', false);
    
    if(is_array($number_omv) && !empty($number_omv)){        
        $contacts = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}teamleader_contacts WHERE CUSTOM_FIELD_238760 IN ('".implode("','",$number_omv)."')" );
    }
        
    /**
     * Edit account form
     *
     * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
     *
     * HOWEVER, on occasion WooCommerce will need to update template files and you
     * (the theme developer) will need to copy the new files to your theme to
     * maintain compatibility. We try to do this as little as possible, but it does
     * happen. When this occurs the version of the template file will be bumped and
     * the readme will list any important changes.
     *
     * @see     https://docs.woocommerce.com/document/template-structure/
     * @package WooCommerce/Templates
     * @version 3.4.0
     */

    do_action( 'woocommerce_before_edit_account_form' ); 
    
?>

<div style="display: flex; align-items: center; justify-content: center; width: 100vw">
    <div style="width: 40%; ">
        <form class="woocommerce-EditAccountForm edit-account" id="edit-account" action="" method="post">
            <?php do_action('woocommerce_edit_account_form_start'); ?>

            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                <label for="account_first_name"><?php esc_html_e('Prénom', 'woocommerce'); ?>&nbsp;</label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr($user_info->first_name); ?>" />
            </p>
            <div class="clear"></div>
            <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                <label for="account_last_name"><?php esc_html_e('Nom', 'woocommerce'); ?>&nbsp;</label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr($user_info->last_name); ?>" />
            </p>
            <div class="clear"></div>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="account_display_name"><?php esc_html_e('Nom d’utilisateur', 'woocommerce'); ?>&nbsp;</label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo $user_info->display_name; ?>" /> <span><em><?php esc_html_e('', 'woocommerce'); ?></em></span>
            </p>            
            <div class="clear"></div>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="account_email"><?php esc_html_e('Adresse email', 'woocommerce'); ?>&nbsp;</label>
                <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr($user_info->user_email); ?>" />
            </p>
            <div class="clear"></div>            
            <div class="wrapper" style="margin-top: 60px;margin-bottom: -20px;">
                <label for="account_email">Membre UPV &nbsp;</label>
                <div class="switch_box box_1">
                    <input type="checkbox" class="switch_1" name="member_upv" disabled <?php echo $user_info->member_upv == 'company' ? 'checked' : ''; ?> style="background: #ddd!important; cursor: text;">
                    <p id="textMember" style="color: white; margin: 0">OUI</p>
                </div>
            </div>	
            <div class="clear"></div>                
            <fieldset>
                <legend style="width: 100%;padding-bottom: 10px;">
                    <span style="display: inline-block; line-height: 36px;"><?php esc_html_e('Changement de mot de passe', 'woocommerce'); ?></span>
                    <div class="pull-right" style="">                        
                        <div class="switch_box box_1" style="width: 140px;">
                            <label for="pass_checkbox" style="position: absolute;color: #fff;z-index: 1;cursor: pointer;">Changement</label>
                            <input id="pass_checkbox" type="checkbox" class="switch_1" name="pass_change" style="height: 1em; width: 4.5em;">
                        </div>
                    </div>	
                </legend>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide upv-row-pass" style="display: none">
                    <label for="password_current"><?php esc_html_e('Mot de passe actuel (laissez vide pour laisser inchangé)', 'woocommerce'); ?></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" />
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide upv-row-pass" style="display: none">
                    <label for="password_1"><?php esc_html_e('Nouveau mot de passe (laisser en blanc pour laisser inchangé)', 'woocommerce'); ?></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide upv-row-pass" style="display: none">
                    <label for="password_2"><?php esc_html_e('Confirmer le nouveau mot de passe', 'woocommerce'); ?></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />
                </p>
            </fieldset>
            <div class="clear"></div>
            <?php do_action('woocommerce_edit_account_form'); ?>
            <p>
                <?php wp_nonce_field('save_account_details', 'save-account-details-nonce'); ?>
                <button type="submit" class="button button_default small" name="save_account_details" value="<?php esc_attr_e('Save changes', 'woocommerce'); ?>"><?php esc_html_e('Sauvegarder les modifications', 'woocommerce'); ?></button>
                <input type="hidden" name="action" value="save_account_details" />
            </p>
            <?php do_action('woocommerce_edit_account_form_end'); ?>
        </form>
    </div>
</div>

<?php foreach($contacts as $contact){ ?>
    <div style="display: flex; align-items: center; justify-content: center; width: 100vw">
        <div style="width: 40%; border: 1px solid #ccc; margin-bottom: 20px;">
            <div class="ebd-info-content-wrap" style="display: flex; flex-direction: column; justify-content: center; align-items: center; padding: 10px;">
                    <div class="ebd-info-title">
                        <span class="" style="color: #5DC0D5; font-weight: bold;"><?php echo $contact->forename . ' ' . $contact->surname; ?></span>
                    </div>
                    <div class="ebd-info-address">
                        <?php
                            $address = trim($contact->street . ' ' . $contact->number . ', ' . $contact->zipcode . ', ' . $contact->city);
                            echo rtrim($address, ",");
                        ?>                        
                    </div>
                    <div class="ebd-info-phone">
                        <a href="#"><?php echo $contact->telephone ?></a>
                    </div>
                    <div class="ebd-info-phone">
                        <a href="#"><?php echo $contact->gsm ?></a>
                    </div>
                    <div class="ebd-info-website">                        
                        <a href="#"><?php echo $contact->email ?></a>
                    </div>
                    
                    <p>
                        <label style="font-weight: bold;">Infrastructure&nbsp;</label>
                    </p>                    
                    <img style="margin-top:-20px" src="http://word.bild.com/wp-content/uploads/files/library_upv/logos-pictos/structures/<?php if($contact->member_type == 'client'){ echo 'cabinet';}else{ echo 'clinique';} ?>.gif" alt="">
                    
                    <p>
                        <label style="font-weight: bold;" for="Entreprise">Centre d'intérêts&nbsp;</label>
                    </p>
                    <div style="margin-top:-20px">
                        &nbsp;<?php echo $contact->CUSTOM_FIELD_225073 ?>
                    </div>

                    <p style="justify-content: center; display: flex;">	
                        <a href="word.bild.com/formulaire-de-demande-de-changement/?contact=<?php echo $contact->id ?>" style="background-color: #44b6df" class="wpcf7-form-control wpcf7-submit wpcf7-form-submit-button customIdClick"> Demander un changement</a>
                    </p>
                </div>            
        </div>
    </div>
<?php } ?>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>

<script>
    jQuery(document).ready( function($)  {
        
        $(document).on('change', '#pass_checkbox', function(e){
            e.preventDefault();            
            if($(this).prop('checked')){
                $('.upv-row-pass').show();
            }else{
                $('.upv-row-pass').hide();
                $('#password_current, #password_1, #password_2').val('');
            }
        });
        
        $(document).on('click', '.woocommerce-error li', function (){
            $(this).closest('li').remove();
        });
                        
    });
</script>
	
<?php get_footer();