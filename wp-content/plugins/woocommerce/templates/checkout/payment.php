<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if(isset($_POST['custom_form']) && $_POST['custom_form'] == "custom_submit") 
{
	$user_id = get_current_user_id(); 
	$product_name =$_POST['product_name3'];
	$product_price = $_POST['product_price3'];
	$product_total = $_POST['product_total3'];
	
	global $wpdb;
    $host = 'localhost';  
    $database = 'clevermint'; 
    $user = 'clevermint'; 
    $password = 'n%78Nr3o'; 


	$link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
	$query ="INSERT INTO `customer_order_info` VALUES('0','$user_id', '$product_name','$product_price ','$product_total', NOW())";
	$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
    // if($result)
    // {
    //     echo "<span style='color:blue;'>Данные добавлены</span>";
    // }
    mysqli_close($link);
	
	$prod_name = explode(',', $_POST['product_name3']);
	$prod_price = explode('€',$_POST['product_price3']);
	$product_info = get_all_product_and_cost($prod_name,$prod_price);

	$headers= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$devenez_membre = '<a style="color:#5DC0D5" href="http://upv.be/info-customer/" target=\"_blank\">Référence de la commande</a>';
	$upv_email = 'upv@upv.be';

						$message_to_upv =  '<hr>
						 <p>Un utilisateur souhaite acheter un produit sur le site.</p>
						 <p>Référence de la commande : '.$devenez_membre .'</p>
						<p>Voici les informations transmises</p>
						<p><br /> </p>
						<p>Prénom: '. $_POST['first_name'].' </p>
						<p>Nom: '. $_POST['last_name'].' </p>
						<p>Numéro d’OMV: '. $_POST['product_name3'].' </p>
						<p>Email: '. $_POST['email'].' </p>
						<p>Adresse de livraison:: '. $_POST['postcode'].' '.$_POST['city'].' '.$_POST['address_2'].' '.$_POST['address_1'].' </p>
						<p>Adresse de facturation: '. $_POST['shipping_postcode'].' '.$_POST['shipping_city'].' '.$_POST['shipping_address_1'].' </p>
						<p>Informations additionnelles: '. $_POST['order_comments'].' </p>
						<p><br /> </p>
						<p>Produits choisis:</p>
						<p>'. $product_info.' </p>
						<p>'.$_POST['product_total3'].'</p>
						<p><br /> </p>
						<p>Ceci est un message automatique, merci de ne pas y répondre.</p>
						</hr>';
						
						// if « membre UPV » field is yes (Oui)

						$message_to_customer = '<hr>
						<p>Chère cliente, cher client,</p>
						<p>Pour valider votre achat sur notre boutique, voici les informations nécessaires pour procéder à votre paiement :</p>
						<p><br /> </p>
					   <p>Numéro de compte : BE38 7420 2554 3872 </p>
					   <p>BIC : CREGBEBB</p>
					   <p>Montant : '.$_POST['product_total3'].' </p>
					   <p>Communication : Commande </p>
					   <p><br /> </p>
					   <p>Récapitulatif de votre commande:						</p>
					   <p><br /> </p>
					   <p>Référence de la commande : '.$devenez_membre .'</p>
					   <p>Prénom: '. $_POST['first_name'].' </p>
					   <p>Nom: '. $_POST['last_name'].' </p>
					   <p><br /> </p>
					   <p>Adresse de livraison: '. $_POST['postcode'].' '.$_POST['city'].' '.$_POST['address_2'].' '.$_POST['address_1'].' </p>
					   <p>Adresse de facturation: '. $_POST['shipping_postcode'].' '.$_POST['shipping_city'].' '.$_POST['shipping_address_1'].' </p>
					   <p>Informations additionnelles: '. $_POST['order_comments'].' </p>
					   <p><br /> </p>
					   <p>Produits choisis:</p>
						<p>'. $product_info.' </p>
						<p>'.$_POST['product_total3'].'</p>
						<p><br /> </p>
						<p>Ceci est un message automatique, merci de ne pas y répondre.</p>
					   <p>En cas de problème, veuillez prendre contact avec l’UPV à l’adresse '.$upv_email.' ou par téléphone au 067/ 21 21 11 </p>
					   </hr>';

					   mail ('upv@upv.be', "Nouvel achat sur la boutique", $message_to_upv,$headers);
					   mail ($_POST['email'], "Nouvel achat sur la boutique", $message_to_customer,$headers);

                $korzina = new WC_Cart();
				$korzina->empty_cart( $clear_persistent_cart = true );
				//wp_redirect('http://upv.be/boutique/');
				do_action( 'woocommerce_before_edit_account_form' ); 
			

}

defined( 'ABSPATH' ) || exit;

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
<div id="payment" class="woocommerce-checkout-payment">
	<?php if ( WC()->cart->needs_payment() ) : ?>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
			if ( ! empty( $available_gateways ) ) {
				foreach ( $available_gateways as $gateway ) {
					wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
				}
			} else {
				echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( '', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
			}
			?>
		</ul>
	<?php endif; ?>
	<div class="form-row place-order">
		<noscript>
			<?php esc_html_e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
			<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
		</noscript>
		<?php 
			
		
		?>
		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">commander</button>' ); // @codingStandardsIgnoreLine ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

		<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
	</div>
</div>

<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}

	?>
	<form id="form-submit-2" action="" method="post" >
	<input type="hidden" name="custom_form" value="custom_submit"/>
		<input id="first_name1" class="first_name2" name="first_name" type="hidden" value="">
		<input id="last_name" class="last_name2" name="last_name" type="hidden" value="">
		<input id="company" class="company2" name="company" type="hidden" value="">
		<input id="address_1" class="address_12" name="address_1" type="hidden" value="">
		<input id="address_2" class="address_22" name="address_2" type="hidden" value="">
		<input id="postcode" class="postcode2" name="postcode" type="hidden" value="">
		<input id="city" class="city2" name="city" type="hidden" value="">
		<input id="phone" class="phone2" name="phone" type="hidden" value="">
		<input id="email" class="email2" name="email" type="hidden" value="">
		<input id="order_comments" class="order_comments2" name="order_comments" type="hidden" value="">
		<input id="shipping_first_name" class="shipping_first_name2" name="shipping_first_name" type="hidden" value="">
		<input id="shipping_last_name" class="shipping_last_name2" name="shipping_last_name" type="hidden" value="">
		<input id="shipping_company" class="shipping_company2" name="shipping_company" type="hidden" value="">
		<input id="shipping_address_1" class="shipping_address_12" name="shipping_address_1" type="hidden" value="">
		<input id="shipping_postcode" class="shipping_postcode2" name="shipping_postcode" type="hidden" value="">
		<input id="shipping_city" class="shipping_city2" name="shipping_city" type="hidden" value="">

		<input id="product_name3" class="product_name3" name="product_name3" type="hidden" value="">
		<input id="product_price3" class="product_price3" name="product_price3" type="hidden" value="">
		<input id="product_total3" class="product_total3" name="product_total3" type="hidden" value="">
	</form>

	<script>
jQuery(document).ready(function(){
$(document).on("click","#place_order", function(e){
//console.log("1111");
 $(".first_name2").val($("#billing_first_name").val());
 $(".last_name2").val($("#billing_last_name").val());
 $(".company2").val($("#billing_company").val());
 $(".address_12").val($("#billing_address_1").val());
 $(".address_22").val($("#billing_address_2").val());
 $(".postcode2").val($("#billing_postcode").val());
 $(".city2").val($("#billing_city").val());
 $(".phone2").val($("#billing_phone").val());
 $(".email2").val($("#billing_email").val());
 $(".order_comments2").val($("#order_comments").val());

 $(".shipping_first_name2").val($("#shipping_first_name").val());
 $(".shipping_last_name2").val($("#shipping_last_name").val());
 $(".shipping_company2").val($("#shipping_company").val());
 $(".shipping_address_12").val($("#shipping_address_1").val());
 $(".shipping_postcode2").val($("#shipping_postcode").val());
 $(".shipping_city2").val($("#shipping_city").val());

	var	product_name = [];
    $(".shop_table.woocommerce-checkout-review-order-table").find('.cart_item').each(function(){		
		product_name.push($(this).find('.product_list_name').text().replace(/\s+/g,' ').trim());
	});

	var product_price = [];
	$(".shop_table.woocommerce-checkout-review-order-table").find('.cart_item').each(function(){		
		product_price.push($(this).find('.product_list_total').text().replace(/\s+/g,' ').trim());
	});

// total price
	var product_total = [];
	$(".shop_table.woocommerce-checkout-review-order-table").find('.order-total').each(function(){		
		product_total.push($(this).find('.total_price_all').text());
	});
	$(".product_name3").val(product_name);
	$(".product_price3").val(product_price);
	$(".product_total3").val(product_total);
$('#form-submit-2').submit();
})
});
	</script>

<?php
