<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<ul class="products columns-3">

	<li class="post-5348 product type-product status-publish has-post-thumbnail product_cat-uncategorized first instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-3-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-3-member/" rel="nofollow" data-quantity="1" data-product_id="5348" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-3-member/">Guide Prévention Chiens</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>20,00</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5375 product type-product status-publish has-post-thumbnail product_cat-uncategorized instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-14-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-14-member/" rel="nofollow" data-quantity="1" data-product_id="5375" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-14-member/">Carnets de vaccination sans nom</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>29,50</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5346 product type-product status-publish has-post-thumbnail product_cat-uncategorized last instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-2-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-2-member/" rel="nofollow" data-quantity="1" data-product_id="5346" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-2-member/">Guide Prévention Chiens</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>30,00</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5363 product type-product status-publish has-post-thumbnail product_cat-uncategorized first instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-8-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-8-member/" rel="nofollow" data-quantity="1" data-product_id="5363" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-8-member/">Carnets de santé du char sans nom</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>32,50</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5379 product type-product status-publish has-post-thumbnail product_cat-uncategorized instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-3-non-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-3-non-member/" rel="nofollow" data-quantity="1" data-product_id="5379" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-3-non-member/">Guide Prévention Chiens</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>40,00</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5401 product type-product status-publish has-post-thumbnail product_cat-uncategorized last instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-14-non-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading" style="display: none;"><i class="fa fa-check"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-14-non-member/" rel="nofollow" data-quantity="1" data-product_id="5401" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-14-non-member/">Carnets de vaccination sans nom</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>47,00</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5389 product type-product status-publish has-post-thumbnail product_cat-uncategorized first instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-8-non-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-8-non-member/" rel="nofollow" data-quantity="1" data-product_id="5389" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-8-non-member/">Carnets de santé du char sans nom</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>49,50</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-4667 product type-product status-publish has-post-thumbnail product_cat-uncategorized instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/acces-membre/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/acces-membre/" rel="nofollow" data-quantity="1" data-product_id="4667" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/acces-membre/">Guide Prévention Chiens</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>50,00</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5373 product type-product status-publish has-post-thumbnail product_cat-uncategorized last instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-13-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-13-member/" rel="nofollow" data-quantity="1" data-product_id="5373" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-13-member/">Carnets de vaccination sans nom</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>53,50</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li>
	<li class="post-5361 product type-product status-publish has-post-thumbnail product_cat-uncategorized first instock shipping-taxable purchasable product-type-variable">
	
    <div class="inside_prod">       
    
            <div class="image_prod">
            					<div class="image_prod_wrap">
					<a href="http://upv.be/product/test-product-7-member/">
							                		<img src="http://upv.be/wp-content/uploads/2018/11/UPV-e1541778407829.png">
	                	<span class="cart-loading"><i class="fa fa-refresh"></i></span>
	                 </a>
                 </div>
                 <div class="product_buttons_wrap clearfix">
    
                    <a href="http://upv.be/product/test-product-7-member/" rel="nofollow" data-quantity="1" data-product_id="5361" class="add_to_cart_button product_type_variable "><i class="fa fa-cog"></i>Select options</a>               
        
                </div>
            </div>

    
       
        
        <div class="product_details">
            
                <h3><a href="http://upv.be/product/test-product-7-member/">Carnets de santé du char sans nom</a></h3>               
                
                <div class="product_price">
    
                    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>60,00</span></span>
    
                </div>                
                       
		</div>

	</div>        

</li></ul>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
