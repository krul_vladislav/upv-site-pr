<?php if ( $options['page'] ): ?>
<div class="<?php echo $options['page']; ?> awpcp-page" id="classiwrapper">
<?php else: ?>
<div id="classiwrapper">
<?php endif; ?>
    <?php echo $before_content; ?>

    <?php if ( $options['show_intro_message'] ): ?>
    <div class="uiwelcome"><?php echo stripslashes_deep( get_awpcp_option( 'uiwelcome' ) ); ?></div>
    <?php endif; 

$user = wp_get_current_user();
$user_check = checkUserMember($user->ID); // function in file function.php  912 string

if($user->roles[0] == 'administrator' || $user_check[0]->meta_value == 'company'){
      ?>
    <li class="awpcp-classifieds-menu--menu-item awpcp-classifieds-menu--post-listing-menu-item">
        <a class="awpcp-classifieds-menu--menu-item-link" href="http://upv.be/awpcp/place-ad">Poster une annonce</a>
    </li>
    <li class="awpcp-classifieds-menu--menu-item awpcp-classifieds-menu--post-listing-menu-item">
        <a class="awpcp-classifieds-menu--menu-item-link" href="http://upv.be/awpcp/place-ad">Editer une annonce</a>
    </li>
<?php } ?>
    <li class="awpcp-classifieds-menu--menu-item awpcp-classifieds-menu--post-listing-menu-item">
        <p class="awpcp-classifieds-menu--menu-item-link" >Parcourir les annonces</p>
    </li>
    <li class="awpcp-classifieds-menu--menu-item awpcp-classifieds-menu--post-listing-menu-item">
        <p class="awpcp-classifieds-menu--menu-item-link" >Rechercher une annonce</p>
    </li>
    <?php if ( $options['show_menu_items'] ): ?>
    <?php echo awpcp_render_classifieds_bar( $options['classifieds_bar_components'] ); ?>
    <?php endif; ?>

    <?php echo implode( '', $before_pagination ); ?>
    <?php echo $pagination; ?>
    <?php echo $before_list; ?>

    <div class="awpcp-listings awpcp-clearboth">

        <?php
       
        if ( count( $items ) ): ?>
            <?php echo implode( ' ', $items ); 
           
            ?>
        <?php else: ?>
            <p><?php echo esc_html( __( 'There were no listings found.', 'another-wordpress-classifieds-plugin' ) ); ?></p>
        <?php endif;?>
    </div>

    <?php echo $pagination; ?>
    <?php echo implode( '', $after_pagination ); ?>
    <?php echo $after_content; ?>
</div>
<?php

