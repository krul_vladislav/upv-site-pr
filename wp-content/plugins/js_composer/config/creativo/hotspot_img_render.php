<?php 

return array(
		  "name" => __("Image With Hotspots", "js_composer"),
		  "base" => "cr_image_with_hotspots",
		  //"weight" => 2,
		  "icon" => "icon-wpb-single-image",
		  'category'                => 'Creativo',
		  "description" => __('Add Hotspots Over Your Image', 'js_composer'),
		  "params" => array(

		  	array(
				"type" => "attach_image",
				"class" => "",
				"heading" => "Image",
				"value" => "",
				"param_name" => "image",
				"description" => "Choose your image that will show the hotspots. <br/> You can then click on the image in the preview area to add your hotspots in the desired locations."
			),
			array(
		      "type" => "hotspot_image_preview",
		      "heading" => __("Preview", "js_composer"),
		      "param_name" => "preview",
		      "description" => __("Click to add - Drag to move - Edit content below <br/><br/> Note: this preview will not reflect hotspot style choices or show tooltips. <br/>This is only used as a visual guide for positioning.", "js_composer"),
		      "value" => ''
		    ),	
			 array(
		      "type" => "textarea_html",
		      "heading" => __("Hotspots", "js_composer"),
		      "param_name" => "content",
		      "description" => __("", "js_composer"),
		    ),	 

			/*array(
			"type" => "dropdown",
			"class" => "",
			'save_always' => true,
			"group" => "Style",
			"heading" => "Hotspot Style",
			"param_name" => "style",
			"description" => __("Select the overall style of your hotspots here", "js_composer"),
			"value" => array(
				"Color Pulse" => "color_pulse",
				"Transparent + Border" => "border"
			)),*/			
			
			/*array(
			"type" => "dropdown",
			"class" => "",
			'save_always' => true,
			"group" => "Style",
			"heading" => "Color",
			"param_name" => "color_2",
			"description" => __("Choose the color which the hotspot will use", "js_composer"),
			"dependency" => array('element' => "style", 'value' => 'border'),
			"value" => array(
				"Light" => "light",
				"Dark" => "dark",
			)),*/
			array(
			"type" => "dropdown",
			"class" => "",
			'save_always' => true,
			"group" => "Style",
			"heading" => "Hotspot Icon Design",
			"description" => __("Select the design of the Hotspot", "js_composer"),
			"param_name" => "hotspot_icon",
			"admin_label" => true,
			"value" => array(
				"Bullet" => "bullet",
				"Number" => "number"
			)),

			array(
			  "type" => "colorpicker",				  
			  "group" => "Style",
			  "heading" => __("Hotspot Background color", 'Creativo'),
			  "param_name" => "hotspot_bg_color",
			  "value" => '#ffffff', //Default Red color
			  "description" => __("Choose the background color of the Hotpot and the ripple effect", 'Creativo')
			),

			array(
			  "type" => "colorpicker",		  
			  "group" => "Style",
			  "heading" => __("Hotspot Number color", 'Creativo'),
			  "param_name" => "hotspot_color_number",
			  "value" => '#333333', //Default Red color
			  "description" => __("Choose the text color of the Hotpot Number. Only works if the Number option is used for Hotspot Icon Design", 'Creativo')
			),
			array(
			  "type" => "colorpicker",			  
			  "group" => "Style",
			  "heading" => __("Tooltip Text Color", 'Creativo'),
			  "param_name" => "tooltip_color",
			  "value" => '#333333', //Default Red color
			  "description" => __("Choose the text color of the Tooltip", 'Creativo')
			),
			array(
			  "type" => "colorpicker",			  
			  "group" => "Style",
			  "heading" => __("Tooltip Background Color", 'Creativo'),
			  "param_name" => "tooltip_bg_color",
			  "value" => '#ffffff', //Default Red color
			  "description" => __("Choose the background color of the Tooltip", 'Creativo')
			),
			/*array(
			"type" => "dropdown",
			"class" => "",
			'save_always' => true,
			"group" => "Style",
			"heading" => "Hotspot Size",
			"param_name" => "size",
			"description" => __("Select the size of your hotspots here", "js_composer"),
			"value" => array(
				"Small" => "small",
				"Medium" => "medium",
				"Large" => "large"
			)),*/
			array(
			"type" => "dropdown",
			"class" => "",
			'save_always' => true,
			"group" => "Style",
			"heading" => "Tooltip Display",
			"param_name" => "tooltip",
			"description" => __("Choose how the tooltips will be displayed to the user", "js_composer"),
			"value" => array(
				"On Hover" => "hover",
				//"Show On Click" => "click",
				"Always Show" => "always_show"
			)),
			array(
			"type" => "dropdown",
			"class" => "",
			'save_always' => true,
			"group" => "Style",
			"heading" => "Tooltip Shadow",
			"param_name" => "tooltip_shadow",
			"description" => __("Select the shadow size for your tooltip", "js_composer"),
			"value" => array(__("None", "js_composer") => "none", __("Small Shadow", "js_composer") => "small_depth", __("Medium Shadow", "js_composer") => "medium_depth", __("Large Shadow", "js_composer") => "large_depth"),
			),			
		  )
		);
