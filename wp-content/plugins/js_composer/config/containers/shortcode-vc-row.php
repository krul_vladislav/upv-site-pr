<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

return array(
	'name' => __( 'Row', 'js_composer' ),
	'is_container' => true,
	'icon' => 'icon-wpb-row',
	'show_settings_on_create' => false,
	'category' => __( 'Content', 'js_composer' ),
	'class' => 'vc_main-sortable-element',
	'description' => __( 'Place content elements inside the row', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => __( 'Row width', 'js_composer' ),
			'param_name' => 'layout', //former full_width
			'value' => array(
			/*
				__( 'Default', 'js_composer' ) => '',
				__( 'Stretch row', 'js_composer' ) => 'stretch_row',
				__( 'Stretch row and content', 'js_composer' ) => 'stretch_row_content',
				__( 'Stretch row and content (no paddings)', 'js_composer' ) => 'stretch_row_content_no_spaces',
			*/
				__('Normal','js_composer') => 'normal',
				__('Normal without padding left and right','js_composer') => 'normal_no_spaces',
				__('Wide','js_composer') => 'wide',				
				__('Wide without spaces and no padding','js_composer') => 'stretch_row_content_no_spaces',
			
			),
				
			'description' => __( 'Select stretching options for row and content (Note: stretched may not work properly if parent container has "overflow: hidden" CSS property).', 'js_composer' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Columns gap', 'js_composer' ),
			'param_name' => 'gap',
			'value' => array(
				'0px' => '0',
				'1px' => '1',
				'2px' => '2',
				'3px' => '3',
				'4px' => '4',
				'5px' => '5',
				'10px' => '10',
				'15px' => '15',
				'20px' => '20',
				'25px' => '25',
				'30px' => '30',
				'35px' => '35',
			),
			'std' => '0',
			'description' => __( 'Select gap between columns in row.', 'js_composer' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => __( 'Full height row?', 'js_composer' ),
			'param_name' => 'fullscreen', // used to be full_height 
			'description' => __( 'If checked row will be set to full height.', 'js_composer' ),
			'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Columns position', 'js_composer' ),
			'param_name' => 'columns_placement',
			'value' => array(
				__( 'Middle', 'js_composer' ) => 'middle',
				__( 'Top', 'js_composer' ) => 'top',
				__( 'Bottom', 'js_composer' ) => 'bottom',
				__( 'Stretch', 'js_composer' ) => 'stretch',
			),
			'description' => __( 'Select columns position within row.', 'js_composer' ),
			'dependency' => array(
				'element' => 'fullscreen', // used to be full_height
				'not_empty' => true,
			),
		),
		array(
			'type' => 'checkbox',
			'heading' => __( 'Equal height', 'js_composer' ),
			'param_name' => 'eq_height', // used to be equal_height 
			'description' => __( 'If checked columns will be set to equal height.', 'js_composer' ),
			'value' => array( __( 'Yes', 'js_composer' ) => 'yes' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Content position', 'js_composer' ),
			'param_name' => 'content_placement',
			'value' => array(
				__( 'Default', 'js_composer' ) => '',
				__( 'Top', 'js_composer' ) => 'top',
				__( 'Middle', 'js_composer' ) => 'middle',
				__( 'Bottom', 'js_composer' ) => 'bottom',
			),
			'description' => __( 'Select content position within columns.', 'js_composer' ),
		),
		array(
			'type' => 'css_editor',
			'heading' => __( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => __( 'Design Options', 'js_composer' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => __( 'Enable video background?', 'js_composer' ),
			'param_name' => 'video_bg',
			'description' => __( 'To enable video background you will have to first set the links to your video files in .webm and .mp4 format using the fields below. You can upload your .mp4 and .webm files under the Media -> Library section of WordPress. <br/>* IMPORTANT: You can also use a YouTube Video instead of using .webm and .mp4 files', 'js_composer' ),
			'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
			'group' => __( 'Video Background', 'js_composer' )
		),
		array(
			"type" => "textfield",
			"heading" => __("MP4 video file url", "Creativo"),
			"param_name" => "mp4",
			"value" => "",
			'group' => __( 'Video Background', 'js_composer' ),
			"description" => __("Paste here the link to your MP4 video file.", "Creativo"),
			'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			"type" => "textfield",
			"heading" => __("WebM video file url", "Creativo"),
			"param_name" => "webm",
			"value" => "",
			'group' => __( 'Video Background', 'js_composer' ),
			"description" => __("Paste here the link to your WebM video file.", "Creativo"),
			'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'YouTube link', 'js_composer' ),
			'param_name' => 'video_bg_url',
			
			'description' => __( 'Add YouTube link. E.g: https://www.youtube.com/watch?v=lMJXxhRFO1k', 'js_composer' ),
			'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
			'group' => __( 'Video Background', 'js_composer' )
		),
		array(
		  "type" => "colorpicker",		  
		  "class" => "",
		  "heading" => __("Video Overlay", 'Creativo'),
		  "param_name" => "video_overlay",
		  "value" => 'rgba(0,0,0,0.45)', //Default Red color
		  'group' => __( 'Video Background', 'js_composer' ),
		  "description" => __("Choose the color and opacity of the video overlay", 'Creativo'),
		  'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),	
		array(
		  "type" => "attach_image",
		  "heading" => __("Video Placeholder Image for Mobile Devices", "Creativo"),
		  "param_name" => "video_placeholder",
		  "value" => "",
		  'group' => __( 'Video Background', 'js_composer' ),
		  "description" => __("Upload an image to be used as a background image for mobile devices.", "Creativo"),
		  'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),/*
		array(
			'type' => 'dropdown',
			'heading' => __( 'Parallax', 'js_composer' ),
			'param_name' => 'video_bg_parallax',
			'group' => __( 'Video Background', 'js_composer' ),
			'value' => array(
				__( 'None', 'js_composer' ) => '',
				__( 'Simple', 'js_composer' ) => 'content-moving',
				__( 'With fade', 'js_composer' ) => 'content-moving-fade',
			),
			'description' => __( 'Add parallax type background for row.', 'js_composer' ),
			'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Parallax speed', 'js_composer' ),
			'param_name' => 'parallax_speed_video',
			'group' => __( 'Video Background', 'js_composer' ),
			'value' => '1.5',
			'description' => __( 'Enter parallax speed ratio (Note: Default value is 1.5, min value is 1)', 'js_composer' ),
			'dependency' => array(
				'element' => 'video_bg_parallax',
				'not_empty' => true,
			),
		),*/
		array(
			'type' => 'checkbox',
			'heading' => __( 'Enable Parallax effect', 'js_composer' ),
			'param_name' => 'parallax',
			'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
			'description' => __( 'To enable parallax effect you will have to first set a Background Image under Design Options tab.', 'js_composer' ),
			'group' => __( 'Parallax', 'js_composer' ),
			'dependency' => array(
				'element' => 'video_bg',
				'is_empty' => true,
			),
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Parallax Image Size', 'js_composer' ),
			'param_name' => 'parallax_sizing',
			'group' => __( 'Parallax', 'js_composer' ),
			'value' => array(
				__( 'Cover', 'js_composer' ) => 'cover',
				__( '100%', 'js_composer' ) => '100%',				
			),
			'description' => __( 'Select how the Parallax Image Width will be calculated. Sometimes the 100% works better than cover mode.', 'js_composer' ),
			'dependency' => array(
				'element' => 'parallax',
				'not_empty' => true,
			),
		),
		/*
		array(
			'type' => 'dropdown',
			'heading' => __( 'Parallax Type', 'js_composer' ),
			'param_name' => 'parallax_type',
			'value' => array(
				//__( 'None', 'js_composer' ) => '',
				__( 'Simple', 'js_composer' ) => 'content-moving',
				__( 'With fade', 'js_composer' ) => 'content-moving-fade',
			),			
			
			'dependency' => array(
				'element' => 'parallax',
				'not_empty' => true,
			),
			'group' => __( 'Parallax', 'js_composer' )
		),*//*
		array(
			'type' => 'attach_image',
			'heading' => __( 'Image', 'js_composer' ),
			'param_name' => 'parallax_image',
			'value' => '',
			'description' => __( 'Select image from media library.', 'js_composer' ),
			'dependency' => array(
				'element' => 'parallax',
				'not_empty' => true,
			),
			'group' => __( 'Parallax', 'js_composer' )
		),*/
		/*
		array(
			'type' => 'textfield',
			'heading' => __( 'Parallax speed', 'js_composer' ),
			'param_name' => 'parallax_speed_bg',
			'value' => '1.5',
			'description' => __( 'Enter parallax speed ratio (Note: Default value is 1.5, min value is 1)', 'js_composer' ),
			'dependency' => array(
				'element' => 'parallax',
				'not_empty' => true,
			),
			'group' => __( 'Parallax', 'js_composer' )
		),*/
		array(
			'type' => 'textfield',
			'heading' => __( 'Section ID', 'js_composer' ),
			'param_name' => 'section_id',
			'description' => sprintf( __( 'Enter row ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'js_composer' ), 'http://www.w3schools.com/tags/att_global_id.asp' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => __( 'Disable row', 'js_composer' ),
			'param_name' => 'disable_element',
			// Inner param name.
			'description' => __( 'If checked the row won\'t be visible on the public side of your website. You can switch it back any time.', 'js_composer' ),
			'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
		),

			array(
				"type" => "checkbox",
				"class" => "",
				"group" => "Shape Divider",
				"heading" => "Enable Top Shape Divider",
				"value" => array("Yes, please" => "true" ),
				"param_name" => "enable_shape_divider",
				"description" => ""
			),
			array(
				"type" => "cr_img_select",
				"class" => "",
				'save_always' => true,
				'dependency' => array(
					'element' => 'enable_shape_divider', 'value' => 'true'
				),
				"heading" => "Shape Type",
				"param_name" => "shape_type",
				"group" => "Shape Divider",
				"options" => array(					
					'fan' => array( __('Fan', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/fan_cr.jpg"),
					'fan2' => array( __('Fan 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/fan2_cr.jpg"),
					'slant-2' => array( __('Slant', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/slant_cr.jpg"),
					'tilt' => array( __('Tilt', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/tilt_cr.jpg"),					
					'triangle' => array( __('Triangle', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/triangle_cr.jpg"),
					'triangle_op' => array( __('Triangle Opacity', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/triangle_opacity_cr.jpg"),
					'rocks' => array( __('Rocks', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/rocks_cr.jpg"),
					'rocks_op' => array( __('Rocks Opacity', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/rocks2_cr.jpg"),
					'ramp' => array( __('Ramp', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/ramp_cr.jpg"),
					'ramp_op' => array( __('Ramp Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/ramp_op_cr.jpg"),
					'curve' => array( __('Curve', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/curve_down_cr.jpg"),
					'curve_opacity' => array( __('Curve Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/curve_opacity_cr.jpg"),					
					'curve_asym' => array( __('Curve Asym.', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/curve_asym_cr.jpg"),
					"mountains" => array( __('Mountains', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/mountains_cr.jpg"),						
					'wave1' => array( __('Wave', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/wave.jpg"),
					'wave2' => array( __('Wave Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/wave_opacity.jpg"),
					'waves' => array( __('Waves', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/waves_no_opacity_cr.jpg"),
					'waves_opacity' => array( __('Waves Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/waves_cr.jpg"),
					'waves_opacity_alt' => array( __('Waves Opacity 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/waves_opacity_cr.jpg"),
					'waves_opacity_3' => array( __('Waves Opacity 3', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/waves_opacity_3.jpg"),
					'asym' => array( __('Asymetric 1', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/asym.jpg"),
					'asym2' => array( __('Asymetric 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/asym_2.jpg"),
					'graph1' => array( __('Graph 1', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/graph_1.jpg"),
					'graph2' => array( __('Graph 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/graph_2.jpg"),
					'clouds' => array( __('Clouds', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/clouds_cr.jpg"),
					'clouds2' => array( __('Clouds 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/clouds_2.jpg"),
					'clouds3' => array( __('Clouds 3', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/clouds_3.jpg"),
					"speech" => array( __('Speech', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/speech_cr.jpg")
				),
			),
			/*
			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Shape Divider Position",
				"param_name" => "shape_divider_position",
				"group" => "Shape Divider",
				"value" => array(
					"Bottom" => "bottom",
					"Top" => "top",
					"Bottom & Top" => 'both'
				),
			),*/
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Shape Divider Color",
				"param_name" => "shape_divider_color",
				"value" => "#0bbf50",
				"group" => "Shape Divider",
				'dependency' => array(
					'element' => 'enable_shape_divider', 'value' => 'true'
				),
				"description" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"group" => "Shape Divider",
				"heading" => "Shape Divider Height",
				"param_name" => "shape_divider_height",
				"value" => "120",
				'dependency' => array(
					'element' => 'enable_shape_divider', 'value' => 'true'
				),
				"description" => "Enter an optional custom height for your shape divider in pixels without the \"px\", e.g. 50"
			),/*
			array(
				"type" => "checkbox",
				"class" => "",
				"group" => "Shape Divider",
				"heading" => "Bring to front?",
				"value" => array("Yes, please" => "true" ),
				"param_name" => "shape_divider_bring_to_front",
				'dependency' => array(
					'element' => 'enable_shape_divider', 'value' => 'true'
				),
				"description" => "This will bring the shape divider to the top layer, placing it on top of any content it intersects/"
			),*/
			array(
				"type" => "checkbox",
				"class" => "",
				"group" => "Shape Divider",
				"heading" => "Enable Bottom Shape Divider",
				"value" => array("Yes, please" => "true" ),
				"param_name" => "enable_bot_shape_divider",
				"description" => ""
			),
			array(
				"type" => "cr_img_select",
				"class" => "",
				'save_always' => true,
				'dependency' => array(
					'element' => 'enable_bot_shape_divider', 'value' => 'true'
				),
				"heading" => "Shape Type",
				"param_name" => "bot_shape_type",
				"group" => "Shape Divider",
				"options" => array(					
					'fan' => array( __('Fan', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/fan_cr.jpg"),
					'fan2' => array( __('Fan 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/fan2_cr.jpg"),
					'slant-2' => array( __('Slant', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/slant_cr.jpg"),
					'tilt' => array( __('Tilt', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/tilt_cr.jpg"),					
					'triangle' => array( __('Triangle', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/bottom/triangle_cr.jpg"),
					'triangle_op' => array( __('Triangle Opacity', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/bottom/triangle_opacity_cr.jpg"),
					'rocks' => array( __('Rocks', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/bottom/rocks_cr.jpg"),
					'rocks_op' => array( __('Rocks Opacity', 'js_composer') => get_template_directory_uri()."/images/vc/shape_dividers/bottom/rocks2_cr.jpg"),
					'ramp' => array( __('Ramp', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/ramp_cr.jpg"),
					'ramp_op' => array( __('Ramp Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/ramp_op_cr.jpg"),
					'curve' => array( __('Curve', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/curve_down_cr.jpg"),
					'curve_opacity' => array( __('Curve Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/curve_opacity_cr.jpg"),					
					'curve_asym' => array( __('Curve Asym.', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/curve_asym_cr.jpg"),
					"mountains" => array( __('Mountains', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/mountains_cr.jpg"),						
					'wave1' => array( __('Wave', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/wave.jpg"),
					'wave2' => array( __('Wave Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/wave_opacity.jpg"),
					'waves' => array( __('Waves', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/waves_no_opacity_cr.jpg"),
					'waves_opacity' => array( __('Waves Opacity', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/waves_cr.jpg"),
					'waves_opacity_alt' => array( __('Waves Opacity 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/waves_opacity_cr.jpg"),
					'waves_opacity_3' => array( __('Waves Opacity 3', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/waves_opacity_3.jpg"),
					'asym' => array( __('Asymetric 1', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/asym.jpg"),
					'asym2' => array( __('Asymetric 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/asym_2.jpg"),
					'graph1' => array( __('Graph 1', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/graph_1.jpg"),
					'graph2' => array( __('Graph 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/graph_2.jpg"),
					'clouds' => array( __('Clouds', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/clouds_cr.jpg"),
					'clouds2' => array( __('Clouds 2', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/clouds_2.jpg"),
					'clouds3' => array( __('Clouds 3', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/clouds_3.jpg"),
					"speech" => array( __('Speech', "js_composer") => get_template_directory_uri()."/images/vc/shape_dividers/bottom/speech_cr.jpg")
				),
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Shape Divider Color",
				"param_name" => "bot_shape_divider_color",
				"value" => "#0bbf50",
				"group" => "Shape Divider",
				'dependency' => array(
					'element' => 'enable_bot_shape_divider', 'value' => 'true'
				),
				"description" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"group" => "Shape Divider",
				"heading" => "Shape Divider Height",
				"param_name" => "bot_shape_divider_height",
				"value" => "120",
				'dependency' => array(
					'element' => 'enable_bot_shape_divider', 'value' => 'true'
				),
				"description" => "Enter an optional custom height for your shape divider in pixels without the \"px\", e.g. 50"
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"group" => "Shape Divider",
				"heading" => "Bring to front?",
				"value" => array("Yes, please" => "true" ),
				"param_name" => "bot_shape_divider_bring_to_front",				
				"description" => "This will force the shape dividers to appear above your content."
			),				

			array(
				"type" => "checkbox",				
				"group" => "Color Overlay",
				"heading" => "Enable Gradient",
				"value" => array("Yes, please" => "true" ),
				"param_name" => "enable_gradient",
				"description" => "Check this option to create a gradient overlay. Leave unchecked to use only one color overlay"
			),	

			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Color Overlay",
				"param_name" => "col_overlay",
				"value" => "",
				"group" => "Color Overlay",				
				"description" => "Set the overlay color. If Gradient option enabled, this will be used as the primary gradient color."
			),

			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Color Overlay 2",
				"param_name" => "col_overlay2",
				"value" => "",
				"group" => "Color Overlay",
				'dependency' => array(
					'element' => 'enable_gradient', 'value' => 'true'
				),
				"description" => ""
			),	

			array(
				"type" => "cr_slider",
				"class" => "",
				"heading" => "Gradient Direction in degrees",
				"param_name" => "cr_slider_deg",
				"value" => "90",
				"group" => "Color Overlay",				
	            "min" => "0",
	            "max" => "360",
	            "step" => "5",
	            'unit' => 'degrees',
				'dependency' => array(
					'element' => 'enable_gradient', 'value' => 'true'
				),
				"description" => ""
			),
		vc_map_add_css_animation( false ),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
		),
		
	),
	'js_view' => 'VcRowView',
);