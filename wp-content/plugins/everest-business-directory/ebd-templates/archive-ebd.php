<?php

/**
 * The Template for displaying directory archives
 *
 * This template can be overridden by copying it to yourtheme/ebd-templates/archive-ebd.php.
 *
 * @package everest-business-directory/ebd-templates
 * @version 1.0.0
 */
defined('ABSPATH') || exit;

get_header();

/**
 * ebd_main_outer_wrap hook
 *
 * @hooked EBD_Directory_Display::ebd_main_outer_wrap_start - 10 (outputs main outer wrap just below header)
 */
do_action('ebd_main_outer_wrap_start');

/**
 * Hook: ebd_before_main_content.
 *
 * @hooked EBD_Directory_Display::ebd_output_content_wrapper - 10 (outputs opening divs for the content)
 */
do_action('ebd_before_main_content');

/**
 * Hook: ebd_load_archive_templates
 *
 * @hooked EBD_Directory_Display::
 */
do_action('ebd_load_archive_templates');



/**
 * Hook: ebd_after_main_content.
 *
 * @hooked EBD_Directory_Display::ebd_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action('ebd_after_main_content');

/**
 * Hook: ebd_sidebar.
 *
 *
 */
do_action('ebd_sidebar');

/**
 * ebd_main_outer_wrap_end hook
 *
 * @hooked EBD_Directory_Display::ebd_main_outer_wrap_end - 10 (outputs main outer wrap just below header)
 */
do_action('ebd_main_outer_wrap_end');

get_footer();
