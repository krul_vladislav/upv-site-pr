<?php
global $ebd_settings;
$no_result_text = (!empty($ebd_settings[ 'general' ][ 'no_result_text' ])) ? esc_attr($ebd_settings[ 'general' ][ 'no_result_text' ]) : __('No Results Found', 'everest-business-directory');
?>
<div class="ebd-no-results-img">
	<img src="<?php echo EBD_IMG_URL . 'no-results-found.png'; ?>">
</div>
<h2 class="ebd-no-result"><?php echo $no_result_text; ?></h2>
<?php
