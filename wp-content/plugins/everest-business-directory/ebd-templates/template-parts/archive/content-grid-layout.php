<?php
global $ebd_directory_display;
global $ebd_settings;
global $ebd_directory_expiry;
$current_taxonomy_obj = get_queried_object();
$listing_template = (!empty($ebd_settings[ 'general' ][ 'grid_layout_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_layout_template' ]) : 'template-1';
$listing_template_class = 'ebd-grid-' . $listing_template;
$_GET[ 'layout' ] = $layout = 'grid';
$grid_column = (!empty($ebd_settings[ 'general' ][ 'grid_column' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_column' ]) : 2;
$grid_column_class = 'ebd-grid-column-' . $grid_column;
$image_original_size = (!empty($ebd_settings[ 'general' ][ 'image_original_size' ])) ? 'ebd-original-image-size' : '';
?>

<div class="ebd-directory-listing-wrap <?php echo $listing_template_class; ?> ebd-grid-layout <?php echo $grid_column_class; ?> <?php echo $image_original_size; ?>" <?php $ebd_directory_display->display_none($current_view, 'grid'); ?>>
    <div class="ebd-main-grid-wrap">
        <?php
        wp_reset_query();
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        //   var_dump($paged);
        $per_page = (!empty($ebd_settings[ 'general' ][ 'per_page' ])) ? intval($ebd_settings[ 'general' ][ 'per_page' ]) : 10;
        $directory_listing_args = array( 'posts_per_page' => $per_page, 'post_status' => 'publish', 'post_type' => 'ebd', 'paged' => $paged );
        if ( is_tax('ebd-categories') ) {
            $directory_listing_args[ 'tax_query' ][] = array( 'taxonomy' => 'ebd-categories', 'field' => 'slug', 'terms' => $current_taxonomy_obj->slug );
        }
        if ( is_tax('ebd-tags') ) {
            $directory_listing_args[ 'tax_query' ][] = array( 'taxonomy' => 'ebd-tags', 'field' => 'slug', 'terms' => $current_taxonomy_obj->slug );
        }
        if ( $ebd_directory_expiry ) {
            $directory_listing_args[ 'meta_query' ][] = array( 'key' => '_ebd_expiry_date', 'value' => date('Y-m-d'), 'compare' => '>=', 'type' => 'DATE' );
        }
        $directory_listing_args = apply_filters('ebd_query_args', $directory_listing_args);
        $directory_listing_query = new WP_Query($directory_listing_args);
        $data[ 'directory_listing_query' ] = $directory_listing_query;
        $data[ 'listing' ] = true;
        if ( $directory_listing_query->have_posts() ) {
            $directory_counter = 0;
            while ( $directory_listing_query->have_posts() ) {

                $directory_listing_query->the_post();

                $directory_counter++;

                ebd_load_template_part('archive/grid/content', 'grid-' . $listing_template, $data);
            }
        }

        /**
         * Pagination
         */
        ebd_load_template_part('common/pagination', '', $data);
        ?>
    </div>

</div>
