<?php
global $ebd_settings;
global $ebd_directory_display;
$current_taxonomy_obj = get_queried_object();
$_GET[ 'layout' ] = $layout = 'map';
$map_width = (!empty($ebd_settings[ 'map' ][ 'map_width' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_width' ]) : '100%';
$map_height = (!empty($ebd_settings[ 'map' ][ 'map_height' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_height' ]) : '500px';
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=en&key=<?php echo (!empty($ebd_settings[ 'map' ][ 'map_api_key' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_api_key' ]) : ''; ?>" type="text/javascript"></script>
<div class="ebd-map-view-wrap ebd-map-layout ebd-directory-listing-wrap" <?php $ebd_directory_display->display_none($current_view, 'map'); ?>>
    <?php
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $per_page = (!empty($ebd_settings[ 'general' ][ 'per_page' ])) ? intval($ebd_settings[ 'general' ][ 'per_page' ]) : 10;
    $directory_listing_args = array( 'posts_per_page' => $per_page, 'post_status' => 'publish', 'post_type' => 'ebd', 'paged' => $paged );
    if ( is_tax('ebd-categories') ) {
        $directory_listing_args[ 'tax_query' ][] = array( 'taxonomy' => 'ebd-categories', 'field' => 'slug', 'terms' => $current_taxonomy_obj->slug );
    }
    if ( is_tax('ebd-tags') ) {
        $directory_listing_args[ 'tax_query' ][] = array( 'taxonomy' => 'ebd-tags', 'field' => 'slug', 'terms' => $current_taxonomy_obj->slug );
    }

    //  $ebd_directory_display->print_array($directory_listing_args);
    global $ebd_directory_expiry;
    if ( $ebd_directory_expiry ) {
        $directory_listing_args[ 'meta_query' ][] = array( 'key' => '_ebd_expiry_date', 'value' => date('Y-m-d'), 'compare' => '>=', 'type' => 'DATE' );
    }
    $directory_listing_args = apply_filters('ebd_query_args', $directory_listing_args);
    $directory_listing_query = new WP_Query($directory_listing_args);
    $client = new Teamleader($ebd_settings['map']['map_api_key']);
    $loc_client = $client->getContactsFromDBclient();
    //$ebd_directory_display->print_array($directory_listing_query);

    $x =$client->getContactsFromDB2($directory_listing_args);
    print_r($x);

    $directory_array = array();
    $data[ 'directory_listing_query' ] = $directory_listing_query;
    if ( $directory_listing_query->have_posts() ) {
        $directory_counter = 0;
        while ( $directory_listing_query->have_posts() ) {
            $directory_listing_query->the_post();
            include(EBD_PATH . 'inc/cores/list-variables.php');
            global $ebd_settings;
            $directory_id = get_the_ID();
            $featured_directory = get_post_meta($directory_id, '_ebd_featured', true);
            $featured_text = (!empty($ebd_settings[ 'general' ][ 'featured_text' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_text' ]) : '';
            $featured_template = (!empty($ebd_settings[ 'general' ][ 'featured_ribbon_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_ribbon_template' ]) : 'template-1';
            $featured_template = (!empty($featured_directory)) ? $featured_template : '';
            $featured_template_class = (!empty($featured_template)) ? 'ebd-ribbon-' . $featured_template : '';
            $ribbon_html = '';
            if ( $featured_directory ) {
                $ribbon_html .= '<span class="ebd-featured-tag">' . $featured_text . '</span>';
            }
            $info = '<div class="ebd-map-info ' . $featured_template_class . '">'
                    . '<div class="ebd-info-image">' . $ribbon_html . $ebd_directory_display->the_featured_image_url('medium') . '</div>'
                    . '<div class="ebd-info-content-wrap23">'
                    . '<div class="ebd-info-title"><a href="' . $ebd_directory_display->get_directory_url() . '">' . $directory_title . '</a></div>'
                    . '<div clas class="ebd-info-address"><span class="ebd-address-icon"></span>' . $address . '</div>'
                    . '<div clas class="ebd-info-phone"><span class="ebd-phone-icon"></span><a href="tel:' . $phone_number . '">' . $phone_number . '</a></div>'
                    . '<div clas class="ebd-info-website"><span class="ebd-website-icon"></span><a href="mailto:' . $email_address . '" target="_blank">' . $email_address . '</a></div>'
                    . '</div>'
                    . '</div>';
            $info = apply_filters('ebd_map_info', $info, $directory_id);
            die('asdsad'.$info);
            $normal_marker_id = (!empty($ebd_settings[ 'map' ][ 'map_normal_marker' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_normal_marker' ]) : 1;
            $featured_marker_id = (!empty($ebd_settings[ 'map' ][ 'map_featured_marker' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_featured_marker' ]) : 1;
            $normal_marker = EBD_URL . 'images/markers/normal/map-marker-' . $normal_marker_id . '.png';
            $featured_marker = EBD_URL . 'images/markers/featured/map-marker-featured-' . $featured_marker_id . '.png';
            $marker = (!empty($featured_directory)) ? $featured_marker : $normal_marker;
            $featured = (!empty($featured_directory)) ? 1 : 0;
            $directory_array[] = array( $info, $latitude, $longitude, $directory_counter - 1, $marker, $featured );
        }
    }
    if ( empty($directory_array) || empty($directory_array[ 0 ][ 1 ]) || empty($directory_array[ 0 ][ 1 ]) ) {
        ?>
        <div id="ebd-map" style="height: <?php echo $map_height; ?>;width:<?php echo $map_width; ?>;" data-base-latitude="<?php echo esc_attr($directory_array[$x[0]][1]); ?>" data-base-longitude="<?php echo esc_attr($directory_array[$x[0]][1]); ?>" data-zoom-level="<?php echo intval($ebd_settings[ 'map' ][ 'zoom_level' ]); ?>"></div>
        <?php
    } else {
        $ebd_directory_display->print_array($directory_array);
        ?>
        <div id="ebd-map" style="height: <?php echo $map_height; ?>;width:<?php echo $map_width; ?>;" data-base-latitude="<?php echo esc_attr($directory_array[$x[0]][2]); ?>" data-base-longitude="<?php echo esc_attr($directory_array[$x[0]][2]); ?>" data-zoom-level="<?php echo intval($ebd_settings[ 'map' ][ 'zoom_level' ]); ?>"></div>
        <?php
    }
    /**
     * Pagination
     */
    ebd_load_template_part('common/pagination', '', $data);
    ?>
    <textarea id="ebd-map-locations" style="display:none;"><?php echo json_encode($directory_array); ?></textarea>
</div>
