<?php
/**
 * Contains all the variables used in the template files
 *
 * Please don't remove this
 */
include(EBD_PATH . 'inc/cores/list-variables.php');
global $ebd_directory_display;
?>
<div class="ebd-each-directory <?php echo $image_class; ?> <?php echo $featured_directory_class; ?> <?php echo $featured_template_class; ?>">
    <div class="ebd-feat-img-wrap">
        <?php
        if ( !(empty($featured_directory) || empty($featured_text)) ) {
            $featured_template = (!empty($ebd_settings[ 'general' ][ 'featured_ribbon_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_ribbon_template' ]) : 'template-1';
            ?>
            <span class="ebd-featured-tag"><?php echo $featured_text; ?></span>
        <?php } ?>
        <div class="ebd-directory-image">
            <a href="<?php $ebd_directory_display->the_directory_url(); ?>"><?php
                $ebd_directory_display->the_featured_image();
                ?>
            </a>
        </div>
        <div class="ebd-other-elements-wrap">
            <div class="ebd-directory-category">
                <?php
                if ( !empty($directory_categories) ) {
                    foreach ( $directory_categories as $directory_category ) {
                        ?>
                        <span class="ebd-each-category"><a href="<?php $ebd_directory_display->the_category_url($directory_category->term_id); ?>"><?php echo $directory_category->name; ?></a></span>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="ebd-directory-location">
                <?php if ( !empty($address) ) { ?><div class="ebd-location-sim-styl ebd-address"><span class="ebd-tool-tip-main"><span><?php echo $address; ?></span></span></div><?php } ?>
                <?php if ( !empty($phone_number) ) { ?><div class="ebd-location-sim-styl ebd-phn-number"><span class="ebd-tool-tip-main"><span><a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></span></span></div><?php } ?>
                <?php if ( !empty($email_address) ) { ?><div class="ebd-location-sim-styl ebd-email-add"><span class="ebd-tool-tip-main"><span><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></span></span></div><?php } ?>
                <?php if ( !empty($website) ) { ?><div class="ebd-location-sim-styl ebd-webadd"><span class="ebd-tool-tip-main"><span><a href="<?php echo esc_url($website); ?>"><?php echo $website; ?></a></span></span></div><?php } ?>
            </div>
            <?php if ( !empty($expiry_date) ) { ?>
                <div class="ebd-expiry-date">
                    <span class="ebd-exp-date-wrap"><span><?php echo $expiry_date; ?></span></span>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="ebd-list-details-wrap">
        <div class="ebd-directory-tags">
            <span class="ebd-tag-title">Tags</span>
            <?php
            if ( !empty($directory_tags) ) {
                foreach ( $directory_tags as $directory_tag ) {
                    ?>
                    <span class="ebd-each-tag"><a href="<?php $ebd_directory_display->the_tag_url($directory_tag->term_id); ?>"><?php echo $directory_tag->name; ?></a></span>
                    <?php
                }
            }
            ?>
        </div>
        <h2 class="ebd-list-title"><a href="<?php $ebd_directory_display->the_directory_url(); ?>"><?php the_title(); ?></a></h2>
        <div class="ebd-listing-description"><?php the_excerpt(); ?></div>

        <div class="ebd-directory-social">
            <span class="ebd-social-title"><i class="fa fa-share-alt"></i></span>
            <?php if ( !empty($facebook) ) { ?><span class="ebd-social-links ebd-facebook"><a href="<?php echo esc_url($facebook); ?>"><i class="fa fa-facebook-f"></i></a></span><?php } ?>
            <?php if ( !empty($twitter) ) { ?><span class="ebd-social-links ebd-twitter"><a href="<?php echo esc_url($twitter); ?>"><i class="fa fa-twitter"></i></a></span><?php } ?>
            <?php if ( !empty($googleplus) ) { ?><span class="ebd-social-links ebd-googleplus"><a href="<?php echo esc_url($googleplus); ?>"><i class="fa fa-google-plus"></i></a></span><?php } ?>
            <?php if ( !empty($instagram) ) { ?><span class="ebd-social-links ebd-instagram"><a href="<?php echo esc_url($instagram); ?>"><i class="fa fa-instagram"></i></a></span><?php } ?>
            <?php if ( !empty($youtube) ) { ?><span class="ebd-social-links ebd-youtube"><a href="<?php echo esc_url($youtube); ?>"><i class="fa fa-youtube"></i></a></span><?php } ?>
            <?php if ( !empty($linkedin) ) { ?><span class="ebd-social-links ebd-linkedin"><a href="<?php echo esc_url($linkedin); ?>"><i class="fa fa-linkedin"></i></a></span><?php } ?>
        </div>
        <?php if ( !empty($view_detail_link) ) { ?>
            <div class="ebd-view-more-btn">
                <a href="<?php $ebd_directory_display->the_directory_url(); ?>"><?php echo $view_detail_label; ?></a>
            </div>
        <?php } ?>
    </div>
</div>