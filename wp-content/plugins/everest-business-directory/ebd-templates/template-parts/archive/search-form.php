<?php

global $ebd_settings;
global $ebd_directory_library;
global $wpdb;

$search_form_template = (!empty($ebd_settings['general']['search_form_template'])) ? esc_attr($ebd_settings['general']['search_form_template']) : 'template-1';
$search_form_template = apply_filters('ebd_search_form_template', $search_form_template);

$listing_page = (!empty($ebd_settings['general']['listing_page'])) ? intval($ebd_settings['general']['listing_page']) : '';
if (!isset($shortcode)) {
    $listing_page = '';
}

$search_form_components = (!empty($ebd_settings['general']['search_form_components'])) ? $ebd_settings['general']['search_form_components'] : array();
if (!empty($search_form_components)) {
    $components_number = count($search_form_components);
    $components_class = 'ebd-search-secs-' . $components_number;
    ?>
    <div class="ebd-search-form-wrap ebd-col-3 <?php echo 'ebd-search-' . $search_form_template; ?> <?php echo $components_class; ?>">
        <form method="get" action="<?php echo (!empty($listing_page)) ? get_permalink($listing_page) : ''; ?>">
            <div class="ebd-search-form-main-fields-wrap">
                
                <?php if (in_array('location', $search_form_components)) { ?>
                    <div class="ebd-search-location ebd-search-field ebd-col-1">
                        <label class="ebd-title-field-lbl"><?php _e('Location', 'everest-business-directory'); ?></label>
                        <input type="text" name="location" placeholder="<?php _e('Adresse', 'everest-business-directory'); ?>" value="<?php echo (!empty($_GET['location'])) ? esc_attr($_GET['location']) : ''; ?>"/>
                    </div><?php } ?>
                
                <?php if (in_array('category', $search_form_components)) { ?>
                    <div class="ebd-directory-category ebd-search-field ebd-col-1 ebd-col-mt-20">
                        <label class="ebd-title-field-lbl"><?php echo (!empty($ebd_settings['general']['category_label'])) ? esc_attr($ebd_settings['general']['category_label']) : __('Category', 'everest-business-directory'); ?></label>
                        <select name="directory_category">
                            <option id="radius_5" value="5" <?php echo ($_GET['directory_category'] == "5") ? 'selected' : ''; ?>>5KM</option>
                            <option id="radius_10" value="10" <?php echo ($_GET['directory_category'] == "10" || empty($_GET['directory_category'])) ? 'selected' : '10'; ?>>10KM</option>
                            <option id="radius_20" value="20" <?php echo ($_GET['directory_category'] == "20") ? 'selected' : ''; ?>>20KM</option>
                            <option id="radius_50" value="50" <?php echo ($_GET['directory_category'] == "50") ? 'selected' : ''; ?>>50KM</option>
                        </select>
                    </div>
                <?php } ?>
                
                <?php if (in_array('keyword', $search_form_components)) { ?>
                    <div class="ebd-search-keyword ebd-search-field ebd-col-1 ebd-col-mt-20">
                        <label class="ebd-title-field-lbl"><?php echo (!empty($ebd_settings['general']['keyword_label'])) ? esc_attr($ebd_settings['general']['keyword_label']) : __('Keyword', 'everest-business-directory'); ?></label>
                        <input type="text" name="search_keyword" placeholder="Nom du vétérinaire" value="<?php echo (!empty($_GET['search_keyword'])) ? esc_attr($_GET['search_keyword']) : ''; ?>"/>
                    </div>
                <?php } ?>
                
                <?php if (in_array('field225073', $search_form_components)) { ?>
                    <?php                         
                        $results = $wpdb->get_results( "(SELECT DISTINCT CUSTOM_FIELD_225073 as CUSTOM_FIELD FROM {$wpdb->prefix}teamleader_contacts WHERE CUSTOM_FIELD_225073 IS NOT NULL AND (`country` = 'BE' OR `country` = 'LU') AND `CUSTOM_FIELD_271505` = 1 ) "
                                                        . "UNION "
                                                        . "(SELECT DISTINCT CUSTOM_FIELD_270502 as CUSTOM_FIELD FROM {$wpdb->prefix}teamleader_contacts WHERE CUSTOM_FIELD_270502 IS NOT NULL AND (`country` = 'BE' OR `country` = 'LU') AND `CUSTOM_FIELD_271506` = 1 )");
                        
                        $resultsKeys = [];
                        foreach($results as $resultsRow){    
                            $resultsKeys = array_merge($resultsKeys, explode(',', $resultsRow->CUSTOM_FIELD));
                        }
                        $resultsKeys = array_unique($resultsKeys);
                        asort($resultsKeys);
                        
                        $trans = get_html_translation_table(HTML_ENTITIES);        
                        $field_225073 = str_replace('\\\'', '&#039;', strtr(sanitize_text_field(isset($_GET['field_225073']) && !empty($_GET['field_225073']) ? $_GET['field_225073'] : ''), $trans)); 
                        
                        $preSetVals = !empty($field_225073) ? explode('|', $field_225073) : [];
                    ?>
                                        
                    <div class="ebd-directory-tags ebd-search-field ebd-col-1 ebd-col-mt-20">
                        <label class="ebd-title-field-lbl"><?php echo (!empty($ebd_settings['general']['field225073_label'])) ? esc_attr($ebd_settings['general']['field225073_label']) : __('225073', 'everest-business-directory'); ?></label>
                        <select id="select2-225073" class="select2-225073" multiple style="width:-webkit-fill-available; width:-moz-available;" placeholder="Pôles d'intérêts">
                            <?php foreach ($resultsKeys as $key=>$resultRow){ ?>
                            <option id="field_225073_<?php echo $key; ?>" value="<?php echo $resultRow ?>" <?php echo (in_array($resultRow, $preSetVals) ? 'selected' : ''); ?> > <?php echo $resultRow ?> </option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="field_225073" value="<?php echo (isset($_GET['field_225073']) ? $_GET['field_225073'] : ''); ?>">
                    </div>
                <?php } ?>
                
                <?php if (in_array('tags', $search_form_components)) { ?>
                    <div class="ebd-directory-tags ebd-search-field ebd-col-1 ebd-col-mt-20">
                        <label class="ebd-title-field-lbl"><?php echo (!empty($ebd_settings['general']['tags_label'])) ? esc_attr($ebd_settings['general']['tags_label']) : __('Tags', 'everest-business-directory'); ?></label>
                        <select name="directory_tag">
                            <option value=""><?php _e('Choose Tags', 'everest-business-directory'); ?></option>
                            <?php
                            $terms = get_terms('ebd-tags', array('hide_empty' => 0));
                            $categoryHierarchy = array();
                            $ebd_directory_library->sort_terms_hierarchicaly($terms, $categoryHierarchy, 0);
                            echo $ebd_directory_library->print_option($categoryHierarchy, array(), 1, '', 'directory_tags', $selected_directory_tag);
                            ?>
                        </select>
                    </div>
                <?php } ?>
                
                    <div class="ebd-directory-category ebd-search-field ebd-col-1 ebd-col-mt-20" style="line-height:30px;">
                    <img src="word.bild.com/wp-content/uploads/files/library_upv/logos-pictos/structures/cabinet.gif">
                    <span class="">Vétérinaire</span>
                </div>
                <div class="ebd-directory-category ebd-search-field ebd-col-1 ebd-col-mt-20" style="line-height:30px;">
                    <img src="word.bild.com/wp-content/uploads/files/library_upv/logos-pictos/structures/clinique.gif">
                    <span class="">Clinique vétérinaire</span>
                </div>
                    
                <div class="ebd-search-field ebd-col-1 ebd-col-mt-20" id="kak_nibyt" >
                    <input id="search_info" type="submit" value="<?php _e('Rechercher', 'everest-business-directory'); ?>" class="ebd-search-submit" />
                </div>
                    
            </div>
            <input type="hidden" name="paged" value="1"/>
        </form>
        
        
    </div>

    <?php
}?>