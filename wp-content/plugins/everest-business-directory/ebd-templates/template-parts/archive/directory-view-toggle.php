<?php
include(EBD_PATH . 'inc/cores/current-view.php');
if ( $toggle_counter > 1 ) {
    ?>
    <div class="ebd-view-toggle-wrap">
        <?php
        if ( !empty($ebd_settings[ 'general' ][ 'list_layout' ]) ) {
            ?>
            <a href="javascript:void(0);" class="ebd-view-toggle-trigger ebd-list-view-icon <?php echo ($current_view == 'list') ? 'ebd-current-view' : '' ?>" data-view-type="list"><i class="fa fa-th-list"></i></a>
            <?php
        }
        if ( !empty($ebd_settings[ 'general' ][ 'grid_layout' ]) ) {
            ?>
            <a href="javascript:void(0);" class="ebd-view-toggle-trigger ebd-grid-view-icon <?php echo ($current_view == 'grid') ? 'ebd-current-view' : '' ?>" data-view-type="grid"><i class="fa fa-th-large"></i></a>
            <?php
        }
        if ( !empty($ebd_settings[ 'general' ][ 'map_view' ]) ) {
            ?>
            <a href="javascript:void(0);" class="ebd-view-toggle-trigger ebd-map-view-icon <?php echo ($current_view == 'map') ? 'ebd-current-view' : '' ?>" data-view-type="map"><i class="fa fa-map-marker"></i></a>
                <?php
            }
            ?>

    </div>
    <?php
}?>