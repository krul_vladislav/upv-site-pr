<?php
/**
 * Contains all the variables used in the template files
 *
 * Please don't remove this
 */
include(EBD_PATH . 'inc/cores/list-variables.php');
global $ebd_directory_display;
global $ebd_directory_expiry;
?>
<div class="ebd-detail-template-1 ebd-detail-template-main-wrap">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="ebd-singl-dtl-top-wrap">
            <?php if ( !empty($directory_detail_settings[ 'featured_image' ]) ) { ?>
                <div class="ebd-directory-image">

                    <?php
                    $ebd_directory_display->the_featured_image();
                    ?>

                </div>
            <?php } ?>
            <div class="ebd-second-wrap-insdimg">
                <?php if ( !empty($directory_detail_settings[ 'directory_category' ]) ) {
                    ?>
                    <div class="ebd-directory-category">
                        <?php
                        if ( !empty($directory_categories) ) {
                            foreach ( $directory_categories as $directory_category ) {
                                ?>

                                <div class="ebd-each-category"><a href="<?php echo $ebd_directory_display->the_category_url($directory_category->term_id); ?>"><?php echo $directory_category->name; ?></a></div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                <?php } ?>
                <?php if ( $ebd_directory_expiry ) { ?>
                    <div class="ebd-singlpge-expiry-dte">
                        <span><span class="lnr lnr-calendar-full"></span><?php echo $expiry_date; ?></span>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="ebd-secndwrap-desc">
            <h1><?php the_title(); ?></h1>
            <div class="ebd-content-wrap"><?php the_content(); ?></div>
            <?php if ( !empty($directory_detail_settings[ 'contact_information' ]) ) { ?>
                <div class="ebd-contact-information-wrap">
                    <?php if ( !empty($directory_detail_settings[ 'location_information' ]) ) { ?>
                        <div class="ebd-location-information">
                            <div class="ebd-each-location-information">
                                <span class="ebd-info-value ebd-singl-dtl-addrsc"><?php echo $address; ?></span>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ( !empty($phone_number) ) { ?>
                        <div class="ebd-each-contact-information">
                            <span class="ebd-info-value ebd-singl-dtl-phnsc"><?php echo $phone_number; ?></span>
                        </div>
                    <?php } ?>
                    <?php if ( !empty($email_address) ) { ?>
                        <div class="ebd-each-contact-information">
                            <span class="ebd-info-value ebd-singl-dtl-emailsc"><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></span>
                        </div>
                    <?php } ?>
                    <?php if ( !empty($website) ) { ?>
                        <div class="ebd-each-contact-information">
                            <span class="ebd-info-value ebd-singl-dtl-websc"><a href="<?php echo esc_url($website); ?>"><?php echo $website; ?></a></span>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
            global $ebd_settings;
            if ( !empty($ebd_settings[ 'custom_fields' ]) ) {
                ?>
                <div class="ebd-custom-fields-wrap">
                    <?php
                    foreach ( $ebd_settings[ 'custom_fields' ] as $field_key => $field_details ) {
                        $field_value = get_post_meta($directory_id, $field_key, true);
                        if ( !empty($field_value) && !empty($field_details[ 'frontend_display' ]) ) {
                            $field_value = (is_array($field_value)) ? implode(', ', $field_value) : $field_value;
                            ?>
                            <div class="ebd-each-custom-field">
                                <span class="ebd-label"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></span>
                                <span class="ebd-info-value">
                                    <?php
                                    if ( $field_details[ 'field_type' ] == 'url' ) {
                                        ?>
                                        <a href="<?php echo esc_url($field_value); ?>"><?php echo $field_value; ?></a>
                                        <?php
                                    } else {
                                        echo $field_value;
                                    }
                                    ?>
                                </span>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php
            }
            if ( !empty($directory_detail_settings[ 'directory_gallery' ]) ) {
                ?>
                <div class="ebd-gallery-wrap">
                    <?php
                    if ( !empty($directory_gallery) ) {
                        foreach ( $directory_gallery as $gallery_item ) {
                            $attachment_src = wp_get_attachment_image_src($gallery_item);
                            $attachment_full_src = wp_get_attachment_image_src($gallery_item, 'large');
                            ?>
                            <div class="ebd-each-gallery">
                                <a href="<?php echo $attachment_full_src[ 0 ] ?>" rel="prettyPhoto[ebd_gallery]"><img src="<?php echo esc_url($attachment_src[ 0 ]); ?>"/></a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            <?php } ?>
            <div class="ebd-foot-wrap-tagsnsoc">
                <?php
                if ( !empty($directory_detail_settings[ 'directory_tags' ]) ) {
                    ?>
                    <div class="ebd-directory-tags">
                        <?php
                        if ( !empty($directory_tags) ) {
                            foreach ( $directory_tags as $directory_tag ) {
                                ?>
                                <span class="ebd-each-tag"><a href="<?php $ebd_directory_display->the_tag_url($directory_tag->term_id); ?>"><?php echo $directory_tag->name; ?></a></span>
                                <?php
                            }
                        }
                        ?>
                    </div>
                <?php } ?>
                <?php if ( !empty($directory_detail_settings[ 'social_information' ]) ) { ?>
                    <div class="ebd-social-information">
                        <?php if ( !empty($facebook) ) { ?><a class="ebd-fb-icn" href="<?php echo esc_url($facebook); ?>"><i class="fa fa-facebook"></i></a><?php } ?>
                        <?php if ( !empty($twitter) ) { ?><a class="ebd-tw-icn" href="<?php echo esc_url($twitter); ?>"><i class="fa fa-twitter"></i></a><?php } ?>
                        <?php if ( !empty($googleplus) ) { ?><a class="ebd-gp-icn" href="<?php echo esc_url($googleplus); ?>"><i class="fa fa-google-plus"></i></a><?php } ?>
                        <?php if ( !empty($instagram) ) { ?><a class="ebd-ins-icn" href="<?php echo esc_url($instagram); ?>"><i class="fa fa-instagram"></i></a><?php } ?>
                        <?php if ( !empty($youtube) ) { ?><a class="ebd-ytb-icn" href="<?php echo esc_url($youtube); ?>"><i class="fa fa-youtube"></i></a><?php } ?>
                        <?php if ( !empty($linkedin) ) { ?><a class="ebd-lnk-icn" href="<?php echo esc_url($linkedin); ?>"><i class="fa fa-linkedin"></i></a><?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </article>
</div>