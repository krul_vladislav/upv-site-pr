<?php

/**
 * The template for displaying all single posts of everest business directory
 *
 * @package Everest Business Directory
 *
 * @since 1.0.0
 */
global $ebd_settings;
get_header();

/**
 * ebd_main_outer_wrap hook
 *
 * @hooked EBD_Directory_Display::ebd_main_outer_wrap_start - 10 (outputs main outer wrap just below header)
 */
do_action('ebd_main_outer_wrap_start');

/**
 * ebd_before_main_content hook.
 *
 * @hooked EBD_Directory_Display::ebd_output_content_wrapper - 10 (outputs opening divs for the content)
 */
do_action('ebd_before_main_content');
do_action('ebd_after_content_wrap');

while ( have_posts() ) : the_post();
    $single_template = (!empty($ebd_settings[ 'directory_detail' ][ 'template' ])) ? esc_attr($ebd_settings[ 'directory_detail' ][ 'template' ]) : 'template-1';
    $single_template = apply_filters('ebd_detail_template', $single_template);
    ebd_load_template_part('single/content', $single_template);


endwhile;


/**
 * ebd_after_main_content hook.
 *
 * @hooked EBD_Directory_Display::ebd_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action('ebd_after_content_wrap_close');

do_action('ebd_after_main_content');
/**
 * ebd_sidebar hook.
 *
 * @hooked EBD_Directory_Display::ebd_sidebar - 10 (generates sidebar as per configuration)
 */
do_action('ebd_sidebar');
/**
 * ebd_main_outer_wrap_end hook
 *
 * @hooked EBD_Directory_Display::ebd_main_outer_wrap_end - 10 (outputs main outer wrap just below header)
 */
do_action('ebd_main_outer_wrap_end');
get_footer();



