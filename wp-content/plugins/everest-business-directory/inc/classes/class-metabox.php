<?php
defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Metabox') ) {

    class EBD_Metabox extends EBD_Library {

        function __construct() {
            add_action('add_meta_boxes', array( $this, 'directory_metaboxes' ));
            add_action('save_post', array( $this, 'store_metabox_values' ));
            //  add_action('ebd-categories_edit_form_fields', array( $this, 'category_icon_fields' ));
            //  add_action('edited_terms', array( $this, 'save_category_meta_fields' ));
        }

        function directory_metaboxes() {
            add_meta_box(
                    'ebd-contact-information', __('Directory Contact Information'), array( $this, 'render_contact_information' ), 'ebd', 'normal', 'default'
            );
            add_meta_box(
                    'ebd-social-information', __('Directory Social Information'), array( $this, 'render_social_information' ), 'ebd', 'normal', 'default'
            );
            add_meta_box(
                    'ebd-location-information', __('Directory Location Information'), array( $this, 'render_location_information' ), 'ebd', 'normal', 'default'
            );
            add_meta_box(
                    'ebd-gallery', __('Directory Gallery'), array( $this, 'render_gallery' ), 'ebd', 'normal', 'default'
            );
            add_meta_box(
                    'ebd-custom-fields', __('Directory Custom Fields'), array( $this, 'render_custom_fields' ), 'ebd', 'normal', 'default'
            );
            add_meta_box(
                    'ebd-expiry-date', __('Directory Expiration'), array( $this, 'render_expiry_fields' ), 'ebd', 'side', 'high'
            );
            add_meta_box(
                    'ebd-featured-directory', __('Featured Directory'), array( $this, 'render_featured_directory' ), 'ebd', 'side', 'default'
            );
        }

        /**
         * Generates contact information metabox
         *
         * @since 1.0.0
         */
        function render_contact_information($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/contact-information.php');
        }

        /**
         * Generates social information metabox
         *
         * @since 1.0.0
         */
        function render_social_information($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/social-information.php');
        }

        /**
         * Generates location information metabox
         *
         * @since 1.0.0
         */
        function render_location_information($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/location-information.php');
        }

        /**
         * Generates gallery metabox
         *
         * @since 1.0.0
         */
        function render_gallery($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/directory-gallery.php');
        }

        /**
         * Generates custom fields metabox
         *
         * @since 1.0.0
         */
        function render_custom_fields($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/custom-fields.php');
        }

        /**
         * Generates Expiry Date metabox
         *
         * @since 1.0.0
         */
        function render_expiry_fields($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/expiry-fields.php');
        }

        /**
         * Generates Featured Directory metabox
         *
         * @since 1.0.0
         */
        function render_featured_directory($post) {
            include(EBD_PATH . 'inc/views/backend/metaboxes/featured-directory.php');
        }

        /**
         * Store directory meta box values
         *
         * @since 1.0.0
         */
        function store_metabox_values($post_id) {

            if ( !isset($_POST[ 'ebd_metabox_nonce' ]) ) {
                return;
            }

            if ( !wp_verify_nonce($_POST[ 'ebd_metabox_nonce' ], 'ebd-metabox-nonce') ) {
                return;
            }

            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
                return;
            }

            if ( !current_user_can('edit_post', $post_id) ) {
                return;
            }

            if ( isset($_POST[ 'post_type' ]) && 'ebd' === $_POST[ 'post_type' ] ) {
                $sanitize_rule = array();
                global $ebd_settings;
                if ( !empty($ebd_settings[ 'custom_fields' ]) ) {
                    foreach ( $ebd_settings[ 'custom_fields' ] as $field_key => $field_details ) {
                        if ( $field_details[ 'field_type' ] == 'html' ) {
                            $sanitize_rule[ $field_key ] = 'none';
                        }
                    }
                }
                // var_dump($sanitize_rule);
                //  die();
                $directory_fields = $this->sanitize_array($_POST[ 'directory_fields' ], $sanitize_rule);
                $location_fields = $directory_fields[ 'location' ];
                $contact_fields = $directory_fields[ 'contact' ];
                $social_fields = $directory_fields[ 'social' ];
                $custom_fields = (!empty($directory_fields[ 'custom_fields' ])) ? $directory_fields[ 'custom_fields' ] : array();
                $gallery = (!empty($directory_fields[ 'gallery' ])) ? $directory_fields[ 'gallery' ] : array();
                $expiry_date = $directory_fields[ 'expiry_date' ];
                foreach ( $location_fields as $location_field_key => $location_field_value ) {
                    $meta_key = '_ebd_' . $location_field_key;
                    update_post_meta($post_id, $meta_key, $location_field_value);
                }
                foreach ( $contact_fields as $contact_field_key => $contact_field_value ) {
                    $meta_key = '_ebd_' . $contact_field_key;
                    update_post_meta($post_id, $meta_key, $contact_field_value);
                }
                foreach ( $social_fields as $social_field_key => $social_field_value ) {
                    $meta_key = '_ebd_' . $social_field_key;
                    update_post_meta($post_id, $meta_key, $social_field_value);
                }
                if ( !empty($custom_fields) ) {
                    foreach ( $custom_fields as $custom_field_key => $custom_field_value ) {
                        $meta_key = $custom_field_key;
                        update_post_meta($post_id, $meta_key, $custom_field_value);
                    }
                }
                update_post_meta($post_id, '_ebd_gallery', $gallery);
                update_post_meta($post_id, '_ebd_expiry_date', $expiry_date);
                if ( !empty($directory_fields[ 'featured_directory' ]) ) {
                    update_post_meta($post_id, '_ebd_featured', 1);
                } else {
                    update_post_meta($post_id, '_ebd_featured', 0);
                }
                //    die('reached here');
            }
        }

        function category_icon_fields($term) {
            $category_icon = get_metadata('term', $term->term_id, 'ebd_category_icon', TRUE);
            $category_icon_color = get_metadata('term', $term->term_id, 'ebd_category_icon_color', TRUE);
            $category_icon_bg = get_metadata('term', $term->term_id, 'ebd_category_icon_bg', TRUE);
            ?>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="categor_icon"><?php _e('Category Icon') ?></label></th>
                <td>
                    <input type="text" name="ebd_category_icon" value="<?php echo $category_icon; ?>" class="ebd-icon-picker"/>
                </td>
            </tr>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="icon_color"><?php _e('Icon Color') ?></label></th>
                <td>
                    <input type="text" name="ebd_category_icon_color" value="<?php echo $category_icon_color; ?>"/>
                </td>
            </tr>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="icon_background"><?php _e('Icon Background Color') ?></label></th>
                <td>
                    <input type="text" name="ebd_category_icon_bg" value="<?php echo $category_icon_bg; ?>"/>
                </td>
            </tr>
            <?php
        }

        function save_category_meta_fields($term_id) {
            if ( isset($_POST[ 'ebd_category_icon' ], $_POST[ 'ebd_category_icon_color' ], $_POST[ 'ebd_category_icon_bg' ]) ) {
                $category_icon = sanitize_text_field($_POST[ 'ebd_category_icon' ]);
                $category_icon_color = sanitize_text_field($_POST[ 'ebd_category_icon_color' ]);
                $category_icon_bg = sanitize_text_field($_POST[ 'ebd_category_icon_bg' ]);

                update_metadata('term', $term_id, 'ebd_category_icon', $category_icon);
                update_metadata('term', $term_id, 'ebd_category_icon_color', $category_icon_color);
                update_metadata('term', $term_id, 'ebd_category_icon_bg', $category_icon_bg);
            }
        }

    }

    new EBD_Metabox();
}
