<?php

defined('ABSPATH') or die('No script kiddies please!!');

if ( !class_exists('EBD_Email_Notification') ) {

    class EBD_Email_Notification {

        function __construct() {
            /**
             * Directory submission admin notification
             */
            add_action('ebd_admin_notification', array( $this, 'send_admin_notification' ), 10, 1);

            /**
             * Directory reject notification
             */
            add_action('wp_trash_post', array( $this, 'directory_reject_notification' ));

            /**
             *
             * Directory publish notification
             *
             */
            add_action('publish_ebd', array( $this, 'directory_publish_notification' ), 10, 2);
        }

        function send_admin_notification($directory_id) {
            global $ebd_settings;
            $email_from_name = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ]) : 'No Reply';
            $email_from_email = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ]) : 'noreply' . $_SERVER[ 'SERVER_NAME' ];
            $admin_email = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_email' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_email' ]) : get_option('admin_email');
            $notification_subject = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_subject' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_subject' ]) : __('New Directory Submission', 'everest-business-directory');
            $default_admin_notification_message = $this->get_default_admin_notification_message();
            $admin_notification_message = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_message' ])) ? $ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_message' ] : $this->sanitize_escaping_linebreaks($default_admin_notification_message);
            $directory_title = get_the_title($directory_id);
            $directory_admin_link = admin_url('post.php?post=' . $directory_id . '&action=edit');
            $admin_notification_message = str_replace('#directory_title', $directory_title, $admin_notification_message);
            $admin_notification_message = str_replace('#directory_admin_link', "<a href='$directory_admin_link'>$directory_admin_link</a>", $admin_notification_message);
            $headers = array();
            $charset = get_option('blog_charset');
            $headers[] = 'Content-Type: text/html; charset=' . $charset;
            $headers[] = "From: $email_from_name <$email_from_email>";
            wp_mail($admin_email, $notification_subject, $admin_notification_message, $headers);
        }

        function directory_reject_notification($directory_id) {
            $trash_notification_check = get_post_meta($directory_id, '_ebd_trash_notification', true);
            if ( !empty($trash_notification_check) ) {
                return;
            }
            $post_type = get_post_type($directory_id);
            if ( 'ebd' == $post_type ) {
                global $ebd_settings;
                if ( !empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'directory_reject_notification' ]) ) {
                    $email_from_name = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ]) : 'No Reply';
                    $email_from_email = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ]) : 'noreply' . $_SERVER[ 'SERVER_NAME' ];
                    $notification_subject = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_subject' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_subject' ]) : __('Directory Rejected', 'everest-business-directory');
                    $reject_default_notification_message = $this->get_default_reject_notification_message();
                    $reject_notification_message = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_message' ])) ? $ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_message' ] : $this->sanitize_escaping_linebreaks($reject_default_notification_message);
                    $directory_title = get_the_title($directory_id);
                    $reject_notification_message = str_replace('#directory_title', $directory_title, $reject_notification_message);
                    $directory_author_id = get_post_field('post_author', $directory_id);
                    $reject_email = get_the_author_meta('user_email', $directory_author_id);
                    $headers = array();
                    $charset = get_option('blog_charset');
                    $headers[] = 'Content-Type: text/html; charset=' . $charset;
                    $headers[] = "From: $email_from_name <$email_from_email>";
                    wp_mail($reject_email, $notification_subject, $reject_notification_message, $headers);
                }
            }
        }

        function directory_publish_notification($directory_id, $directory) {
            if ( !isset($_POST[ 'hidden_post_status' ]) ) {
                return;
            }
            if ( $_POST[ 'hidden_post_status' ] == 'draft' || $_POST[ 'hidden_post_status' ] == 'pending' ) {

                global $ebd_settings;
                if ( !empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'directory_publish_notification' ]) ) {
                    $email_from_name = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ]) : 'No Reply';
                    $email_from_email = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ]) : 'noreply' . $_SERVER[ 'SERVER_NAME' ];
                    $notification_subject = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_subject' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_subject' ]) : __('Directory Published', 'everest-business-directory');
                    $publish_default_notification_message = $this->get_default_publish_notification_message();
                    $publish_notification_message = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_message' ])) ? $ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_message' ] : $this->sanitize_escaping_linebreaks($publish_default_notification_message);
                    $directory_title = get_the_title($directory_id);
                    $directory_link = get_permalink($directory_id);
                    $publish_notification_message = str_replace('#directory_title', $directory_title, $publish_notification_message);
                    $publish_notification_message = str_replace('#directory_link', "<a href='$directory_admin_link'>$directory_link</a>", $publish_notification_message);
                    $directory_author_id = get_post_field('post_author', $directory_id);
                    $publish_email = get_the_author_meta('user_email', $directory_author_id);
                    $headers = array();
                    $charset = get_option('blog_charset');
                    $headers[] = 'Content-Type: text/html; charset=' . $charset;
                    $headers[] = "From: $email_from_name <$email_from_email>";
                    wp_mail($publish_email, $notification_subject, $publish_notification_message, $headers);
                }
            }
        }

        function get_default_admin_notification_message() {
            return __(sprintf('Hello There,

                                A new directory has been submitted via Everest Business Directory plugin in your %s website. Please find details below:

                                Directory Title: #directory_title

                                _____

                                To take action (approve/reject) - please go here:
                                #directory_admin_link

                                Thank you', esc_attr(get_bloginfo('name'))), 'everest-business-directory');
        }

        function get_default_reject_notification_message() {
            return __(sprintf('Hello There,

                                Your directory has been rejected in our %s website. Please find details below:

                                Directory Title: #directory_title

                                Thank you', esc_attr(get_bloginfo('name'))), 'everest-business-directory');
        }

        function get_default_publish_notification_message() {
            return __(sprintf('Hello There,

                                Your directory has been published in our %s website. Please find details below:

                                Directory Title: #directory_title
                                Directory Link: #directory_link

                                Thank you', esc_attr(get_bloginfo('name'))), 'everest-business-directory');
        }

    }

    $GLOBALS[ 'ebd_email_notifier' ] = new EBD_Email_Notification();
}