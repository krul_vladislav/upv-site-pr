<?php
defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Categories_List_Widget') ) {

    /**
     * Adds Foo_Widget widget.
     */
    class EBD_Categories_List_Widget extends WP_Widget {

        /**
         * Register widget with WordPress.
         */
        function __construct() {
            parent::__construct(
                    'ebd_categories_list_widget', // Base ID
                    esc_html__('Directory Categories List', 'everest-business-directory'), // Name
                    array( 'description' => esc_html__('A widget to display directory categories list', 'everest-business-directory'), ) // Args
            );
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args     Widget arguments.
         * @param array $instance Saved values from database.
         */
        public function widget($args, $instance) {
            echo $args[ 'before_widget' ];
            if ( !empty($instance[ 'title' ]) ) {
                echo $args[ 'before_title' ] . apply_filters('widget_title', $instance[ 'title' ]) . $args[ 'after_title' ];
            }
            $exclude = $instance[ 'exclude' ];
            $number = $instance[ 'number' ];
            echo do_shortcode('[ebd_directory_categories_list exclude="' . $exclude . '"  number="' . $number . '"]');
            echo $args[ 'after_widget' ];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance) {
            $title = !empty($instance[ 'title' ]) ? $instance[ 'title' ] : '';
            $exclude = !empty($instance[ 'exclude' ]) ? $instance[ 'exclude' ] : '';
            $number = !empty($instance[ 'number' ]) ? $instance[ 'number' ] : -1;
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'everest-business-directories'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('exclude')); ?>"><?php esc_attr_e('Exclude:', 'everest-business-directories'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('exclude')); ?>" name="<?php echo esc_attr($this->get_field_name('exclude')); ?>" type="text" value="<?php echo esc_attr($exclude); ?>">
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_attr_e('Number', 'everest-business-directories'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="number" value="<?php echo esc_attr($number); ?>">
            <p class="description"><?php _e('Please enter the total number of directory categories that you want to display. Default is -1 which will display all categories.', 'everest-business-directory'); ?></p>
            </p>
            <?php
        }

        /**
         * Sanitize widget form values as they are saved.
         *
         * @see WP_Widget::update()
         *
         * @param array $new_instance Values just sent to be saved.
         * @param array $old_instance Previously saved values from database.
         *
         * @return array Updated safe values to be saved.
         */
        public function update($new_instance, $old_instance) {
            $instance = array();
            $instance[ 'title' ] = (!empty($new_instance[ 'title' ]) ) ? sanitize_text_field($new_instance[ 'title' ]) : '';
            $instance[ 'exclude' ] = (!empty($new_instance[ 'exclude' ]) ) ? sanitize_text_field($new_instance[ 'exclude' ]) : '';
            $instance[ 'number' ] = (!empty($new_instance[ 'number' ]) ) ? sanitize_text_field($new_instance[ 'number' ]) : -1;
            return $instance;
        }

    }

    // class Foo_Widget

    new EBD_Categories_List_Widget();
}