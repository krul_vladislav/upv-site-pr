<?php
defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Admin') ) {

    class EBD_Admin extends EBD_Library {

        function __construct() {
            add_action('admin_menu', array( $this, 'ebd_menu' ));
            add_action('admin_footer', array( $this, 'add_js_templates' ));

            /**
             * Quick Edit Hooks
             */
            add_filter('manage_ebd_posts_columns', array( $this, 'add_expiry_column' ));
            add_action('manage_ebd_posts_custom_column', array( $this, 'display_expiry_column_value' ), 10, 2);
            add_action('quick_edit_custom_box', array( $this, 'generate_directory_expiry_quickedit_field' ), 10, 2);
            add_action('save_post', array( $this, 'save_quickedit_fields' ), 10, 2);
            add_filter('post_row_actions', array( $this, 'add_quickedit_set_data' ), 10, 2);
            add_action('admin_print_footer_scripts-edit.php', array( $this, 'inject_quickedit_javascript' ));

            /**
             * Widget
             */
            add_action('widgets_init', array( $this, 'register_directory_widgets' ));

            /**
             * Custom Image Sizes
             */
            add_image_size('everest-large', 1024, 1024, true);
            add_image_size('everest-medium-large', 700, 700, true);
            add_image_size('everest-medium', 300, 300, true);
        }

        function ebd_menu() {
            add_submenu_page('edit.php?post_type=ebd', __('Settings', 'everest-business-directory'), __('Settings', 'everest-business-directory'), 'manage_options', 'ebd-settings', array( $this, 'ebd_settings' ));
            add_submenu_page('edit.php?post_type=ebd', __('How to use', 'everest-business-directory'), __('How to use', 'everest-business-directory'), 'manage_options', 'ebd-how-to-use', array( $this, 'generate_how_to_use' ));
            add_submenu_page('edit.php?post_type=ebd', __('About us', 'everest-business-directory'), __('About us', 'everest-business-directory'), 'manage_options', 'ebd-about-us', array( $this, 'generate_about_us' ));
        }

        /**
         * Plugin Settings Page
         *
         * @since 1.0.0
         */
        function ebd_settings() {
            global $ebd_settings;
            include(EBD_PATH . 'inc/views/backend/settings.php');
        }

        /**
         * How to use page
         *
         * @since 1.0.0
         */
        function generate_how_to_use() {
            include(EBD_PATH . 'inc/views/backend/how-to-use.php');
        }

        /**
         * About us page
         *
         * @since 1.0.0
         */
        function generate_about_us() {
            include(EBD_PATH . 'inc/views/backend/about-us.php');
        }

        /**
         * JS Templates
         *
         * @since 1.0.0
         */
        function add_js_templates() {
            ?>
            <script type="text/html" id="tmpl-ebd-textfield">
            <?php include(EBD_PATH . 'inc/views/backend/js-templates/custom-fields/textfield.php'); ?>
            </script>
            <script type="text/html" id="tmpl-ebd-radio">
            <?php include(EBD_PATH . 'inc/views/backend/js-templates/custom-fields/radio-button.php'); ?>
            </script>
            <script type="text/html" id="tmpl-ebd-option-value">
            <?php include(EBD_PATH . 'inc/views/backend/js-templates/custom-fields/option-value.php'); ?>
            </script>
            <?php
        }

        /**
         * Add Expiry Column
         *
         */
        function add_expiry_column($posts_columns) {
            $posts_columns[ 'directory_expiration' ] = __('Expires On', 'everest-business-directory');
            return $posts_columns;
        }

        /**
         *
         *
         */
        function display_expiry_column_value($column_name, $post_id) {
            if ( 'directory_expiration' == $column_name ) {
                $directory_expiration = get_post_meta($post_id, '_ebd_expiry_date', true);

                echo $directory_expiration;
            }
        }

        function generate_directory_expiry_quickedit_field($column_name, $post_type) {
            if ( 'directory_expiration' != $column_name )
                return;
            $post_id = get_the_ID();
            $directory_expiration = get_post_meta($post_id, '_ebd_expiry_date', true);
            ?>

            <fieldset class="inline-edit-col-right">
                <div class="inline-edit-col">
                    <label>
                        <span class="title"><?php esc_html_e('Expiry Date', 'generatewp'); ?></span>
                        <span class="input-text-wrap">
                            <input type="text" name="directory_expiry_date" class="ebd-datepicker-1" value="<?php echo $directory_expiration; ?>">
                        </span>
                    </label>
                </div>
            </fieldset>
            <?php
        }

        function save_quickedit_fields($post_id, $post) {
            // if called by autosave, then bail here
            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
                return;

            // if this "post" post type?
            if ( $post->post_type != 'ebd' )
                return;

            // does this user have permissions?
            if ( !current_user_can('edit_post', $post_id) )
                return;

            // update!
            if ( isset($_POST[ 'directory_expiry_date' ]) ) {
                update_post_meta($post_id, '_ebd_expiry_date', sanitize_text_field($_POST[ 'directory_expiry_date' ]));
            }
        }

        function add_quickedit_set_data($actions, $post) {
            $expiry_date = get_post_meta($post->ID, '_ebd_expiry_date', true);

            if ( !empty($expiry_date) ) {
                if ( isset($actions[ 'inline hide-if-no-js' ]) ) {
                    $new_attribute = sprintf('data-expiry-date="%s"', esc_attr($expiry_date));
                    $actions[ 'inline hide-if-no-js' ] = str_replace('class=', "$new_attribute class=", $actions[ 'inline hide-if-no-js' ]);
                }
            }

            return $actions;
        }

        function inject_quickedit_javascript() {
            $current_screen = get_current_screen();
            if ( $current_screen->post_type != 'ebd' )
                return;
            // Ensure jQuery library loads
            wp_enqueue_script('jquery');
            ?>
            <script type="text/javascript">
                jQuery(function ($) {

                    $('#the-list').on('click', 'a.editinline', function (e) {
                        e.preventDefault();
                        var expiry_date = $(this).data('expiry-date');
                        inlineEditPost.revert();
                        $('.ebd-datepicker-1').val(expiry_date ? expiry_date : '');

                    });
                });
            </script>
            <?php
        }

        function register_directory_widgets() {
            register_widget('ebd_featured_widget');
            register_widget('ebd_categories_list_widget');
            register_widget('ebd_tags_list_widget');
        }

    }

    new EBD_Admin();
}


