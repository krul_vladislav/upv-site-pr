<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Init') ) {

    class EBD_Init {

        /**
         * Plugin initial tasks
         *
         * @since 1.0.0
         */
        function __construct() {
            add_action('init', array( $this, 'plugin_init' ));
            add_action('plugins_loaded', array( $this, 'load_textdomain' ));
        }

        function plugin_init() {
            $this->register_directory();
            $this->register_directory_category();
            $this->register_directory_tags();
            $this->flush_permalink_rules();
        }

        /**
         * Translation Ready text domain
         *
         * @since 1.0.0
         */
        function load_textdomain() {
            load_plugin_textdomain('everest-business-directory', false, EBD_PATH . 'languages');
        }

        /**
         * Register directory post type
         *
         * @since 1.0.0
         */
        function register_directory() {
            $labels = array(
                'name' => _x('Directories', 'post type general name', 'everest-business-directory'),
                'singular_name' => _x('Directory', 'post type singular name', 'everest-business-directory'),
                'menu_name' => _x('Directories', 'admin menu', 'everest-business-directory'),
                'name_admin_bar' => _x('Directory', 'add new on admin bar', 'everest-business-directory'),
                'add_new' => _x('Add New', 'everest-business-directory', 'everest-business-directory'),
                'add_new_item' => __('Add New Directory', 'everest-business-directory'),
                'new_item' => __('New Directory', 'everest-business-directory'),
                'edit_item' => __('Edit Directory', 'everest-business-directory'),
                'view_item' => __('View Directory', 'everest-business-directory'),
                'all_items' => __('All Directories', 'everest-business-directory'),
                'search_items' => __('Search Directories', 'everest-business-directory'),
                'parent_item_colon' => __('Parent Directories:', 'everest-business-directory'),
                'not_found' => __('No directories found.', 'everest-business-directory'),
                'not_found_in_trash' => __('No directories found in Trash.', 'everest-business-directory')
            );

            $args = array(
                'labels' => $labels,
                'description' => __('Description.', 'everest-business-directory'),
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array( 'slug' => 'directory' ),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
                'menu_icon' => 'dashicons-category'
            );

            register_post_type('ebd', $args);
        }

        /**
         * Register directory category taxonomy
         *
         * @since 1.0.0
         */
        function register_directory_category() {
            $labels = array(
                'name' => _x('Categories', 'taxonomy general name', 'everest-business-directory'),
                'singular_name' => _x('Category', 'taxonomy singular name', 'everest-business-directory'),
                'search_items' => __('Search Categories', 'everest-business-directory'),
                'all_items' => __('All Categories', 'everest-business-directory'),
                'parent_item' => __('Parent Category', 'everest-business-directory'),
                'parent_item_colon' => __('Parent Category:', 'everest-business-directory'),
                'edit_item' => __('Edit Category', 'everest-business-directory'),
                'update_item' => __('Update Category', 'everest-business-directory'),
                'add_new_item' => __('Add New Category', 'everest-business-directory'),
                'new_item_name' => __('New Category Name', 'everest-business-directory'),
                'menu_name' => __('Categories', 'everest-business-directory'),
            );

            $args = array(
                'hierarchical' => true,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'query_var' => true,
                'rewrite' => array( 'slug' => 'directory-categories' ),
            );

            register_taxonomy('ebd-categories', array( 'ebd' ), $args);
        }

        function register_directory_tags() {
            // Add new taxonomy, NOT hierarchical (like tags)
            $labels = array(
                'name' => _x('Tags', 'taxonomy general name', 'everest-business-directory'),
                'singular_name' => _x('Tag', 'taxonomy singular name', 'everest-business-directory'),
                'search_items' => __('Search Tags', 'everest-business-directory'),
                'popular_items' => __('Popular Tags', 'everest-business-directory'),
                'all_items' => __('All Tags', 'everest-business-directory'),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __('Edit Tag', 'everest-business-directory'),
                'update_item' => __('Update Tag', 'everest-business-directory'),
                'add_new_item' => __('Add New Tag', 'everest-business-directory'),
                'new_item_name' => __('New Tag Name', 'everest-business-directory'),
                'separate_items_with_commas' => __('Separate Tags with commas', 'everest-business-directory'),
                'add_or_remove_items' => __('Add or remove Tags', 'everest-business-directory'),
                'choose_from_most_used' => __('Choose from the most used Tags', 'everest-business-directory'),
                'not_found' => __('No Tags found.', 'everest-business-directory'),
                'menu_name' => __('Tags', 'everest-business-directory'),
            );

            $args = array(
                'hierarchical' => false,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                'rewrite' => array( 'slug' => 'directory-tags' ),
            );

            register_taxonomy('ebd-tags', 'ebd', $args);
        }

        function flush_permalink_rules() {
            $ebd_plugin_state = get_option('ebd_plugin_state_change');

            if ( $ebd_plugin_state == 'yes' ) {
                flush_rewrite_rules();
                update_option('ebd_plugin_state_change', 'no');
            }
        }

    }

    new EBD_Init();
}

