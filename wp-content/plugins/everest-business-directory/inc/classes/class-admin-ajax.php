<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Ajax') ) {

    class EBD_Ajax extends EBD_Library {

        function __construct() {
            /**
             * Directory gallery Images upload
             */
            add_action('wp_ajax_ebd_gallery_images_action', array( $this, 'generate_gallery_images' ));
            add_action('wp_ajax_nopriv_ebd_gallery_images_action', array( $this, 'permission_denied' ));

            /**
             * Directory Image Upload
             */
            add_action('wp_ajax_ebd_file_upload_action', array( $this, 'frontend_upload_images' ));
            add_action('wp_ajax_nopriv_ebd_file_upload_action', array( $this, 'frontend_upload_images' ));

            /**
             * Directory file remove
             */
            add_action('wp_ajax_ebd_remove_file_action', array( $this, 'remove_uploaded_image' ));
            add_action('wp_ajax_nopriv_ebd_remove_file_action', array( $this, 'remove_uploaded_image' ));

            /**
             * Directory frontend submission
             */
            add_action('wp_ajax_ebd_frontend_submission_action', array( $this, 'frontend_submission_action' ));
            add_action('wp_ajax_nopriv_ebd_frontend_submission_action', array( $this, 'frontend_submission_action' ));

            /**
             * Directory delete
             */
            add_action('wp_ajax_ebd_directory_delete_action', array( $this, 'delete_directory' ));
            add_action('wp_ajax_nopriv_ebd_directory_delete_action', array( $this, 'delete_directory' ));
        }

        /**
         * Generates gallery image html
         *
         * @since 1.0.0
         */
        function generate_gallery_images() {
            if ( !empty($_POST[ '_wpnonce' ]) && wp_verify_nonce($_POST[ '_wpnonce' ], 'ebd-ajax-nonce') ) {
                include(EBD_PATH . 'inc/views/backend/ajax/directory-gallery.php');
                die();
            } else {
                $this->permission_denied();
            }
        }

        /**
         * Uploads Image from Frontend Form
         *
         * @since 1.0.0
         */
        function frontend_upload_images() {

            if ( !empty($_GET[ 'file_uploader_nonce' ]) && wp_verify_nonce($_GET[ 'file_uploader_nonce' ], 'ebd_ajax_nonce') ) {


                $allowedExtensions = array( 'jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP' );
                $unallowed_extensions = array( 'php', 'exe', 'ini', 'perl' );


                $sizeLimit = sanitize_text_field($_GET[ 'sizeLimit' ]);
                $custom_folder = sanitize_text_field($_GET[ 'custom_folder' ]);
                $uploader = new ebd_qqFileUploader($allowedExtensions, $sizeLimit);
                $upload_dir = wp_upload_dir();

                $upload_path = $upload_dir[ 'path' ] . '/';
                $upload_url = $upload_dir[ 'url' ];


                $result = $uploader->handleUpload($upload_path, $replaceOldFile = false, $upload_url);

                echo json_encode($result);
                die();
            } else {
                $this->permission_denied();
            }
        }

        /**
         * Removes uploaded image
         *
         * @since 1.0.0
         */
        function remove_uploaded_image() {
            if ( isset($_POST[ '_wpnonce' ], $_POST[ 'attachment_id' ], $_POST[ 'attachment_code' ]) && wp_verify_nonce($_POST[ '_wpnonce' ], 'ebd_ajax_nonce') ) {
                $attachment_id = intval($_POST[ 'attachment_id' ]);
                $attachment_code = sanitize_text_field($_POST[ 'attachment_code' ]);
                $attachment_date = get_the_date("U", $attachment_id);
                $attachment_check_code = md5($attachment_date);
                if ( $attachment_check_code == $attachment_code ) {
                    $check = wp_delete_attachment($attachment_id, true);
                    if ( $check ) {
                        die('success');
                    }
                } else {
                    $this->permission_denied();
                }
            } else {
                $this->permission_denied();
            }
        }

        /**
         * Frontend form submission
         *
         * @since 1.0.0
         */
        function frontend_submission_action() {

            if ( isset($_POST[ '_wpnonce' ], $_POST[ 'form_data' ]) && wp_verify_nonce($_POST[ '_wpnonce' ], 'ebd_ajax_nonce') ) {
                global $ebd_settings;
                // die('reached');
                parse_str($_POST[ 'form_data' ], $form_data);
                //  var_dump($form_data);
                $sanitize_rule = array( 'directory_content' => 'html' );
                if ( !empty($ebd_settings[ 'custom_fields' ]) ) {
                    foreach ( $ebd_settings[ 'custom_fields' ] as $field_key => $field_details ) {
                        if ( $field_details[ 'field_type' ] == 'html' ) {
                            $sanitize_rule[ $field_key ] = 'html';
                        }
                    }
                }
                $form_data = $this->sanitize_array($form_data, $sanitize_rule);
                $frontend_submitted_fields = $form_data[ 'frontend_form' ][ 'frontend_fields' ];
                include(EBD_PATH . 'inc/cores/frontend-form-validation.php');
                $this->print_array($form_data);
                die();
            } else {
                $this->permission_denied();
            }
        }

        function delete_directory() {
            if ( isset($_POST[ '_wpnonce' ]) && wp_verify_nonce($_POST[ '_wpnonce' ], 'ebd_directory_delete_nonce') ) {
                $directory_id = intval($_POST[ 'directory_id' ]);
                $security_key = sanitize_text_field($_POST[ 'security_key' ]);
                $date_format = get_option('date_format');
                $directory_date = get_the_date($date_format, $directory_id);
                if ( md5($directory_date) != $security_key ) {
                    $response = array( 'status' => 403, 'message' => __('Invalid security key', 'everest-business-directory') );
                } else {
                    if ( is_user_logged_in() ) {
                        $current_user = wp_get_current_user();
                        $current_user_id = $current_user->ID;
                        $directory_author = get_post_field('post_author', $directory_id);
                        if ( $directory_author != $current_user_id ) {
                            $response = array( 'status' => 403, 'message' => __('Invalid directory', 'everest-business-directory') );
                        } else {
                            update_post_meta($directory_id, '_ebd_trash_notification', 'no');
                            $check = wp_trash_post($directory_id);
                            // var_dump($check);
                            if ( $check ) {
                                $response = array( 'status' => 200, 'message' => __('Directory delete successfully', 'everest-business-directory') );
                            }
                        }
                    }
                }
                echo json_encode($response);
                die();
            } else {
                $this->permission_denied();
            }
        }

    }

    new EBD_Ajax();
}

