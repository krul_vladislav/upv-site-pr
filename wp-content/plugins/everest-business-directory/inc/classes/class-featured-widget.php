<?php
defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Featured_Widget') ) {

    /**
     * Adds Foo_Widget widget.
     */
    class EBD_Featured_Widget extends WP_Widget {

        /**
         * Register widget with WordPress.
         */
        function __construct() {
            parent::__construct(
                    'ebd_featured_widget', // Base ID
                    esc_html__('Featured Directories', 'everest-business-directory'), // Name
                    array( 'description' => esc_html__('A widget to display featured directories', 'everest-business-directory'), ) // Args
            );
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args     Widget arguments.
         * @param array $instance Saved values from database.
         */
        public function widget($args, $instance) {
            echo $args[ 'before_widget' ];
            if ( !empty($instance[ 'title' ]) ) {
                echo $args[ 'before_title' ] . apply_filters('widget_title', $instance[ 'title' ]) . $args[ 'after_title' ];
            }
            $layout = $instance[ 'layout_type' ];
            $template = $instance[ 'template' ];
            $number = $instance[ 'number' ];
            echo do_shortcode('[ebd_featured layout="' . $layout . '" template="' . $template . '" number="' . $number . '"]');
            echo $args[ 'after_widget' ];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance) {
            $title = !empty($instance[ 'title' ]) ? $instance[ 'title' ] : esc_html__('New title', 'everest-business-directories');
            $layout_type = !empty($instance[ 'layout_type' ]) ? $instance[ 'layout_type' ] : 'list';
            $template = !empty($instance[ 'template' ]) ? $instance[ 'template' ] : 'template-1';
            $number = !empty($instance[ 'number' ]) ? $instance[ 'number' ] : 10;
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'everest-business-directories'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('layout_type')); ?>"><?php esc_attr_e('Layout', 'everest-business-directories'); ?></label>
                <select class="widefat" id="<?php echo esc_attr($this->get_field_id('layout_type')); ?>" name="<?php echo esc_attr($this->get_field_name('layout_type')); ?>">
                    <option value="list" <?php selected($layout_type, 'list'); ?>><?php _e('List', 'everest-business-directory'); ?></option>
                    <option value="grid" <?php selected($layout_type, 'grid'); ?>><?php _e('Grid', 'everest-business-directory'); ?></option>
                </select>
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('template')); ?>"><?php esc_attr_e('Template', 'everest-business-directories'); ?></label>
                <select class="widefat" id="<?php echo esc_attr($this->get_field_id('template')); ?>" name="<?php echo esc_attr($this->get_field_name('template')); ?>">
                    <?php
                    for ( $i = 1; $i <= 5; $i++ ) {
                        ?>
                        <option value="template-<?php echo $i; ?>" <?php selected($template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_attr_e('Number', 'everest-business-directories'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="number" min="1" value="<?php echo esc_attr($number); ?>">
            <p class="description"><?php _e('Please enter the total number of featured directories that you want to display. Default is 10.', 'everest-business-directory'); ?></p>
            </p>
            <?php
        }

        /**
         * Sanitize widget form values as they are saved.
         *
         * @see WP_Widget::update()
         *
         * @param array $new_instance Values just sent to be saved.
         * @param array $old_instance Previously saved values from database.
         *
         * @return array Updated safe values to be saved.
         */
        public function update($new_instance, $old_instance) {
            $instance = array();
            $instance[ 'title' ] = (!empty($new_instance[ 'title' ]) ) ? sanitize_text_field($new_instance[ 'title' ]) : '';
            $instance[ 'layout_type' ] = (!empty($new_instance[ 'layout_type' ]) ) ? sanitize_text_field($new_instance[ 'layout_type' ]) : 'list';
            $instance[ 'template' ] = (!empty($new_instance[ 'template' ]) ) ? sanitize_text_field($new_instance[ 'template' ]) : 'template-1';
            $instance[ 'number' ] = (!empty($new_instance[ 'number' ]) ) ? sanitize_text_field($new_instance[ 'number' ]) : 10;

            return $instance;
        }

    }

    // class Foo_Widget

    new EBD_Featured_Widget();
}