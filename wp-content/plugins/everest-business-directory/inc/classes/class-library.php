<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Library') ) {

    class EBD_Library {

        function print_array($array) {
            if ( isset($_GET[ 'debug' ]) ) {
                echo "<pre>";
                print_r($array);
                echo "</pre>";
            }
        }

        function permission_denied() {
            die('No script kiddies please!!');
        }

        /**
         * Sanitizes Multi Dimensional Array
         * @param array $array
         * @param array $sanitize_rule
         * @return array
         *
         * @since 1.0.0
         */
        function sanitize_array($array = array(), $sanitize_rule = array()) {
            if ( !is_array($array) || count($array) == 0 ) {
                return array();
            }

            foreach ( $array as $k => $v ) {
                if ( !is_array($v) ) {

                    $default_sanitize_rule = (is_numeric($k)) ? 'html' : 'text';
                    $sanitize_type = isset($sanitize_rule[ $k ]) ? $sanitize_rule[ $k ] : $default_sanitize_rule;
                    $array[ $k ] = $this->sanitize_value($v, $sanitize_type);
                }
                if ( is_array($v) ) {
                    $array[ $k ] = $this->sanitize_array($v, $sanitize_rule);
                }
            }

            return $array;
        }

        /**
         * Sanitizes Value
         *
         * @param type $value
         * @param type $sanitize_type
         * @return string
         *
         * @since 1.0.0
         */
        function sanitize_value($value = '', $sanitize_type = 'text') {
            switch ( $sanitize_type ) {
                case 'html':
                    $allowed_html = wp_kses_allowed_html('post');
                    return wp_kses($value, $allowed_html);
                    break;
                case 'to_br':
                    return $this->sanitize_escaping_linebreaks($value);
                    break;
                case 'none':
                    return $value;
                    break;
                default:
                    return sanitize_text_field($value);
                    break;
            }
        }

        /**
         * Returns the default frontend fields
         *
         * @return array
         */
        function get_frontend_fields() {
            $frontend_fields = array( 'directory_title' => array(),
                'directory_content' => array(),
                'directory_image' => array(),
                'directory_category' => array(),
                'directory_tags' => array(),
                'contact_information' => array(),
                'social_information' => array(),
                'location_information' => array(),
                'directory_gallery' => array(),
                'directory_expiry_date' => array()
            );
            return $frontend_fields;
        }

        function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0) {
            foreach ( $cats as $i => $cat ) {
                if ( $cat->parent == $parentId ) {
                    $into[ $cat->term_id ] = $cat;
                    unset($cats[ $i ]);
                }
            }

            foreach ( $into as $topCat ) {
                $topCat->children = array();
                $this->sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
            }
        }

        function check_parent($term, $space = '') {
            if ( is_object($term) ) {
                if ( $term->parent != 0 ) {
                    $space .= str_repeat('&nbsp;', 2);
                    $parent_term = get_term_by('id', $term->parent, $term->taxonomy);
                    // var_dump($space);
                    $space .= $this->check_parent($parent_term, $space);
                }
            }

            return $space;
        }

        /**
         * Prints terms in checkbox with hierarchical format
         *
         * @since 1.0.0
         *
         * @param array $terms
         * @param array $exclude_terms
         * @param int $hierarchical
         * @param string $form
         * @param string $field_title
         * @param array $checked_term
         * @return string
         */
        function print_checkbox($terms, $exclude_terms = array(), $hierarchical = 1, $form = '', $field_title = '', $checked_term = array()) {


            foreach ( $terms as $term ) {
                if ( !in_array($term->slug, $exclude_terms) ) {
                    $space = $this->check_parent($term);
                    $option_value = ($hierarchical == 0) ? $term->name : $term->term_id;
                    // var_dump($option_value);
                    //  var_dump($checked_term);
                    //  echo '<br/>';

                    $checked = (in_array($option_value, $checked_term)) ? 'checked="checked"' : '';
                    $form .= '<label class="ebd-checkbox-label">' . $space . '<input type="checkbox" name="' . $field_title . '[]"  value="' . $option_value . '" id="ebd-category-' . $option_value . '" ' . $checked . '/><label for="ebd-category-' . $option_value . '" >' . $term->name . '</label></label>';
                }


                if ( !empty($term->children) ) {

                    $form .= $this->print_checkbox($term->children, $exclude_terms, $hierarchical, '', $field_title, $checked_term);
                }
            }

            return $form;
        }

        /**
         * Prints terms in checkbox with hierarchical format
         *
         * @since 1.0.0
         *
         * @param array $terms
         * @param array $exclude_terms
         * @param int $hierarchical
         * @param string $form
         * @param string $field_title
         * @param string $selected_term
         * @param string $taxonomy_print
         * @return string
         */
        function print_option($terms, $exclude_terms = array(), $hierarchical = 1, $form = '', $field_title = '', $selected_term = '', $taxonomy_print = false) {
            // $this->print_array($terms);

            foreach ( $terms as $term ) {
                if ( !in_array($term->slug, $exclude_terms) ) {
                    $space = $this->check_parent($term);
                    $option_value = ($hierarchical == 0) ? $term->name : $term->term_id;
                    if ( $taxonomy_print ) {
                        $option_value = $option_value . '|' . $term->taxonomy;
                    }
                    if ( is_array($selected_term) ) {
                        $selected = (in_array($option_value, $selected_term)) ? 'selected="selected"' : '';
                    } else {

                        $selected = ($selected_term == $option_value) ? 'selected="selected"' : '';
                    }
                    /*  var_dump($selected_term);
                      var_dump($option_value);
                      var_dump($selected);
                     *
                     */
                    $form .= '<option value="' . $option_value . '" ' . $selected . '>' . $space . $term->name . '</option>';
                }


                if ( !empty($term->children) ) {

                    $form .= $this->print_option($term->children, $exclude_terms, $hierarchical, '', $field_title, $selected_term, $taxonomy_print);
                }
            }

            return $form;
        }

        /**
         * Returns id of first author of WordPress
         *
         * @since 1.0.0
         *
         * @return int
         */
        function get_first_author() {
            $users = get_users();
            return $users[ 0 ]->ID;
        }

        /**
         * Prints display none
         *
         * @param string $first_param
         * @param string $second_param
         *
         * @since 1.0.0
         *
         * @return void
         */
        function display_none($first_param, $second_param) {
            echo ($first_param != $second_param) ? 'style="display:none"' : '';
        }

        /**
         * Generates current page URL
         *
         * @return string $pageURL
         *
         * @since 1.0.0
         */
        function get_current_page_url() {
            $pageURL = 'http';
            if ( isset($_SERVER[ "HTTPS" ]) && $_SERVER[ "HTTPS" ] == "on" ) {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            if ( $_SERVER[ "SERVER_PORT" ] != "80" ) {
                $pageURL .= $_SERVER[ "SERVER_NAME" ] . ":" . $_SERVER[ "SERVER_PORT" ] . $_SERVER[ "REQUEST_URI" ];
            } else {
                $pageURL .= $_SERVER[ "SERVER_NAME" ] . $_SERVER[ "REQUEST_URI" ];
            }
            $pageURL = explode('?', $pageURL);
            $pageURL = $pageURL[ 0 ];
            return $pageURL;
        }

        /**
         * Sanitizes field by converting line breaks to <br /> tags
         *
         * @since 1.0.0
         *
         * @return string $text
         */
        function sanitize_escaping_linebreaks($text) {
            $text = implode("<br \>", array_map('sanitize_text_field', explode("\n", $text)));
            return $text;
        }

        /**
         * Outputs by converting <Br/> tags into line breaks
         *
         * @since 1.0.0
         *
         * @return string $text
         */
        function output_converting_br($text) {
            $text = implode("\n", array_map('sanitize_text_field', explode("<br \>", $text)));
            return $text;
        }

        /**
         * Check if WordPress default login page
         */
        function is_login_page() {
            return in_array($GLOBALS[ 'pagenow' ], array( 'wp-login.php', 'wp-register.php' ));
        }

    }

    $GLOBALS[ 'ebd_directory_library' ] = new EBD_Library();
}