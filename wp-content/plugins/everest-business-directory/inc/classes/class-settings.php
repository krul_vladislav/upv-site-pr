<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Settings') ) {

    class EBD_Settings extends EBD_Library {

        function __construct() {
            add_action('admin_post_ebd_settings_action', array( $this, 'save_settings' ));
        }

        function save_settings() {
            if ( !empty($_POST[ 'ebd_settings_nonce' ]) && wp_verify_nonce($_POST[ 'ebd_settings_nonce' ], 'ebd_settings_nonce') ) {
                global $ebd_settings;
                $_POST = stripslashes_deep($_POST);
                $tab = sanitize_text_field($_POST[ 'tab' ]);
                $sanitize_rule = array( 'reject_notification_message' => 'to_br', 'admin_notification_message' => 'to_br', 'publish_notification_message' => 'to_br', 'single_page_template_markup' => 'none', 'archive_page_template_markup' => 'none', 'sidebar_markup' => 'none' );
                $ebd_settings[ $tab ] = $this->sanitize_array($_POST[ 'ebd_settings' ][ $tab ], $sanitize_rule);
                $this->update_settings($ebd_settings);
                wp_redirect(admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=' . $tab . '&message=1'));
                exit();
            } else {
                $this->permission_denied();
            }
        }

        function get_settings() {
            $ebd_settings = get_settings('ebd_settings');
            return $ebd_settings;
        }

        function update_settings($ebd_settings) {
            update_option('ebd_settings', $ebd_settings);
        }

    }

    new EBD_Settings();
}

