<?php

defined('ABSPATH') or defined('No script kiddies please!!');
if ( !class_exists('EBD_Enqueue') ) {

    class EBD_Enqueue {

        function __construct() {
            add_action('wp_enqueue_scripts', array( $this, 'register_frontend_assets' ));
            add_action('admin_enqueue_scripts', array( $this, 'register_admin_assets' ));
        }

        /**
         * Registers CSS and JS for frontend
         *
         * @since 1.0.0
         */
        function register_frontend_assets() {
            global $ebd_settings;
            $translation_strings = array( 'directory_delete' => __('Are you sure you want to delete this directory?', 'everest-business-directory') );
            $frontend_js_obj = array( 'ajax_url' => admin_url('admin-ajax.php'), 'ajax_nonce' => wp_create_nonce('ebd_ajax_nonce'), 'translation_strings' => $translation_strings );
            wp_enqueue_style('ebd-jquery-ui-css', EBD_CSS_URL . 'jquery-ui.css', array(), EBD_VERSION);
            wp_enqueue_style('ebd-fileuploader-style', EBD_CSS_URL . 'ebd-fileuploader.css', array(), EBD_VERSION);
            wp_enqueue_style('select2-style', 'https://unpkg.com/multiple-select@1.3.1/dist/multiple-select.css', array(), EBD_VERSION);
            wp_enqueue_style('ebd-prettyPhoto-style', EBD_CSS_URL . 'prettyPhoto.css', array(), EBD_VERSION);
            if ( is_rtl() ) {
                wp_enqueue_style('ebd-rtl-style', EBD_CSS_URL . 'ebd-frontend-rtl.css', array(), EBD_VERSION);
            } else {
                wp_enqueue_style('ebd-frontend-style', EBD_CSS_URL . 'ebd-frontend.css', array(), EBD_VERSION);
                wp_enqueue_style('ebd-responsive-style', EBD_CSS_URL . 'ebd-frontend-responsive.css', array(), EBD_VERSION);
            }

            /**
             * Icon Picker
             */
            wp_enqueue_style('ebd-fa-brands-css', EBD_CSS_URL . 'brands.css', EBD_VERSION);
            wp_enqueue_style('ebd-fa-solid-css', EBD_CSS_URL . 'solid.css', EBD_VERSION);
            wp_enqueue_style('ebd-fa-regular-css', EBD_CSS_URL . 'regular.css', EBD_VERSION);
            wp_enqueue_style('ebd-fontawesome-min-css', EBD_CSS_URL . 'font-awesome.min.css', EBD_VERSION);
            wp_enqueue_style('ebd-fontawesome-style', EBD_CSS_URL . 'font-awesome.css', array(), EBD_VERSION);

            wp_enqueue_style('ebd-linearicon-style', EBD_CSS_URL . 'linearicons.css', array(), EBD_VERSION);
            wp_enqueue_style('ebd-google-font', '//fonts.googleapis.com/css?family=Roboto:400,500,700');
            wp_enqueue_script('ebd-google-captcha', 'https://www.google.com/recaptcha/api.js', array(), EBD_VERSION);
            wp_enqueue_script('ebd-fileuploader-script', EBD_JS_URL . 'ebd-fileuploader.js', array( 'jquery' ), EBD_VERSION);
            wp_enqueue_script('ebd-prettyphoto-script', EBD_JS_URL . 'jquery.prettyPhoto.js', array( 'jquery' ), EBD_VERSION);
            $google_map_api_key = (!empty($ebd_settings[ 'map' ][ 'map_api_key' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_api_key' ]) : '';
            ///  wp_enqueue_script('ebd-googlemap-script', 'https://maps.googleapis.com/maps/api/js?key=' . $google_map_api_key, array(), EBD_VERSION);
            wp_enqueue_script('ebd-frontend-script', EBD_JS_URL . 'ebd-frontend.js', array( 'jquery', 'ebd-fileuploader-script', 'jquery-ui-datepicker', 'ebd-prettyphoto-script' ), EBD_VERSION);
            wp_enqueue_script('select2', 'https://unpkg.com/multiple-select@1.3.1/dist/multiple-select.js', array( 'jquery' ), EBD_VERSION);
            wp_localize_script('ebd-frontend-script', 'ebd_js_obj', $frontend_js_obj);
        }

        /**
         * Registers CSS and JS for admin
         *
         * @since 1.0.0
         */
        function register_admin_assets() {
            $ajax_nonce = wp_create_nonce('ebd-ajax-nonce');
            $js_obj = array( 'ajax_url' => admin_url('admin-ajax.php'), 'ajax_nonce' => $ajax_nonce );

            /**
             * Icon Picker
             */
            wp_enqueue_style('ebd-fa-brands-css', EBD_CSS_URL . 'fa-brands.css', EBD_VERSION);
            wp_enqueue_style('ebd-fa-solid-css', EBD_CSS_URL . 'fa-solid.css', EBD_VERSION);
            wp_enqueue_style('ebd-fa-regular-css', EBD_CSS_URL . 'fa-regular.css', EBD_VERSION);
            wp_enqueue_style('ebd-fontawesome-min-css', EBD_CSS_URL . 'font-awesome.min.css', EBD_VERSION);
            wp_enqueue_script('ebd-icon-picker-js', EBD_JS_URL . 'icon-picker.js', array( 'jquery' ), EBD_VERSION);

            wp_enqueue_style('ebd-jquery-ui-css', EBD_CSS_URL . 'jquery-ui.css', array(), EBD_VERSION);
            wp_enqueue_script('ebd-colorpicker', EBD_JS_URL . 'wp-color-picker-alpha.js', array( 'wp-color-picker' ), EBD_VERSION);
            wp_enqueue_style('wp-color-picker');
            wp_enqueue_style('ebd-backend-style', EBD_CSS_URL . 'ebd-backend.css', array(), EBD_VERSION);
            wp_enqueue_script('ebd-backend-script', EBD_JS_URL . 'ebd-backend.js', array( 'jquery', 'wp-util', 'jquery-ui-sortable', 'jquery-ui-datepicker', 'ebd-icon-picker-js', 'ebd-colorpicker' ), EBD_VERSION);
            wp_localize_script('ebd-backend-script', 'ebd_backend_js_obj', $js_obj);
        }

    }

    new EBD_Enqueue();
}