<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Activation') ) {

    class EBD_Activation {

        function __construct() {
            register_activation_hook(EBD_PATH . 'everest-business-directory.php', array( $this, 'activation_tasks' ));
        }

        function activation_tasks() {
            update_option('ebd_plugin_state_change', 'yes');
        }

    }

    new EBD_Activation();
}