<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('EBD_Shortcode') ) {

    class EBD_Shortcode extends EBD_Library {

        function __construct() {
            add_shortcode('ebd_frontend_form', array( $this, 'frontend_form_shortcode' ));
            add_action('wp_login_failed', array( $this, 'login_failed' ));
            add_filter('authenticate', array( $this, 'verify_username_password' ), 1, 3);
            add_action('login_form', array( $this, 'login_extra_fields' ));
            add_filter('login_form_middle', array( $this, 'login_extra_fields' ));
            add_shortcode('ebd_featured', array( $this, 'featured_directory_listing' ));
            add_shortcode('ebd_search_form', array( $this, 'search_form_shortcode' ));
            add_shortcode('ebd_directory_categories_list', array( $this, 'directory_categories_list' ));
            add_shortcode('ebd_directory_tags_list', array( $this, 'directory_tags_list' ));
            add_shortcode('ebd_directory_dashboard', array( $this, 'directory_dashboard' ));
            add_shortcode('ebd_directory_listing', array( $this, 'directory_listing' ));
            add_shortcode('ebd_directory_specific_listing', array( $this, 'directory_specific_listing' ));
        }

        function frontend_form_shortcode($atts) {
            global $ebd_settings;
            $frontend_form_settings = $ebd_settings[ 'frontend_form' ];
            ob_start();
            if ( !empty($frontend_form_settings[ 'general' ][ 'check_login' ]) ) {
                if ( is_user_logged_in() ) {
                    include(EBD_PATH . '/inc/views/frontend/frontend-form.php');
                } else {
                    include(EBD_PATH . '/inc/views/frontend/login.php');
                }
            } else {

                include(EBD_PATH . '/inc/views/frontend/frontend-form.php');
            }
            $form_html = ob_get_contents();
            ob_clean();
            return $form_html;
        }

        function redirect_login_page() {
            if ( isset($_POST[ 'requested_page' ]) ) {
                $login_page = esc_url($_POST[ 'requested_page' ]);
                $page_viewed = basename($_SERVER[ 'REQUEST_URI' ]);

                if ( $page_viewed == "wp-login.php" && $_SERVER[ 'REQUEST_METHOD' ] == 'GET' ) {
                    wp_redirect($login_page);
                    exit;
                }
            }
        }

        function login_failed() {
            if ( isset($_POST[ 'requested_page' ]) ) {
                $login_page = esc_url($_POST[ 'requested_page' ]);
                wp_redirect($login_page . '?login=failed');
                exit;
            }
        }

        function verify_username_password($user, $username, $password) {
            if ( isset($_POST[ 'requested_page' ]) ) {
                $login_page = esc_url($_POST[ 'requested_page' ]);


                if ( $username == "" || $password == "" ) {
                    wp_redirect($login_page . "?login=empty");
                    exit;
                } else {

                }
            }
        }

        function login_extra_fields($login_form_buttom_html) {
            if ( !$this->is_login_page() ) {
                $current_page_url = $this->get_current_page_url();
                $login_form_html = '<input type="hidden" name="requested_page" value="' . $current_page_url . '"/>';
                return $login_form_buttom_html . $login_form_html;
            } else {
                return $login_form_buttom_html;
            }
        }

        function is_login_page() {
            return in_array($GLOBALS[ 'pagenow' ], array( 'wp-login.php', 'wp-register.php' ));
        }

        function featured_directory_listing($atts) {
            ob_start();
            include(EBD_PATH . 'inc/views/frontend/featured-directory-shortcode.php');
            $directory_detail_content = ob_get_contents();
            ob_clean();
            return $directory_detail_content;
        }

        function search_form_shortcode() {
            $shortcode = true;
            ob_start();
            ebd_load_template_part('archive/search-form');
            $directory_detail_content = ob_get_contents();
            ob_clean();
            return $directory_detail_content;
        }

        function directory_categories_list($atts) {
            ob_start();
            include(EBD_PATH . 'inc/views/frontend/categories-list.php');
            $directory_categories_list_content = ob_get_contents();
            ob_clean();
            return $directory_categories_list_content;
        }

        function directory_tags_list($atts) {
            ob_start();
            include(EBD_PATH . 'inc/views/frontend/tags-list.php');
            $directory_categories_list_content = ob_get_contents();
            ob_clean();
            return $directory_categories_list_content;
        }

        function directory_dashboard() {
            ob_start();
            if ( is_user_logged_in() ) {
                if ( isset($_GET[ 'action' ], $_GET[ 'directory_id' ]) && $_GET[ 'action' ] == 'edit' ) {
                    include(EBD_PATH . '/inc/views/frontend/directory-edit.php');
                } else {
                    include(EBD_PATH . '/inc/views/frontend/directory-dashboard.php');
                }
            } else {
                include(EBD_PATH . '/inc/views/frontend/login.php');
            }

            $dashboard_html = ob_get_contents();
            ob_clean();
            return $dashboard_html;
        }

        function directory_listing($atts) {
            ob_start();
            include(EBD_PATH . '/inc/views/frontend/directory-listing.php');
            $listing_html = ob_get_contents();
            ob_clean();
            return $listing_html;
        }

        function directory_specific_listing($atts) {
            ob_start();
            include(EBD_PATH . 'inc/views/frontend/directory-specific-listing-shortcode.php');
            $directory_detail_content = ob_get_contents();
            ob_clean();
            return $directory_detail_content;
        }

    }

    new EBD_Shortcode();
}