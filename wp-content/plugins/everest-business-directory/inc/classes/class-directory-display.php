<?php
if ( !class_exists('EBD_Directory_Display') ) {

    class EBD_Directory_Display extends EBD_Library {

        function __construct() {

            add_filter('the_content', array( $this, 'directory_pages' ));
            add_filter('single_template', array( $this, 'directory_single_page' ));
            add_filter('archive_template', array( $this, 'directory_archive_page' ));
            add_action('ebd_before_main_content', array( $this, 'ebd_output_content_wrapper' ));
            add_action('ebd_after_main_content', array( $this, 'ebd_output_content_wrapper_end' ));
            add_action('ebd_load_archive_templates', array( $this, 'load_directory_archive_templates' ));
            add_filter('excerpt_length', array( $this, 'custom_excerpt_length' ), 999);
            add_filter('excerpt_more', array( $this, 'remove_excerpt_readmore' ), 20);
            add_filter('login_form_middle', array( $this, 'login_google_captcha' ));
            add_filter('authenticate', array( $this, 'login_google_recaptcha_validation' ), 10, 3);
            add_action('pre_get_posts', array( $this, 'ebd_taxonomies_per_page' ), 1, 1);
            add_action('ebd_sidebar', array( $this, 'ebd_sidebar' ));
            add_filter('ebd_query_args', array( $this, 'modify_query_args' ), 100, 1);
            add_action('ebd_main_outer_wrap_start', array( $this, 'ebd_main_outer_wrap_start' ));
            add_action('ebd_main_outer_wrap_end', array( $this, 'ebd_main_outer_wrap_end' ));
            add_action('wp_head', array( $this, 'add_custom_css' ), 10);
            add_action('wp_footer', array( $this, 'add_search_flag' ), 10);
        }

        function directory_pages($content) {
            global $ebd_settings;
            $listing_page = (!empty($ebd_settings[ 'general' ][ 'listing_page' ])) ? intval($ebd_settings[ 'general' ][ 'listing_page' ]) : '';
            $dashboard_page = (!empty($ebd_settings[ 'general' ][ 'dashboard_page' ])) ? intval($ebd_settings[ 'general' ][ 'dashboard_page' ]) : '';
            global $post;
            if ( $post->ID == $listing_page ) {
                ob_start();
                include(EBD_PATH . '/inc/views/frontend/directory-listing.php');
                $listing_html = ob_get_contents();
                ob_clean();
                return $content . $listing_html;
            } else
            if ( $post->ID == $dashboard_page ) {
                ob_start();
                if ( is_user_logged_in() ) {
                    if ( isset($_GET[ 'action' ], $_GET[ 'directory_id' ]) && $_GET[ 'action' ] == 'edit' ) {
                        include(EBD_PATH . '/inc/views/frontend/directory-edit.php');
                    } else {
                        include(EBD_PATH . '/inc/views/frontend/directory-dashboard.php');
                    }
                } else {
                    include(EBD_PATH . '/inc/views/frontend/login.php');
                }

                $dashboard_html = ob_get_contents();
                ob_clean();
                return $content . $dashboard_html;
            } else {
                return $content;
            }
        }

        function the_featured_image($size = 'large') {
            if ( has_post_thumbnail() ) {
                $featured_image_url = get_the_post_thumbnail_url(get_the_ID(), $size);
                $directory_id = get_the_ID();

                global $ebd_settings;
             //   echo '<img src="' . $featured_image_url . '" alt="' . get_the_title() . '"/>';
            } else {
             //   echo '<img src="' . EBD_URL . 'images/no-image.gif' . '" alt="' . get_the_title() . '"/>';
            }
        }

        function the_featured_image_url($size = 'thumbnail', $ribbon = false, $img_tag = true) {
            $image_html = '';
            if ( has_post_thumbnail() ) {
                $featured_image_url = get_the_post_thumbnail_url(get_the_ID(), $size);

            //    return '<img src="' . $featured_image_url . '" alt="' . get_the_title() . '"/>';
            } else {
             //   return '<img src="' . EBD_URL . 'images/no-image.gif' . '" alt="' . get_the_title() . '"/>';
            }
        }

        function the_category_url($directory_term_id) {
            $category_url = get_term_link($directory_term_id, 'ebd-categories');
            echo esc_url($category_url);
        }

        function the_tag_url($directory_term_id) {
            $tag_url = get_term_link($directory_term_id, 'ebd-tags');
            echo esc_url($tag_url);
        }

        function the_directory_url() {
            $directory_url = get_the_permalink();
            echo $directory_url;
        }

        function get_directory_url() {
            return get_the_permalink();
        }

        function directory_detail_page($content) {
            if ( is_singular('ebd') ) {
                ob_start();
                include(EBD_PATH . 'inc/views/frontend/directory-detail.php');
                $directory_detail_content = ob_get_contents();
                ob_clean();
                return $directory_detail_content;
            } else {
                return $content;
            }
        }

        function directory_single_page($single_template) {
            global $post;
            if ( $post->post_type == 'ebd' ) {
                if ( file_exists(get_template_directory() . '/ebd-templates/single-ebd.php') ) {
                    $single_template = get_template_directory() . '/ebd-templates/single-ebd.php';
                } else {
                    $single_template = EBD_PATH . 'ebd-templates/single-ebd.php';
                }
            }
            return $single_template;
        }

        function directory_archive_page($archive_template) {
            global $post;
            //  var_dump(is_post_type_archive(array( 'ebd' )));
            if ( is_tax('ebd-categories') || is_tax('ebd-tags') || is_post_type_archive(array( 'ebd' )) ) {
                if ( file_exists(get_template_directory() . '/ebd-templates/archive-ebd.php') ) {
                    $archive_template = get_template_directory() . '/ebd-templates/archive-ebd.php';
                } else {
                    $archive_template = EBD_PATH . 'ebd-templates/archive-ebd.php';
                }
            }
            return $archive_template;
        }

        function ebd_output_content_wrapper() {
            global $ebd_settings;
            $default_single_template_markup = EBD_DEFAULT_TEMPLATE_MARKUP;
            $single_template_markup = (!empty($ebd_settings[ 'customize' ][ 'single_page_template_markup' ])) ? $ebd_settings[ 'customize' ][ 'single_page_template_markup' ] : $default_single_template_markup;
            $single_template_markup_array = explode('#template_content', $single_template_markup);

            $default_archive_template_markup = EBD_DEFAULT_TEMPLATE_MARKUP;
            $archive_template_markup = (!empty($ebd_settings[ 'customize' ][ 'archive_page_template_markup' ])) ? $ebd_settings[ 'customize' ][ 'archive_page_template_markup' ] : $default_archive_template_markup;
            $archive_template_markup_array = explode('#template_content', $archive_template_markup);
            if ( is_tax('ebd-categories') || is_tax('ebd-tags') ) {
                echo $archive_template_markup_array[ 0 ];
            } else {
                echo $single_template_markup_array[ 0 ];
            }
        }

        function ebd_output_content_wrapper_end() {
            global $ebd_settings;
            $default_single_template_markup = EBD_DEFAULT_TEMPLATE_MARKUP;
            $single_template_markup = (!empty($ebd_settings[ 'customize' ][ 'single_page_template_markup' ])) ? $ebd_settings[ 'customize' ][ 'single_page_template_markup' ] : $default_single_template_markup;
            $single_template_markup_array = explode('#template_content', $single_template_markup);

            $default_archive_template_markup = EBD_DEFAULT_TEMPLATE_MARKUP;
            $archive_template_markup = (!empty($ebd_settings[ 'customize' ][ 'archive_page_template_markup' ])) ? $ebd_settings[ 'customize' ][ 'archive_page_template_markup' ] : $default_archive_template_markup;
            $archive_template_markup_array = explode('#template_content', $archive_template_markup);
            if ( is_tax('ebd-categories') || is_tax('ebd-tags') ) {
                if ( isset($archive_template_markup_array[ 1 ]) ) {

                    echo $archive_template_markup_array[ 1 ];
                }
            } else {
                if ( isset($single_template_markup_array[ 0 ]) ) {
                    echo $single_template_markup_array[ 1 ];
                }
            }
        }

        function load_directory_archive_templates() {
            global $ebd_settings;
            $data = array();
            $data[ 'current_view' ] = $this->get_current_view();
            echo '<div class="ebd-main-wrap">';
            ebd_load_template_part('archive/archive-header', '');
            if ( is_post_type_archive(array( 'ebd' )) ) {
                ebd_load_template_part('archive/search-form', '');
            }
            ebd_load_template_part('archive/directory-view-toggle', '');

            $layout_counter = 0;
            if ( !empty($ebd_settings[ 'general' ][ 'list_layout' ]) ) {
                $layout_counter++;
                $data[ 'layout_counter' ] = $layout_counter;
                ebd_load_template_part('archive/content', 'list-layout', $data);
            }
            if ( !empty($ebd_settings[ 'general' ][ 'grid_layout' ]) ) {
                $layout_counter++;
                $data[ 'layout_counter' ] = $layout_counter;
                ebd_load_template_part('archive/content', 'grid-layout', $data);
            }
            if ( !empty($ebd_settings[ 'general' ][ 'map_view' ]) ) {
                $layout_counter++;
                $data[ 'layout_counter' ] = $layout_counter;
                ebd_load_template_part('archive/content', 'map-view', $data);
            }
            echo "</div>";
        }

        function get_current_view() {
            global $ebd_settings;
            $default_layout = '';
            if ( !empty($ebd_settings[ 'general' ][ 'list_layout' ]) ) {
                $default_layout = 'list';
            }
            if ( !empty($ebd_settings[ 'general' ][ 'grid_layout' ]) ) {
                if ( empty($default_layout) ) {
                    $default_layout = 'grid';
                }
            }
            if ( !empty($ebd_settings[ 'general' ][ 'map_view' ]) ) {
                if ( empty($default_layout) ) {
                    $default_layout = 'map';
                }
            }

            $current_view = (!empty($_GET[ 'layout' ])) ? sanitize_text_field($_GET[ 'layout' ]) : $default_layout;
            return $current_view;
        }

        function custom_excerpt_length($length) {
            global $post;
            global $ebd_settings;

            if ( $post->post_type == 'ebd' ) {
                if ( empty($ebd_settings[ 'general' ][ 'excerpt_length' ]) ) {
                    $length = 25;
                } else {
                    $length = intval($ebd_settings[ 'general' ][ 'excerpt_length' ]);
                }
            }

            return $length;
        }

        function remove_excerpt_readmore($output) {
            global $post;
            if ( $post->post_type == 'ebd' ) {

                return '..';
            } else {

                return $output;
            }
        }

        function get_directory_edit_url($post_id = NULL) {
            $post_id = ($post_id) ? intval($post_id) : get_the_ID();
            global $ebd_settings;
            $dashboard_page = (!empty($ebd_settings[ 'general' ][ 'dashboard_page' ])) ? intval($ebd_settings[ 'general' ][ 'dashboard_page' ]) : '';
            $edit_url = get_permalink($dashboard_page) . '?action=edit&directory_id=' . $post_id;
            return $edit_url;
        }

        function get_directory_delete_url($post_id = NULL) {
            $post_id = ($post_id) ? intval($post_id) : get_the_ID();
            $delete_nonce = wp_create_nonce('ebd_directory_delete_nonce');
            global $ebd_settings;
            $dashboard_page = (!empty($ebd_settings[ 'general' ][ 'dashboard_page' ])) ? intval($ebd_settings[ 'general' ][ 'dashboard_page' ]) : '';
            $delete_url = get_permalink($dashboard_page) . '?action=delete&directory_id=' . $post_id . '&_wpnonce=' . $delete_nonce;
            return $delete_url;
        }

        function login_google_captcha($login_form_buttom_html) {
            /**
             * Don't add this in default login page
             */
            if ( !$this->is_login_page() ) {
                global $ebd_settings;
                if ( !empty($ebd_settings[ 'captcha' ][ 'login_captcha' ]) ) {
                    $site_key = (!empty($ebd_settings[ 'captcha' ][ 'site_key' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'site_key' ]) : '';
                    if ( !empty($site_key) ) {
                        ob_start();
                        ?>
                        <div class="ebd-captcha-wrap">
                            <label><?php echo (!empty($ebd_settings[ 'captcha' ][ 'captcha_label' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'captcha_label' ]) : ''; ?></label>
                            <div class="ebd-field">
                                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                            </div>
                        </div>
                        <input type="hidden" name="ebd_login_check" value="yes"/>
                        <?php
                        $captcha_html = ob_get_contents();
                        ob_end_clean();
                        $login_form_buttom_html .= $captcha_html;
                    }
                }
            }
            return $login_form_buttom_html;
        }

        function login_google_recaptcha_validation($user, $username, $password) {
            if ( !empty($_REQUEST[ 'ebd_login_check' ]) ) {

                global $ebd_settings;

                if ( !empty($ebd_settings[ 'captcha' ][ 'login_captcha' ]) ) {

//                    echo "<pre>";
//                    print_r($_POST);
//                    echo "</pre>";
                    $captcha = sanitize_text_field($_REQUEST[ 'g-recaptcha-response' ]);
                    /* Check if captcha is filled */
                    if ( empty($captcha) ) {
                        wp_redirect(esc_url($_POST[ 'redirect_to' ]) . '/?login=captcha_error');
                        exit;
                    } else {

                        $secret_key = (!empty($ebd_settings[ 'captcha' ][ 'secret_key' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'secret_key' ]) : '';
                        $captcha_response = wp_remote_get("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret_key . "&response=" . $captcha);

                        if ( is_wp_error($captcha_response) ) {
                            wp_redirect(esc_url($_POST[ 'redirect_to' ]) . '/?login=captcha_error');
                            exit;
                        } else {
                            $captcha_response = json_decode($captcha_response[ 'body' ]);
                            if ( $captcha_response->success == false ) {
                                wp_redirect(esc_url($_POST[ 'redirect_to' ]) . '/?login=captcha_error');
                                exit;
                            }
                        }
                    }
                }
            }
        }

        function ebd_taxonomies_per_page($query) {
            if ( !is_admin() && (is_tax('ebd-categories') || is_tax('ebd-tags')) && $query->is_main_query() ) {
                global $ebd_settings;
                $per_page = (!empty($ebd_settings[ 'general' ][ 'per_page' ])) ? intval($ebd_settings[ 'general' ][ 'per_page' ]) : 10;
                $query->set('posts_per_page', $per_page); //set query arg ( key, value )
            }
        }

        function ebd_sidebar() {
            global $ebd_settings;
            if ( !empty($ebd_settings[ 'customize' ][ 'sidebar_enable' ]) ) {
                $default_sidebar_markup = EBD_DEFAULT_SIDEBAR_MARKUP;
                $sidebar_markup = (!empty($ebd_settings[ 'customize' ][ 'sidebar_markup' ])) ? $ebd_settings[ 'customize' ][ 'sidebar_markup' ] : $default_sidebar_markup;
                $sidebar_markup_array = explode('#sidebar_content', $sidebar_markup);
                if ( !empty($sidebar_markup_array[ 0 ]) ) {
                    echo $sidebar_markup_array[ 0 ];
                }
                $sidebar_name = (!empty($ebd_settings[ 'customize' ][ 'sidebar_name' ])) ? esc_attr($ebd_settings[ 'customize' ][ 'sidebar_name' ]) : '';
                get_sidebar(apply_filters('ebd_sidebar_name', $sidebar_name));
                if ( !empty($sidebar_markup_array[ 1 ]) ) {
                    echo $sidebar_markup_array[ 1 ];
                }
            }
        }

        function modify_query_args($directory_listing_args) {
            //return array( 'post_type' => 'ebd' );
            global $ebd_directory_expiry;
            if ( !$ebd_directory_expiry ) {
                if ( isset($directory_listing_args[ 'meta_query' ]) ) {
                    $directory_listing_args_counter = 0;
                    foreach ( $directory_listing_args[ 'meta_query' ] as $key => $value ) {
                        if ( $value[ 'key' ] == '_ebd_expiry_date' ) {
                            unset($directory_listing_args[ 'meta_query' ][ $directory_listing_args_counter ]);
                        }
                        $directory_listing_args[ 'meta_query' ] ++;
                    }
                }
            }
            return $directory_listing_args;
        }

        function ebd_main_outer_wrap_start() {
            global $ebd_settings;
            if ( !empty($ebd_settings[ 'customize' ][ 'sidebar_enable' ]) ) {
                $sidebar_type = (!empty($ebd_settings[ 'customize' ][ 'sidebar_type' ])) ? esc_attr($ebd_settings[ 'customize' ][ 'sidebar_type' ]) : 'right';
                $sidebar_class = 'ebd-' . $sidebar_type . '-sidebar';
            } else {
                $sidebar_class = 'ebd-no-sidebar';
            }
            ?>
            <div class="ebd-main-outer-wrap <?php echo $sidebar_class; ?>">
                <?php
            }

            function ebd_main_outer_wrap_end() {
                echo "</div>";
            }

            function add_custom_css() {
                if ( is_tax('ebd-categories') || is_tax('ebd-tags') || is_singular('ebd') ) {
                    global $ebd_settings;
                    echo "<style>";
                    if ( !empty($ebd_settings[ 'customize' ][ 'wrapper_width' ]) ) {
                        $wrapper_width = esc_attr($ebd_settings[ 'customize' ][ 'wrapper_width' ]);
                        echo ".ebd-main-outer-wrap{max-width:$wrapper_width;}";
                    }
                    if ( !empty($ebd_settings[ 'customize' ][ 'wrapper_bg' ]) ) {
                        $wrapper_bg = esc_attr($ebd_settings[ 'customize' ][ 'wrapper_bg' ]);
                        echo ".ebd-main-outer-wrap .ebd-content-area{background:$wrapper_bg;}";
                    }
                }
                echo "</style>";
            }

            function add_search_flag() {
                if ( isset($_GET[ 'search_keyword' ]) || isset($_GET[ 'location' ]) || isset($_GET[ 'directory_category' ]) || isset($_GET[ 'directory_tag' ]) ) {
                    echo '<input type="hidden" id="ebd-search-flag"/>';
                }
            }

        }

        $GLOBALS[ 'ebd_directory_display' ] = new EBD_Directory_Display();
    }