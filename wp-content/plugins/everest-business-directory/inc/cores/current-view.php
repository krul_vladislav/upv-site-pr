<?php

global $ebd_settings;
$default_layout = '';
$toggle_counter = 0;
if ( !empty($ebd_settings[ 'general' ][ 'list_layout' ]) ) {
    $toggle_counter++;
    $default_layout = 'list';
}
if ( !empty($ebd_settings[ 'general' ][ 'grid_layout' ]) ) {
    $toggle_counter++;
    if ( empty($default_layout) ) {
        $default_layout = 'grid';
    }
}
if ( !empty($ebd_settings[ 'general' ][ 'map_view' ]) ) {
    $toggle_counter++;
    if ( empty($default_layout) ) {
        $default_layout = 'map';
    }
}
$current_view = (!empty($_GET[ 'layout' ])) ? sanitize_text_field($_GET[ 'layout' ]) : $default_layout;
