<?php

if ( !function_exists('ebd_load_template_part') ) {

    function ebd_load_template_part($slug, $name = '', $data = array()) {
        if ( !empty($data) ) {
            foreach ( $data as $key => $val ) {
                $$key = $val;
            }
        }
        if ( empty($name) ) {
            $directory_template_name = $slug . '.php';
        } else {
            $directory_template_name = $slug . '-' . $name . '.php';
        }
        if ( file_exists(get_template_directory() . '/ebd-templates/template-parts/' . $directory_template_name) ) {
            $content_file = get_template_directory() . '/ebd-templates/template-parts/' . $directory_template_name;
        } else {
            $content_file = EBD_PATH . '/ebd-templates/template-parts/' . $directory_template_name;
        }
        include($content_file);
    }

}


