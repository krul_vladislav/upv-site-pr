<?php

global $ebd_settings;
$frontend_form_settings = $ebd_settings[ 'frontend_form' ];
$required_message = esc_attr($frontend_form_settings[ 'general' ][ 'field_required_message' ]);
$form_error_message = esc_attr($frontend_form_settings[ 'general' ][ 'form_error_message' ]);
$form_success_message = esc_attr($frontend_form_settings[ 'general' ][ 'success_message' ]);
//$this->print_array($ebd_settings);
$frontend_field_settings = $ebd_settings[ 'frontend_form' ][ 'frontend_fields' ];
$error_flag = 0;
$error_details = array();
$response = array();
foreach ( $frontend_form_settings[ 'frontend_fields' ] as $field_key => $field_details ) {
    $$field_key = $field_details;
    switch ( $field_key ) {
        case 'directory_title':
        case 'directory_content':
        case 'directory_image':
        case 'directory_tags':
        case 'directory_category':
        case 'directory_gallery':
        case 'directory_expiry_date':
            if ( isset($field_details[ 'show' ], $field_details[ 'required' ]) ) {
                if ( empty($frontend_submitted_fields[ $field_key ]) ) {
                    $error_details[ $field_key ] = $required_message;
                    $error_flag = 1;
                } else {
                    /**
                     * Condition to check if character limit is exceeded
                     */
                    if ( $field_key == 'directory_content' ) {
                        if ( !empty($field_details[ 'content_character_limit' ]) ) {
                            $content_length = strlen(strip_tags($frontend_submitted_fields[ $field_key ]));
                            if ( $content_length > $field_details[ 'content_character_limit' ] ) {
                                $error_details[ $field_key ] = apply_filters('ebd_character_limit_message', __(sprintf('Character Limit exceeded. Only %s characters allowed', $field_details[ 'content_character_limit' ]), 'everest-business-directory'));
                                $error_flag = 1;
                            } else {
                                $$field_key = $frontend_submitted_fields[ $field_key ];
                            }
                        } else {
                            $$field_key = $frontend_submitted_fields[ $field_key ];
                        }
                    } else {
                        $$field_key = $frontend_submitted_fields[ $field_key ];
                    }
                }
            }
            if ( isset($frontend_submitted_fields[ $field_key ]) ) {
                $$field_key = $frontend_submitted_fields[ $field_key ];
            }
            break;
        case 'contact_information':
            if ( isset($field_details[ 'show' ]) ) {
                $contact_information = $frontend_submitted_fields[ 'contact_information' ];
                foreach ( $field_details[ 'fields' ] as $contact_field_key => $contact_field_details ) {
                    if ( isset($contact_field_details[ 'show' ], $contact_field_details[ 'required' ]) ) {
                        if ( empty($frontend_submitted_fields[ 'contact_information' ][ $contact_field_key ]) ) {
                            $error_details[ $contact_field_key ] = $required_message;
                            $error_flag = 1;
                        }
                    }
                }
            }
            break;
        case 'social_information':
            if ( isset($field_details[ 'show' ]) ) {
                $social_information = $frontend_submitted_fields[ 'social_information' ];
                foreach ( $field_details[ 'fields' ] as $contact_field_key => $contact_field_details ) {
                    if ( isset($contact_field_details[ 'show' ], $contact_field_details[ 'required' ]) ) {
                        if ( empty($frontend_submitted_fields[ 'social_information' ][ $contact_field_key ]) ) {
                            $error_details[ $contact_field_key ] = $required_message;
                            $error_flag = 1;
                        }
                    }
                }
            }
            break;
        case 'location_information':
            if ( isset($field_details[ 'show' ]) ) {
                $location_information = $frontend_submitted_fields[ 'location_information' ];
                foreach ( $field_details[ 'fields' ] as $contact_field_key => $contact_field_details ) {
                    if ( isset($contact_field_details[ 'show' ], $contact_field_details[ 'required' ]) ) {
                        if ( empty($frontend_submitted_fields[ 'location_information' ][ $contact_field_key ]) ) {
                            $error_details[ $contact_field_key ] = $required_message;
                            $error_flag = 1;
                        }
                    }
                }
            }
            break;

        default:
            if ( strpos($field_key, '_custom_field_') !== false ) {
                if ( isset($field_details[ 'show' ], $field_details[ 'required' ]) ) {
                    if ( empty($frontend_submitted_fields[ 'custom_fields' ][ $field_key ]) ) {
                        $error_details[ $field_key ] = $required_message;
                        $error_flag = 1;
                    }
                }
            }
            break;
    }
}
if ( !empty($ebd_settings[ 'captcha' ][ 'frontend_captcha' ]) ) {
    $captcha = sanitize_text_field($form_data[ 'g-recaptcha-response' ]); // get the captchaResponse parameter sent from our ajax
    // var_dump($frontend_submitted_fields);
    /* Check if captcha is filled */
    if ( empty($captcha) ) {
        $error_details[ 'captcha' ] = (!empty($ebd_settings[ 'captcha' ][ 'error_message' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'error_message' ]) : $required_message;
        $error_flag = 1;
    } else {

        $secret_key = (!empty($ebd_settings[ 'captcha' ][ 'secret_key' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'secret_key' ]) : '';
        //  echo "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret_key . "&response=" . $captcha;
        $captcha_response = wp_remote_get("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret_key . "&response=" . $captcha);
        //   echo "<pre>";
        //   print_r($response);
        //  echo "</pre>";
        // var_dump($response);
        //  die();
        if ( is_wp_error($captcha_response) ) {
            $error_details[ 'captcha' ] = __('Captcha Validation failed.', 'everest-business-directory');
            $error_flag = 1;
        } else {
            $captcha_response = json_decode($captcha_response[ 'body' ]);
            if ( $captcha_response->success == false ) {
                $error_details[ 'captcha' ] = (!empty($ebd_settings[ 'captcha' ][ 'error_message' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'error_message' ]) : $required_message;
                $error_flag = 1;
            }
        }
    }
}
if ( $error_flag == 1 ) {
    $response[ 'success' ] = 0;
    $response[ 'error_details' ] = $error_details;
    $response[ 'message' ] = esc_attr($form_error_message);
} else {
    if ( is_user_logged_in() ) {
        $author = get_current_user_id();
    } else {
        $author = (!empty($frontend_form_settings[ 'general' ][ 'assign_author' ])) ? intval($frontend_form_settings[ 'general' ][ 'assign_author' ]) : $this->get_first_author();
    }
    $directory_id = (!empty($form_data[ 'ebd_directory_id' ])) ? intval($form_data[ 'ebd_directory_id' ]) : '';
    $update = (!empty($directory_id)) ? true : false;
    $directory_status = (!empty($frontend_form_settings[ 'general' ][ 'directory_status' ])) ? esc_attr($frontend_form_settings[ 'general' ][ 'directory_status' ]) : 'draft';
    /**
     * We are checking this so that directory id is not changed in the form before submitting
     */
    if ( !empty($directory_id) ) {
        $current_user = wp_get_current_user();
        $current_user_id = $current_user->ID;
        $directory_author = get_post_field('post_author', $directory_id);
        if ( $directory_author != $current_user_id ) {
            $response[ 'success' ] = 0;
            $response[ 'error_details' ] = $error_details;
            $response[ 'message' ] = __('Invalid directory id', 'everest-business-directory');
            die(json_encode($response));
        }
    }

    $directory_arguments = array( 'post_type' => 'ebd',
        'post_title' => $directory_title,
        'post_content' => $directory_content,
        'post_status' => $directory_status,
        'post_author' => $author
    );
    if ( !empty($directory_id) ) {
        $directory_arguments[ 'ID' ] = $directory_id;
        $post_status = get_post_status($directory_id);
        $directory_arguments[ 'post_status' ] = $post_status;
    }
    //  $this->print_array($directory_arguments);

    /**
     * Insert or update Directory Post
     */
    if ( empty($directory_id) ) {
        $directory_id = wp_insert_post($directory_arguments);
    } else {
        wp_update_post($directory_arguments);
    }

    /**
     * Assign directory category if available in the form
     */
    if ( !empty($directory_category) ) {
        wp_set_post_terms($directory_id, $directory_category, 'ebd-categories', false);
    }

    /**
     * Assign directory tags if available in the form
     */
    if ( !empty($directory_tags) ) {
        if ( is_array($directory_tags) ) {
            $directory_tags = implode(',', $directory_tags);
        }
        wp_set_post_terms($directory_id, $directory_tags, 'ebd-tags', false);
    }

    /**
     * Assign featured image
     */
    if ( !empty($directory_image) ) {
        update_post_meta($directory_id, '_thumbnail_id', intval($directory_image), true);
    }

    /**
     * Add directory gallery
     */
    if ( !empty($directory_gallery) ) {
        update_post_meta($directory_id, '_ebd_gallery', explode(',', $directory_gallery));
    }

    /**
     * Contact Information
     */
    if ( !empty($contact_information) ) {
        //  var_dump($contact_information);
        foreach ( $contact_information as $contact_field_key => $contact_field_value ) {
            $meta_key = '_ebd_' . $contact_field_key;
            update_post_meta($directory_id, $meta_key, $contact_field_value);
        }
    }
    /**
     * Location Information
     */
    if ( !empty($location_information) ) {
        foreach ( $location_information as $location_field_key => $location_field_value ) {
            $meta_key = '_ebd_' . $location_field_key;
            update_post_meta($directory_id, $meta_key, $location_field_value);
        }
    }
    /**
     * Social Information
     */
    if ( !empty($social_information) ) {
        foreach ( $social_information as $social_field_key => $social_field_value ) {
            $meta_key = '_ebd_' . $social_field_key;
            update_post_meta($directory_id, $meta_key, $social_field_value);
        }
    }

    /**
     * Custom fields
     */
    if ( !empty($frontend_submitted_fields[ 'custom_fields' ]) ) {
        foreach ( $frontend_submitted_fields[ 'custom_fields' ] as $field_key => $field_value ) {
            $field_key = str_replace('_custom_field_', '', $field_key);
            if ( isset($ebd_settings[ 'custom_fields' ][ $field_key ]) ) {
                // echo $field_key;
                update_post_meta($directory_id, $field_key, $field_value);
            }
        }
    }

    /**
     * Directory Expiry Date
     */
    if ( !empty($frontend_submitted_fields[ 'directory_expiry_date' ]) ) {
        update_post_meta($directory_id, '_ebd_expiry_date', $directory_expiry_date);
    }
    if ( !empty($frontend_form_settings[ 'email' ][ 'admin_notification' ]) ) {
        /**
         * ebd_admin_notification hook
         *
         * @hooked in EBD_Email_Notification::send_admin_notification
         */
        do_action('ebd_admin_notification', $directory_id);
    }
    $response[ 'redirect' ] = (!empty($frontend_form_settings[ 'general' ][ 'redirect_url' ])) ? esc_url($frontend_form_settings[ 'general' ][ 'redirect_url' ]) : '';
    $response[ 'update' ] = $update;
    $response[ 'success' ] = 1;
    $response[ 'message' ] = esc_attr($form_success_message);
}
die(json_encode($response));
