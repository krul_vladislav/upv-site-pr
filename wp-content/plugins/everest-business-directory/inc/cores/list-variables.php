<?php

global $ebd_settings;
global $ebd_directory_display;
global $ebd_directory_expiry;
$directory_id = get_the_ID();
$directory_detail_settings = $ebd_settings[ 'directory_detail' ];
$location_fields = array( 'latitude', 'longitude', 'address', 'city', 'country', 'postal_code' );
$contact_fields = array( 'email_address', 'website', 'phone_number' );
$social_fields = array( 'facebook', 'twitter', 'googleplus', 'instagram', 'youtube', 'linkedin' );
$directory_fields = array( 'location_fields', 'contact_fields', 'social_fields' );
foreach ( $directory_fields as $directory_field_key ) {
    foreach ( $$directory_field_key as $field_key ) {
        $$field_key = get_post_meta($directory_id, '_ebd_' . $field_key, true);
    }
}
$secondary_address = array();
if ( !empty($city) ) {
    $secondary_address[] = $city;
}
if ( !empty($country) ) {
    $secondary_address[] = $country;
}
if ( !empty($secondary_address) ) {
    $address = (empty($address)) ? implode(', ', $secondary_address) : $address;
}
$directory_gallery = get_post_meta($directory_id, '_ebd_gallery', true);
$directory_categories = get_the_terms($directory_id, 'ebd-categories');
$directory_tags = get_the_terms($directory_id, 'ebd-tags');
$image_class = (has_post_thumbnail()) ? 'ebd-yes-image' : 'ebd-no-image';
$expiry_date = ($ebd_directory_expiry) ? get_post_meta($directory_id, '_ebd_expiry_date', true) : '';
$featured_directory = get_post_meta($directory_id, '_ebd_featured', true);
$featured_directory_class = (!empty($featured_directory)) ? 'ebd-featured-directory' : '';
if ( !empty($expiry_date) ) {
    $expiry_date = date('F d,Y', strtotime($expiry_date));
}
$directory_title = get_the_title();
$featured_text = (!empty($ebd_settings[ 'general' ][ 'featured_text' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_text' ]) : '';
$featured_template = (!empty($ebd_settings[ 'general' ][ 'featured_ribbon_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_ribbon_template' ]) : 'template-1';
$featured_template = (!empty($featured_directory)) ? $featured_template : '';
$featured_template_class = (!empty($featured_template)) ? 'ebd-ribbon-' . $featured_template : '';
$view_detail_label = (!empty($ebd_settings[ 'general' ][ 'view_detail_link_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'view_detail_link_label' ]) : __('View Detail', 'everest-business-directory');
$view_detail_link = (!empty($ebd_settings[ 'general' ][ 'view_detail_link' ])) ? esc_attr($ebd_settings[ 'general' ][ 'view_detail_link' ]) : '';
if ( !(is_singular('ebd') ) || isset($listing) ) {
    if ( is_object($directory_categories) && count($directory_categories) > 2 ) {
        $directory_categories = array_slice($directory_categories, 0, 2);
    }
    if ( is_object($directory_tags) && count($directory_tags) > 2 ) {
        $directory_tags = array_slice($directory_tags, 0, 2);
    }
}


