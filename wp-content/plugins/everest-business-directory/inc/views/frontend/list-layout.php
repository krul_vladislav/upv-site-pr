<?php
$listing_template = (!empty($ebd_settings[ 'general' ][ 'list_layout_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'list_layout_template' ]) : 'template-1';
$listing_template = apply_filters('ebd_list_template', $listing_template);
$listing_template_class = 'ebd-list-' . $listing_template;
$_GET[ 'layout' ] = $layout = 'list';
?>
<div class="ebd-directory-listing-wrap <?php echo $listing_template_class; ?> <?php echo 'ebd-' . $layout . '-layout'; ?>" <?php $this->display_none($current_view, $layout); ?>>

    <?php include(EBD_PATH . 'inc/views/frontend/common/list-query.php'); ?>
</div>
