<div class="ebd-login-form-wrapper">
    <?php
    $username_label = (!empty($frontend_form_settings[ 'general' ][ 'login_username_label' ])) ? esc_attr($frontend_form_settings[ 'general' ][ 'login_username_label' ]) : __('Username', 'everest-business-directory');
    $password_label = (!empty($frontend_form_settings[ 'general' ][ 'login_password_label' ])) ? esc_attr($frontend_form_settings[ 'general' ][ 'login_password_label' ]) : __('Password', 'everest-business-directory');
    $submit_label = (!empty($frontend_form_settings[ 'general' ][ 'login_submit_label' ])) ? esc_attr($frontend_form_settings[ 'general' ][ 'login_submit_label' ]) : __('Login In', 'everest-business-directory');
    $remember_label = (!empty($frontend_form_settings[ 'general' ][ 'login_remember_label' ])) ? esc_attr($frontend_form_settings[ 'general' ][ 'login_remember_label' ]) : __('Remember', 'everest-business-directory');
    global $user_login;
    $login = (isset($_GET[ 'login' ]) ) ? sanitize_text_field($_GET[ 'login' ]) : 0;

    // In case of a login error.
    if ( $login === "failed" ) {
        echo '<p class="login-msg"><strong>ERROR:</strong> Invalid username or password.</p>';
    } elseif ( $login === "empty" ) {
        echo '<p class="login-msg"><strong>ERROR:</strong> Username or Password is empty.</p>';
    } elseif ( $login === "captcha_error" ) {
        $captcha_error_message = (!empty($ebd_settings[ 'captcha' ][ 'error_message' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'error_message' ]) : __('Invalid Captcha', 'everest-business-directory');
        echo '<p class="login-msg"><strong>ERROR:</strong> ' . $captcha_error_message . '</p>';
    }
    $args = array(
        'echo' => true,
        'redirect' => $this->get_current_page_url(),
        'form_id' => 'ebd-login-form',
        'label_username' => $username_label,
        'label_password' => $password_label,
        'label_log_in' => $submit_label,
        'id_username' => 'user_login',
        'id_password' => 'user_pass',
        'id_remember' => 'rememberme',
        'id_submit' => 'wp-submit',
        'remember' => true,
        'label_remember' => $remember_label,
        'value_username' => NULL,
        'value_remember' => false
    );

// Calling the login form.
    wp_login_form($args);
    ?>
</div>
<script>jQuery(document).ready(function ($) {
        if (window.location.href.indexOf('?') > -1) {
            history.pushState('', document.title, window.location.pathname);
        }
    });
</script>