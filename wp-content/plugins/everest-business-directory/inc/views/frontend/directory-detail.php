<?php
global $post;
global $ebd_settings;
$directory_detail_settings = $ebd_settings[ 'directory_detail' ];
$directory_id = $post->ID;

$this->print_array($post);
$location_fields = array( 'latitude', 'longitude', 'address', 'city', 'country', 'postal_code' );
$contact_fields = array( 'email_address', 'website', 'phone_number' );
$social_fields = array( 'facebook', 'twitter', 'googleplus', 'instagram', 'youtube', 'linkedin' );
$directory_fields = array( 'location_fields', 'contact_fields', 'social_fields' );
foreach ( $directory_fields as $directory_field_key ) {
    foreach ( $$directory_field_key as $field_key ) {
        $$field_key = get_post_meta($directory_id, '_ebd_' . $field_key, true);
    }
}
$directory_gallery = get_post_meta($directory_id, '_ebd_gallery', true);
$directory_categories = get_the_terms($directory_id, 'ebd-categories');
$directory_tags = get_the_terms($directory_id, 'ebd-tags');
?>
<?php if ( !empty($directory_detail_settings[ 'featured_image' ]) ) { ?>
    <div class="ebd-directory-image">
        <a href="<?php $this->the_directory_url(); ?>">
            <?php
            $this->the_featured_image();
            ?>
        </a>
    </div>
<?php } ?>
<?php if ( !empty($directory_detail_settings[ 'directory_category' ]) ) { ?>
    <div class="ebd-directory-category">
        <?php
        if ( !empty($directory_categories) ) {
            foreach ( $directory_categories as $directory_category ) {
                ?>
                <div class="ebd-each-category"><a href="<?php $this->the_category_url($directory_category->term_id); ?>"><?php echo $directory_category->name; ?></a></div>
                <?php
            }
        }
        ?>
    </div>
<?php } ?>
<?php if ( !empty($directory_detail_settings[ 'directory_tag' ]) ) { ?>
    <div class="ebd-directory-tags">
        <?php
        if ( !empty($directory_tags) ) {
            foreach ( $directory_tags as $directory_tag ) {
                ?>
                <div class="ebd-each-tag"><a href="<?php $this->the_tag_url($directory_tag->term_id); ?>"><?php echo $directory_tag->name; ?></a></div>
                <?php
            }
        }
        ?>
    </div>
<?php } ?>
<div class="ebd-content-wrap"><?php echo $content; ?></div>
<?php if ( !empty($directory_detail_settings[ 'social_information' ]) ) { ?>
    <div class="ebd-social-information">
        <a href="<?php echo $facebook; ?>">Facebook</a>
        <a href="<?php echo $twitter; ?>">Twitter</a>
        <a href="<?php echo $googleplus; ?>">Google Plus</a>
        <a href="<?php echo $instagram; ?>">Instagram</a>
        <a href="<?php echo $youtube; ?>">Youtube</a>
        <a href="<?php echo $linkedin; ?>">Linkedin</a>
    </div>
<?php } ?>
<?php if ( !empty($directory_detail_settings[ 'contact_information' ]) ) { ?>
    <div class="ebd-contact-information-wrap">
        <div class="ebd-each-contact-information">
            <span class="ebd-label"><?php _e('Email Address', 'everest-business-directory'); ?></span>
            <span class="ebd-info-value"><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></span>
        </div>
        <div class="ebd-each-contact-information">
            <span class="ebd-label"><?php _e('Website', 'everest-business-directory'); ?></span>
            <span class="ebd-info-value"><a href=""><?php echo $website; ?></a></span>
        </div>
        <div class="ebd-each-contact-information">
            <span class="ebd-label"><?php _e('Phone Number', 'everest-business-directory'); ?></span>
            <span class="ebd-info-value"><?php echo $phone_number; ?></span>
        </div>
    </div>
<?php } ?>
<?php if ( !empty($directory_detail_settings[ 'location_information' ]) ) { ?>
    <div class="ebd-location-information">
        <div class="ebd-each-location-information">
            <span class="ebd-label"><?php _e('Address', 'everest-business-directory'); ?></span>
            <span class="ebd-info-value"><?php echo $address; ?></span>
        </div>
    </div>
<?php } ?>
<?php if ( !empty($directory_detail_settings[ 'directory_gallery' ]) ) { ?>
    <div class="ebd-gallery-wrap">
        <?php
        if ( !empty($directory_gallery) ) {
            foreach ( $directory_gallery as $gallery_item ) {
                $attachment_src = wp_get_attachment_image_src($gallery_item);
                $attachment_full_src = wp_get_attachment_image_src($gallery_item, 'large');
                ?>
                <div class="ebd-each-gallery">
                    <a href="<?php echo $attachment_full_src[ 0 ] ?>"><img src="<?php echo esc_url($attachment_src[ 0 ]); ?>"/></a>
                </div>
                <?php
            }
        }
        ?>
    </div>
<?php } ?>
<div class="ebd-custom-fields-wrap">
    <?php
    global $ebd_settings;
    if ( !empty($ebd_settings[ 'custom_fields' ]) ) {
        foreach ( $ebd_settings[ 'custom_fields' ] as $field_key => $field_details ) {
            $field_value = get_post_meta($post->ID, $field_key, true);
            if ( !empty($field_value) && !empty($field_details[ 'frontend_display' ]) ) {
                $field_value = (is_array($field_value)) ? implode(', ', $field_value) : $field_value;
                ?>
                <div class="ebd-each-custom-field">
                    <span class="ebd-label"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></span>
                    <span class="ebd-info-value"><?php echo $field_value; ?></span>
                </div>
                <?php
            }
        }
    }
    ?>
</div>
