<?php
if($_POST['custId']){
    echo "111111111";
}



global $ebd_settings;
global $address;
$data['listing'] = true;


if ($directory_listing_query && $directory_listing_query->have_posts()) {

    $directory_counter = 0;
    while ($directory_listing_query->have_posts()) {
        $directory_listing_query->the_post();
        $directory_counter++;
        include(EBD_PATH . 'inc/cores/list-variables.php');
        switch ($layout) {
            case 'list':
                ebd_load_template_part('archive/list/content', 'list-' . $listing_template, $data);
                break;
            case 'grid':
                ebd_load_template_part('archive/grid/content', 'grid-' . $listing_template, $data);

                break;
            case 'map':
                $directory_title = get_the_title();
                global $ebd_settings;
                $directory_id = get_the_ID();
                $featured_directory = get_post_meta($directory_id, '_ebd_featured', true);
                $featured_text = (!empty($ebd_settings['general']['featured_text'])) ? esc_attr($ebd_settings['general']['featured_text']) : '';
                $featured_template = (!empty($ebd_settings['general']['featured_ribbon_template'])) ? esc_attr($ebd_settings['general']['featured_ribbon_template']) : 'template-1';
                $featured_template = (!empty($featured_directory)) ? $featured_template : '';
                $featured_template_class = (!empty($featured_template)) ? 'ebd-ribbon-' . $featured_template : '';
                $ribbon_html = '';
                if ($featured_directory) {
                    $ribbon_html .= '<span class="ebd-featured-tag">' . $featured_text . '</span>';
                }
                $info = '<div class="ebd-map-info ' . $featured_template_class . '">'
                    . '<div class="ebd-info-image">' . $ribbon_html . $ebd_directory_display->the_featured_image_url('medium') . '</div>'
                    . '<div class="ebd-info-content-wrap" style="display: flex; flex-direction: column; justify-content: center; align-items: center" >'
                    . '<div class="ebd-info-title" style="color: gray"><span>' . $directory_title . '</span></div>'
                    . '<div clas class="ebd-info-address"><span class="ebd-address-icon"></span>' . $address . '</div>'
                    . '<div clas class="ebd-info-phone"><span class="ebd-phone-icon"></span><a href="tel:' . $phone_number . '">' . $phone_number . '</a></div>'
                    . '<div clas class="ebd-info-website"><span class="ebd-website-icon"></span><a href="mailto:' . $email_address . '" target="_blank">' . $email_address . '</a></div>'
                    . '<div clas class="ebd-info-button"><a href="#" target="_blank">' . 'voir la fiche' . '</a></div>'
                    . '</div>'
                    . '</div>';
                $info = apply_filters('ebd_map_info', $info, $directory_id);
                $normal_marker_id = (!empty($ebd_settings['map']['map_normal_marker'])) ? esc_attr($ebd_settings['map']['map_normal_marker']) : 1;
                $featured_marker_id = (!empty($ebd_settings['map']['map_featured_marker'])) ? esc_attr($ebd_settings['map']['map_featured_marker']) : 1;
                $normal_marker = EBD_URL . 'images/markers/normal/map-marker-' . $normal_marker_id . '.png';
                $featured_marker = EBD_URL . 'images/markers/featured/map-marker-featured-' . $featured_marker_id . '.png';
                $marker = (!empty($featured_directory)) ? $featured_marker : $normal_marker;
                $featured = (!empty($featured_directory)) ? 1 : 0;
                $directory_array[] = array($info, $latitude, $longitude, $directory_counter - 1, $marker, $featured, $member_type);
                break;
        }
    }
} else {
    if (count($directory_listing_query_new) > 0) {
        include(EBD_PATH . 'inc/cores/list-variables.php');
        $directory_counter = 0;
        foreach ($directory_listing_query_new as $directory) {
            $directory_counter++;
            switch ($layout) {
                case 'list':
                    ebd_load_template_part('archive/list/content', 'list-' . $listing_template, $data);
                    break;
                case 'grid':
                    ebd_load_template_part('archive/grid/content', 'grid-' . $listing_template, $data);

                    break;
                case 'map':
                    $directory_title = "";
                    global $ebd_settings;
                    $directory_id = get_the_ID();
                    $id = $directory->id;
                    $featured_directory = get_post_meta($directory_id, '_ebd_featured', true);
                    $featured_text = (!empty($ebd_settings['general']['featured_text'])) ? esc_attr($ebd_settings['general']['featured_text']) : '';
                    $featured_template = (!empty($ebd_settings['general']['featured_ribbon_template'])) ? esc_attr($ebd_settings['general']['featured_ribbon_template']) : 'template-1';
                    $featured_template = (!empty($featured_directory)) ? $featured_template : '';
                    $featured_template_class = (!empty($featured_template)) ? 'ebd-ribbon-' . $featured_template : '';
                    $ribbon_html = '';
                    if ($featured_directory) {
                        $ribbon_html .= '<span class="ebd-featured-tag">' . $featured_text . '</span>';
                    }
                    $country = ($directory->country = 'BE') ? 'Belgium' : 'Luxembourg';
                    $address = $directory->street . ' ' . $directory->number . ', ' . $directory->zipcode . ', ' . $directory->city;
                    $address = trim($address);
                    $address = rtrim($address, ",");

                    if ($directory->latitude && $directory->longitude) {
                        $info = '<div style="height: 90%; display: flex; flex-direction: column; justify-content: space-between">'
                            . '<div class="ebd-map-info ' . $featured_template_class . '">'
                            . '<div class="ebd-info-image"></div>'
                            . '<div class="ebd-info-content-wrap" id="'. $id. '" style="display: flex; flex-direction: column; justify-content: center; align-items: center; cursor: pointer">'
                            . '<div class="ebd-info-title" id="title_get_id-'. $id. '" ><span data-cust_id="'. $id.'" class="customIdClick" style="color: #5DC0D5; font-weight: bold;" >' . $directory->forename . ' ' . $directory->surname . '</span></div>';
                        if ($address)
                            $info .= '<div  class="ebd-info-address"><span class="ebd-address-icon"></span>' . $address . '</div>';
                        if ($directory->gsm)
                            $info .= '<div class="ebd-info-phone"><span class="ebd-phone-icon"></span><a href="tel:' . $directory->gsm . '">' . $directory->gsm . '</a></div>';
                        if ($directory->email)
                            $info .= '<div class="ebd-info-website"><span class="ebd-website-icon"></span><a href="mailto:' . $directory->email . '" target="_blank">' . $directory->email . '</a></div>';
                        $info .= '</div>';
                        $info .= '</div>';
                        $info .= '<div id="button3" class="ebd-info-button" style=" display: flex; justify-content: flex-end; margin-right: 20px; margin-top: 60px;"><span data-cust_id="'. $id.'" class="customIdClick" style="color: #00B0F0; font-weight: bold; text-decoration: none; cursor: pointer">Voir la fiche </span></div>';
                        $info .= '</div>';
                        $info = apply_filters('ebd_map_info', $info);
                        $normal_marker_id = (!empty($ebd_settings['map']['map_normal_marker'])) ? esc_attr($ebd_settings['map']['map_normal_marker']) : 1;
                        $featured_marker_id = (!empty($ebd_settings['map']['map_featured_marker'])) ? esc_attr($ebd_settings['map']['map_featured_marker']) : 1;
                        $normal_marker = EBD_URL . 'images/markers/normal/map-marker-' . $normal_marker_id . '.png';
                        $featured_marker = EBD_URL . 'images/markers/featured/map-marker-featured-' . $featured_marker_id . '.png';
                        $marker = (!empty($featured_directory)) ? $featured_marker : $normal_marker;
                        $featured = (!empty($featured_directory)) ? 1 : 0;
                        $directory_array[] = array($info, $directory->latitude, $directory->longitude, $directory_counter - 1, $marker, $featured, $directory->member_type);

                    }
                    break;
            }
        }
    } else {
        /**
         * No result found
         */
        ebd_load_template_part('common/no-result-found', '');
    }
    ?>
    
    <?php 
    function address2()
    {
        global $address;
        return $address;
    }

}
?>
    <!-- <script>
    jQuery(document).ready(function ($) {
        $(document).on('click','.customIdClick',function(e){
            e.preventDefault();
            url = $(this).attr('href')+'?contact='+$(this).attr('data-cust_id');
            console.log(url);
            window.location.replace(url);
        });
        
        // function clickTitle(el){
        //     console.log(el.getAttribute("id"));
        //     var id_contact = el.getAttribute("id")
        //     document.getElementById("custId").value = id_contact;
        // }
    });
    </script> -->