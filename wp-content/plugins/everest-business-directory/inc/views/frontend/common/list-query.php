<?php
wp_reset_query();
wp_reset_postdata();
global $directory_array;
global $directory_category;
$directory_array = array();
// echo '111111111111111111111';
if ($layout_counter == 1) {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $per_page = (!empty($ebd_settings['general']['per_page'])) ? intval($ebd_settings['general']['per_page']) : 10;
    $directory_listing_args = array('posts_per_page' => $per_page, 'post_status' => 'publish', 'post_type' => 'ebd', 'paged' => $paged);
    if (!empty($atts['directory_category'])) {
        $directory_category = $atts['directory_category'];
        $tax_query = array(array('taxonomy' => 'ebd-categories', 'field' => 'slug', 'terms' => $directory_category));
        $directory_listing_args['tax_query'] = $tax_query;
    }
    global $ebd_directory_expiry;
    if ($ebd_directory_expiry) {
        $directory_listing_args['meta_query'][] = array('key' => '_ebd_expiry_date', 'value' => date('Y-m-d'), 'compare' => '>=', 'type' => 'DATE');
    }
    if (!empty($_GET['search_keyword'])) {
        $search_keyword = sanitize_text_field($_GET['search_keyword']);
        $directory_listing_args['terms'] = $search_keyword;
        //   echo  $search_keyword;
    }
    if (!empty($_GET['directory_category'])) {
        $directory_category = intval($_GET['directory_category']);
        $tax_query = array(array('taxonomy' => 'ebd-categories', 'field' => 'id', 'terms' => $directory_category));
        $directory_listing_args['radius'] = $directory_category;
        //  echo $directory_category;
    }
    if (!empty($_GET['directory_tag'])) {
        $directory_tag = intval($_GET['directory_tag']);
        $tax_query = array(array('taxonomy' => 'ebd-tags', 'field' => 'id', 'terms' => $directory_tag));
        $directory_listing_args['tax_query'] = $tax_query;
    }
    if (!empty($_GET['location'])) {
        $location = sanitize_text_field($_GET['location']);
        $meta_query = array('key' => '_ebd_address', 'value' => $location, 'compare' => 'LIKE');

        $directory_listing_args['location'] = $location;
    }

    if (!empty($_GET['field_225073'])) {
        $trans = get_html_translation_table(HTML_ENTITIES);
        $field_225073 = str_replace('\\\'', '&#039;', strtr(sanitize_text_field($_GET['field_225073']), $trans));

        $directory_listing_args['field_225073'] = $field_225073;
    }

    $client = new Teamleader($ebd_settings['map']['map_api_key']);
    $directory_listing_query = false;
    $directory_listing_query_new = $client->getContactsFromDB($directory_listing_args);
    //$x=$client->getContactsFromDB2($directory_listing_args);
    //cl_print_r($directory_listing_query_new);
//    echo '<pre>';print_r($directory_listing_query_new); echo '</pre>';

    $temp_query = $directory_listing_query;
} else {
    $directory_listing_query = $temp_query;
}

$data['directory_listing_query'] = $directory_listing_query;
global $ebd_directory_display;
include(EBD_PATH . 'inc/views/frontend/common/query-loop.php');

//echo  address2();
//cl_print_r ($directory_array[0][]);
if ($layout == 'map') {
    $map_width = (!empty($ebd_settings['map']['map_width'])) ? esc_attr($ebd_settings['map']['map_width']) : '100%';
    $map_height = (!empty($ebd_settings['map']['map_height'])) ? esc_attr($ebd_settings['map']['map_height']) : '500px';
    if (empty($directory_listing_query_new)) {
        ?>
        <div id="ebd-map" style="display:none; height: <?php echo $map_height; ?>;width:<?php echo $map_width; ?>;"
             ></div>
             <?php
//      
         } else {
             ?>
        <div id="ebd-map" style="height: <?php echo $map_height; ?>;width:<?php echo $map_width; ?>;"
             data-base-latitude="<?php echo $x[0]; ?>"
             data-base-longitude="<?php echo $x[1]; ?>"
             data-zoom-level="<?php echo intval($ebd_settings['map']['zoom_level']); ?>"></div>
             <?php
         }

        function get_info_for_map() {

            global $directory_array;
            global $directory_category;
             
            ?>
            
            <div style="display: flex; flex-direction: row; flex-wrap: wrap; max-width: 100%; color: transparent;">
                <?php
                    if (is_array($directory_array)) {
                        foreach ($directory_array as $directory_arrayRow) {
                            if ($directory_arrayRow[0] != null) {
                                ?>
                                <div class="ebd-map-info2" id="<?php echo $directory_category; ?>" style="width: 33%; height: 150px ">
                                    <div class="ebd-info-content-wrap" style="border: 1px solid gray; height: 100%" >
                                        <?php echo $directory_arrayRow[0]; ?>                                        
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    }
                ?>
            </div>

            <div class="showmore"><div id="showmore-button">Plus de résultats</div></div>
        <?php
    }

}
//ebd_load_template_part('archive/list/content-list-template-1');
//include(EBD_PATH . 'inc/views/frontend/common/pagination.php');

/**
 * Pagination
 */
ebd_load_template_part('common/pagination', '', $data);
