<?php
$listing_template = (!empty($ebd_settings[ 'general' ][ 'grid_layout_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_layout_template' ]) : 'template-1';
$listing_template = apply_filters('ebd_grid_template', $listing_template);
$listing_template_class = 'ebd-grid-' . $listing_template;
$_GET[ 'layout' ] = $layout = 'grid';
$grid_column = (!empty($ebd_settings[ 'general' ][ 'grid_column' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_column' ]) : 2;
$grid_column_class = 'ebd-grid-column-' . $grid_column;
$image_original_size = (!empty($ebd_settings[ 'general' ][ 'image_original_size' ])) ? 'ebd-original-image-size' : '';
?>
<div class="ebd-directory-listing-wrap <?php echo $listing_template_class; ?> ebd-grid-layout <?php echo $grid_column_class; ?> <?php echo $image_original_size; ?>" <?php $this->display_none($current_view, 'grid'); ?>>
    <div class="ebd-main-grid-wrap">
        <?php include(EBD_PATH . 'inc/views/frontend/common/list-query.php'); ?>
    </div>
</div>
