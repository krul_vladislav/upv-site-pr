<?php
$exclude_tags = (!empty($atts[ 'exclude' ])) ? trim($atts[ 'exclude' ]) : '';
$tag_args = array( 'taxonomy' => 'ebd-tags', 'hide_empty' => true, 'orderby' => 'count', 'order' => 'desc' );

if ( !empty($exclude_tags) ) {
    $tag_args[ 'exclude' ] = $exclude_tags;
}
if ( isset($atts[ 'number' ]) && $atts[ 'number' ] != -1 ) {
    $tag_args[ 'number' ] = $atts[ 'number' ];
}
$directory_tags = get_terms($tag_args);
if ( !empty($directory_tags) ) {
    ?>
    <div class="ebd-directory-tags-list-wrap">
        <?php
        foreach ( $directory_tags as $directory_tag ) {
            ?>
            <div class="ebd-list-each-tag">
                <a href="<?php echo get_term_link($directory_tag->term_id); ?>">#<?php echo $directory_tag->name ?></a>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}

