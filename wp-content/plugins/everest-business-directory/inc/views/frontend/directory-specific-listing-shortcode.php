<div class="ebd-main-wrap">
    <?php
    $layout = (!empty($atts[ 'layout' ])) ? esc_attr($atts[ 'layout' ]) : 'list';
    $listing_template = (!empty($atts[ 'template' ])) ? esc_attr($atts[ 'template' ]) : 'template-1';
    $listing_template_class = 'ebd-' . $layout . '-' . $listing_template;
    if ( $layout == 'grid' ) {
        $grid_column = (!empty($ebd_settings[ 'general' ][ 'grid_column' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_column' ]) : 2;
        $grid_column_class = 'ebd-grid-column-' . $grid_column;
        $image_original_size = (!empty($ebd_settings[ 'general' ][ 'image_original_size' ])) ? 'ebd-original-image-size' : '';
    } else {
        $grid_column_class = '';
        $image_original_size = '';
    }
    ?>
    <div class="ebd-directory-listing-wrap <?php echo $listing_template_class; ?> <?php echo 'ebd-' . $layout . '-layout'; ?> <?php echo $grid_column_class; ?> <?php echo $image_original_size; ?>">
        <?php if ( $layout == 'grid' ) {
            ?>
            <div class="ebd-main-grid-wrap">
                <?php
            }

            wp_reset_query();
            wp_reset_postdata();
            $per_page = (!empty($atts[ 'number' ])) ? intval($atts[ 'number' ]) : 10;
            if ( !empty($atts[ 'per_page' ]) ) {
                $per_page = intval($atts[ 'per_page' ]);
            }
            $directory_listing_args = array( 'posts_per_page' => $per_page, 'post_status' => 'publish', 'post_type' => 'ebd' );
            if ( isset($atts[ 'directory_category' ]) ) {
                $directory_category = $atts[ 'directory_category' ];
                $tax_query = array( array( 'taxonomy' => 'ebd-categories', 'field' => 'slug', 'terms' => $directory_category ) );
                $directory_listing_args[ 'tax_query' ] = $tax_query;
            }
            if ( !empty($atts[ 'directory_tag' ]) ) {
                $directory_tag = $atts[ 'directory_tag' ];
                $tax_query = array( array( 'taxonomy' => 'ebd-tags', 'field' => 'slug', 'terms' => $directory_tag ) );
                $directory_listing_args[ 'tax_query' ] = $tax_query;
            }
            global $ebd_directory_expiry;
            if ( $ebd_directory_expiry ) {
                $directory_listing_args[ 'meta_query' ][] = array( 'key' => '_ebd_expiry_date', 'value' => date('Y-m-d'), 'compare' => '>=', 'type' => 'DATE' );
            }
            $directory_listing_query = new WP_Query($directory_listing_args);
            $temp_query = $directory_listing_query;

            include(EBD_PATH . 'inc/views/frontend/common/query-loop.php');
            if ( $layout == 'grid' ) {
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>