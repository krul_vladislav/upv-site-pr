<?php
$exclude_categories = (!empty($atts[ 'exclude' ])) ? trim($atts[ 'exclude' ]) : '';
$category_args = array( 'taxonomy' => 'ebd-categories', 'hide_empty' => true, 'parent' => 0 );

if ( !empty($exclude_categories) ) {
    $category_args[ 'exclude' ] = $exclude_categories;
}
if ( isset($atts[ 'number' ]) && $atts[ 'number' ] != -1 ) {
    $category_args[ 'number' ] = $atts[ 'number' ];
}
$directory_categories = get_terms($category_args);
if ( !empty($directory_categories) ) {
    ?>
    <div class="ebd-directory-categories-list-wrap">
        <?php
        foreach ( $directory_categories as $directory_category ) {
            ?>
            <div class="ebd-main-category-wrap">
                <div class="ebd-category-head"><!--<i class="fa fa-bus"></i>--><a href="<?php echo get_term_link($directory_category->term_id); ?>"><?php echo $directory_category->name ?></a></div>
                <?php
                $child_categories_args = array( 'taxonomy' => 'ebd-categories', 'hide_empty' => false, 'parent' => $directory_category->term_id );
                if ( !empty($exclude_categories) ) {
                    $child_categories_args[ 'exclude' ] = $exclude_categories;
                }
                $child_categories = get_terms($child_categories_args);
                if ( !empty($child_categories) ) {
                    foreach ( $child_categories as $child_category ) {
                        ?>
                        <div class="ebd-list-each-category">
                            <a href="<?php echo get_term_link($child_category->term_id); ?>"><?php echo $child_category->name ?></a> <span class="ebd-category-post-count"><?php echo $child_category->count; ?></span>
                        </div>
                        <?php
                    }
                }
                ?>

            </div>
            <?php
        }
        ?>
    </div>
    <?php
}

