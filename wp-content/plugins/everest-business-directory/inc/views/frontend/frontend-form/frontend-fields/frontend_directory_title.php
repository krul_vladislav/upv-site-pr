<div class="ebd-each-frontend-field ebd-title-sec">
    <label class="ebd-fst-title"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <input type="text" name="frontend_form[frontend_fields][directory_title]" data-field-key="directory_title" value="<?php echo (!empty($directory_title)) ? $directory_title : ''; ?>"/>
    </div>
</div>
