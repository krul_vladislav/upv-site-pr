<div class="ebd-each-frontend-field ebd-image-sec">
    <label class="ebd-title-field ebd-image-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <div class="ebd-directory-image ebd-file-uploader"
             data-max-upload-size="<?php echo (!empty($field_details[ 'size_limit' ])) ? intval($field_details[ 'size_limit' ]) * 1000000 : 8000000; ?>"
             data-upload-label="<?php echo (!empty($field_details[ 'uploader_label' ])) ? esc_attr($field_details[ 'uploader_label' ]) : __('Upload Image', 'everest-business-directory'); ?>"
             data-multiple-upload-error-message="<?php echo $file_upload_limit_message; ?>"
             data-extension-error-message="<?php echo $file_extension_error_message; ?>"
             id="ebd-uploader-<?php echo $uploader_id = rand(1111, 9999); ?>">
        </div>
        <input type="hidden" name="frontend_form[frontend_fields][directory_image]" class="ebd-uploaded-files" data-field-key="directory_image" value="<?php echo (!empty($directory_thumbnail_id)) ? $directory_thumbnail_id : ''; ?>"/>
    </div>
    <div class="ebd-file-preview">
        <?php
        if ( !empty($directory_thumbnail_id) ) {
            $thumbnail = wp_get_attachment_image_src($directory_thumbnail_id, 'thumbnail');
            $attachment_date = get_the_date("U", $directory_thumbnail_id);
            $attachment_code = md5($attachment_date);
            $attachment_title = get_the_title($directory_thumbnail_id)
            //var_dump($thumbnail);
            ?>
            <div class="ebd-pro-prev-holder">
                <span class="ebd-prev-name"><?php echo $attachment_title; ?></span>
                <img src="<?php echo $thumbnail[ 0 ]; ?>">
                <span class="ebd-pro-preview-remove" data-url="<?php echo $thumbnail[ 0 ]; ?>" data-id="<?php echo $uploader_id; ?>" data-attachment-id="<?php echo $directory_thumbnail_id; ?>" data-attachment-code="<?php echo $attachment_code; ?>">
                    <span class="lnr lnr-cross"></span>
                </span>
            </div>
            <?php
        }
        ?>
    </div>
</div>