<div class="ebd-each-frontend-field ebd-datepicker-title-wrap">
    <label class="ebd-fst-title"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field ebd-datepicker-wrap">
        <input type="text" name="frontend_form[frontend_fields][directory_expiry_date]" data-field-key="directory_expiry_date" value="<?php echo (!empty($directory_expiry_date)) ? $directory_expiry_date : ''; ?>" class="ebd-datepicker" data-format="yy-mm-dd"/>
    </div>
</div>
