<div class="ebd-each-frontend-field ebd-gallery-sec">
    <label class="ebd-title-field ebd-gallery-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <div class="ebd-directory-gallery ebd-file-uploader" data-upload-limit="<?php echo (!empty($field_details[ 'upload_limit' ])) ? intval($field_details[ 'upload_limit' ]) : -1; ?>"
             data-max-upload-size="<?php echo (!empty($field_details[ 'size_limit' ])) ? intval($field_details[ 'size_limit' ]) * 1000000 : 8000000; ?>"
             data-upload-label="<?php echo (!empty($field_details[ 'uploader_label' ])) ? esc_attr($field_details[ 'uploader_label' ]) : __('Upload Image', 'everest-business-directory'); ?>"
             data-multiple-upload-limit="<?php echo (!empty($field_details[ 'upload_limit' ])) ? esc_attr($field_details[ 'upload_limit' ]) : -1; ?>"
             data-multiple-upload-error-message="<?php echo $file_upload_limit_message; ?>"
             id="ebd-uploader-<?php echo rand(1111, 9999); ?>">
        </div>
        <input type="hidden" name="frontend_form[frontend_fields][directory_gallery]" class="ebd-uploaded-files" data-field-key="directory_gallery" value="<?php echo (!empty($gallery_ids)) ? esc_attr($gallery_ids) : ''; ?>"/>
        <input type="hidden" class="ebd-multiple-upload-limit"/>

        <div class="ebd-file-preview">
            <?php
            if ( !empty($gallery) ) {
                foreach ( $gallery as $gallery_id ) {
                    $thumbnail = wp_get_attachment_image_src($gallery_id, 'thumbnail');
                    if ( $thumbnail ) {
                        $attachment_date = get_the_date("U", $gallery_id);
                        $attachment_code = md5($attachment_date);
                        $attachment_title = get_the_title($gallery_id);
                        ?>
                        <div class="ebd-pro-prev-holder">
                            <span class="ebd-prev-name"><?php echo $attachment_title; ?></span>
                            <img src="<?php echo $thumbnail[ 0 ]; ?>">
                            <span class="ebd-pro-preview-remove" data-url="<?php echo $thumbnail[ 0 ]; ?>" data-id="<?php echo $uploader_id; ?>" data-attachment-id="<?php echo $directory_thumbnail_id; ?>" data-attachment-code="<?php echo $attachment_code; ?>">
                                <span class="lnr lnr-cross"></span>
                            </span>
                        </div>
                        <?php
                    }
                }
            }
            ?>

        </div>
        <div class="ebd-error"></div>
    </div>
</div>