<div class="ebd-each-frontend-field ebd-contact-sec">
    <label class="ebd-title-field ebd-contact-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">

        <?php
        $sub_fields_array = array( 'email_address' => array( 'label' => __('Email Address', 'everest-business-directory') ),
            'website' => array( 'label' => __('Website', 'everest-business-directory') ),
            'phone_number' => array( 'label' => __('Phone Number', 'everest-business-directory') )
        );
        foreach ( $sub_fields_array as $sub_field_key => $sub_field_details ) {
            if ( !empty($field_details[ 'fields' ][ $sub_field_key ][ 'show' ]) ) {
                $field_label = (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ])) ? esc_attr($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ]) : '';
                ?>
                <div class="ebd-sub-field ebd-<?php echo $sub_field_key; ?>">
                    <label><?php echo $field_label; ?></label>
                    <div class="ebd-field">
                        <input type="text" name="frontend_form[frontend_fields][contact_information][<?php echo $sub_field_key; ?>]" data-field-key="<?php echo $sub_field_key; ?>" value="<?php echo (!empty($$sub_field_key)) ? $$sub_field_key : ''; ?>"/>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
