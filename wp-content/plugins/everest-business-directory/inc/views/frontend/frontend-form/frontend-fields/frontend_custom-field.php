<?php
$custom_field_key = str_replace('_custom_field_', '', $field_key);
if ( isset($custom_fields[ $custom_field_key ]) ) {
    $field_type = isset($custom_fields[ $custom_field_key ][ 'field_type' ]) ? esc_attr($custom_fields[ $custom_field_key ][ 'field_type' ]) : 'textfield';
    $field_label = esc_attr($ebd_settings[ 'custom_fields' ][ $custom_field_key ][ 'field_label' ]);
    $custom_field_value = (!empty($directory_id)) ? get_post_meta($directory_id, $custom_field_key, true) : '';
    //  var_dump($custom_field_value);
    switch ( $field_type ) {
        case 'textfield':
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld ebd-text-only-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field">
                    <input type="text" name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>" value="<?php echo $custom_field_value; ?>"/>
                </div>
            </div>
            <?php
            break;
        case 'textarea':
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field">
                    <textarea name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>"><?php echo $custom_field_value; ?></textarea>
                </div>
            </div>
            <?php
            break;
        case 'radio':
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field">
                    <div class="ebd-options-wrap"  data-field-key="<?php echo $field_key; ?>">
                        <?php
                        if ( !empty($custom_fields[ $custom_field_key ][ 'option' ]) ) {
                            $option_count = 0;
                            foreach ( $custom_fields[ $custom_field_key ][ 'option' ] as $option ) {
                                ?>
                                <label class="ebd-opt-main-sc"><input type="radio" name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>]" value="<?php echo $custom_fields[ $custom_field_key ][ 'value' ][ $option_count ]; ?>" id="ebd-option-<?php echo $field_key . '-' . $option_count; ?>" <?php checked($custom_field_value, $custom_fields[ $custom_field_key ][ 'value' ][ $option_count ]); ?>/><label for="ebd-option-<?php echo $field_key . '-' . $option_count; ?>"><?php echo $option; ?></label></label>
                                <?php
                                $option_count++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
            break;
        case 'select':
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field">
                    <select name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>">
                        <?php
                        if ( !empty($custom_fields[ $custom_field_key ][ 'option' ]) ) {
                            $option_count = 0;
                            foreach ( $custom_fields[ $custom_field_key ][ 'option' ] as $option ) {
                                ?>
                                <option value="<?php echo $custom_fields[ $custom_field_key ][ 'value' ][ $option_count ]; ?>" <?php selected($custom_field_value, $custom_fields[ $custom_field_key ][ 'value' ][ $option_count ]); ?>><?php echo $option; ?></option>
                                <?php
                                $option_count++;
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <?php
            break;
        case 'checkbox':
            $custom_field_value = (empty($custom_field_value)) ? array() : $custom_field_value;
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field">
                    <div class="ebd-options-wrap"  data-field-key="<?php echo $field_key; ?>">
                        <?php
                        if ( !empty($custom_fields[ $custom_field_key ][ 'option' ]) ) {
                            $option_count = 0;
                            foreach ( $custom_fields[ $custom_field_key ][ 'option' ] as $option ) {
                                ?>
                                <label class="ebd-opt-main-sc"><input type="checkbox" name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>][]" value="<?php echo $custom_fields[ $custom_field_key ][ 'value' ][ $option_count ]; ?>" id="ebd-option-<?php echo $field_key . '-' . $option_count; ?>" <?php echo in_array($custom_fields[ $custom_field_key ][ 'value' ][ $option_count ], $custom_field_value) ? 'checked="checked"' : ''; ?>/><label for="ebd-option-<?php echo $field_key . '-' . $option_count; ?>"><?php echo $option; ?></label></label>
                                <?php
                                $option_count++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
            break;
        case 'datepicker':
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field ebd-datepicker-wrap">
                    <input type="text" name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>]" class="ebd-datepicker" data-field-key="<?php echo $field_key ?>" value="<?php echo $custom_field_value; ?>"/>
                </div>
            </div>
            <?php
            break;
        case 'url':
            ?>
            <div class="ebd-each-frontend-field ebd-extra-custom-fld">
                <label><?php echo $field_label; ?></label>
                <div class="ebd-field ebd-datepicker-wrap">
                    <input type="url" name="frontend_form[frontend_fields][custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key ?>" value="<?php echo $custom_field_value; ?>"/>
                </div>
            </div>
            <?php
            break;
    }
}