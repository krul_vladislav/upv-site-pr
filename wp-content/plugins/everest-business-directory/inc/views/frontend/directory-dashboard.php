<?php
global $ebd_directory_display;
global $ebd_directory_expiry;
?>
<div class="ebd-dashboard-wrap">
    <div class="ebd-dashboard-head">
        <div class="ebd-dashboard-row">
            <div class="ebd-dashboard-col ebd-dashboard-sn"><?php _e('SN', 'everest-business-directory'); ?></div>
            <div class="ebd-dashboard-col ebd-dashboard-ttl"><?php _e('Title', 'everest-business-directory'); ?></div>
            <?php if ( $ebd_directory_expiry ) { ?>
                <div class="ebd-dashboard-col ebd-dashboard-date"><?php _e('Expiry Date', 'everest-business-directory'); ?></div>
            <?php } ?>
            <div class="ebd-dashboard-col ebd-dashboard-status"><?php _e('Status', 'everest-business-directory'); ?></div>
            <div class="ebd-dashboard-col ebd-dashboard-action"><?php _e('Action', 'everest-business-directory'); ?></div>
        </div>
    </div>
    <div class="ebd-dashboard-body">
        <?php
        $current_user = wp_get_current_user();
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        // var_dump($current_user);
        $author_directory_args = array( 'post_type' => 'ebd', 'posts_per_page' => 20, 'author' => $current_user->ID, 'paged' => $paged, 'post_status' => array( 'publish', 'pending', 'draft' ) );
        $author_directory_query = new WP_Query($author_directory_args);

        if ( $author_directory_query->have_posts() ) {
            $directory_counter = 1;
            while ( $author_directory_query->have_posts() ) {
                $author_directory_query->the_post();
                $directory_expiry_date = get_post_meta(get_the_ID(), '_ebd_expiry_date', true);
                $today = date('Y-m-d');

                if ( strtotime($today) > strtotime($directory_expiry_date) && $ebd_directory_expiry ) {
                    $directory_status = __('Expired', 'everest-business-directory');
                } else {
                    $directory_status = ucwords(get_post_status());
                }
                ?>
                <div class="ebd-dashboard-row">
                    <div class="ebd-dashboard-col ebd-dashboard-sn"><?php echo $directory_counter++; ?></div>
                    <div class="ebd-dashboard-col ebd-dashboard-ttl"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                    <?php if ( $ebd_directory_expiry ) { ?>
                        <div class="ebd-dashboard-col ebd-dashboard-date"><?php echo $directory_expiry_date; ?></div>
                    <?php } ?>
                    <div class="ebd-dashboard-col ebd-dashboard-status"><?php echo $directory_status; ?></div>
                    <div class="ebd-dashboard-col ebd-dashboard-action">
                        <a href="<?php echo $ebd_directory_display->get_directory_edit_url(); ?>" class="ebd-edit"><span class="lnr lnr-cog"></span></a>
                        <a href="javascript:void(0);" class="ebd-delete ebd-directory-delete-trigger" data-directory-id="<?php echo get_the_ID(); ?>" data-nonce="<?php echo wp_create_nonce('ebd_directory_delete_nonce'); ?>" data-security-key="<?php echo md5(get_the_date()); ?>"><span class="lnr lnr-trash"></span></a>
                        <a href="<?php the_permalink(); ?>" class="ebd-view"><span class="lnr lnr-eye"></span></a>
                        <img src="<?php echo EBD_URL . 'images/ajax-loader.gif' ?>" class="ebd-ajax-loader" style="display:none;"/>
                    </div>

                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php if ( $author_directory_query->max_num_pages > 1 ) { ?>
        <div class="ebd-pagination-wrap">
            <?php
            $big = 999999999; // need an unlikely integer
            $translated = __('Page', 'everest-business-directory'); // Supply translatable string
            $page_num_link = explode('?', esc_url(get_pagenum_link($big)));
            if ( !empty($_GET) ) {
                $page_num_link = $page_num_link[ 0 ] . '?' . http_build_query($_GET);
            } else {
                $page_num_link = $page_num_link[ 0 ];
            }

            echo paginate_links(array(
                'base' => str_replace($big, '%#%', $page_num_link),
                'format' => '?%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $author_directory_query->max_num_pages,
                'before_page_number' => '<span class="screen-reader-text">' . $translated . ' </span>',
                'prev_text' => __('Previous', 'everest-business-directory'),
                'next_text' => __('Next', 'everest-business-directory'),
            ));
            ?>
        </div>
    <?php } ?>
</div>