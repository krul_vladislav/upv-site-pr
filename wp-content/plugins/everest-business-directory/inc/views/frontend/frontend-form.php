<?php
global $ebd_settings;
$frontend_form_settings = $ebd_settings['frontend_form'];
$form_general_settings = $frontend_form_settings['general'];
$form_layout = (!empty( $frontend_form_settings['form_layout']['template'] )) ? esc_attr( $frontend_form_settings['form_layout']['template'] ) : 'template-1';
$form_layout = apply_filters( 'ebd_form_template', $form_layout );
//var_dump($form_layout);
$custom_fields = (!empty( $ebd_settings['custom_fields'] )) ? $ebd_settings['custom_fields'] : array();
$file_extension_error_message = (empty( $form_general_settings['file_extension_error_message'] )) ? esc_attr( $form_general_settings['file_extension_error_message'] ) : '';
$file_upload_limit_message = (empty( $form_general_settings['file_upload_limit_message'] )) ? esc_attr( $form_general_settings['file_upload_limit_message'] ) : '';
$file_size_limit_message = (empty( $form_general_settings['file_size_limit_message'] )) ? esc_attr( $form_general_settings['file_size_limit_message'] ) : '';
$this->print_array( $frontend_form_settings );
?>
<div class="ebd-form-wrap ebd-frontend-form-<?php echo $form_layout; ?>">
    <form class="ebd-frontend-form">
        <?php
        foreach ( $frontend_form_settings['frontend_fields'] as $field_key => $field_details ) {
            if ( strpos( $field_key, '_custom_field_' ) !== false ) {
                $frontend_field_file = 'frontend_custom-field.php';
            } else {

                $frontend_field_file = 'frontend_' . $field_key . '.php';
            }

            if ( file_exists( EBD_PATH . 'inc/views/frontend/frontend-form/frontend-fields/' . $frontend_field_file ) ) {
                if ( !empty( $field_details['show'] ) ) {

                    include(EBD_PATH . 'inc/views/frontend/frontend-form/frontend-fields/' . $frontend_field_file);
                }
            } else {
                echo EBD_PATH . 'inc/views/backend/frontend-form/frontend-fields/' . $frontend_field_file;
                //echo $frontend_field_file;
                echo "<hr/>";
            }
        }

        /**
         * Captcha
         */
        if ( !empty( $ebd_settings['captcha']['frontend_captcha'] ) ) {
            $site_key = (!empty( $ebd_settings['captcha']['site_key'] )) ? esc_attr( $ebd_settings['captcha']['site_key'] ) : '';
            if ( !empty( $site_key ) ) {
                ?>
                <div class="ebd-each-frontend-field ebd-captcha">
                    <label><?php echo (!empty( $ebd_settings['captcha']['captcha_label'] )) ? esc_attr( $ebd_settings['captcha']['captcha_label'] ) : ''; ?></label>
                    <div class="ebd-field">
                        <div data-field-key="captcha">
                            <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>

        <div class="ebd-each-frontend-field ebd-submit-field">
            <div class="ebd-field">
                <input type="submit" name="ebd_frontend_submit" value="<?php echo (!empty( $form_general_settings['submit_button_label'] )) ? esc_attr( $form_general_settings['submit_button_label'] ) : __( 'Submit', 'everest-business-directory' ); ?>"/>
                <input type="hidden" name="ebd_directory_id" value="<?php echo (!empty( $directory_id )) ? $directory_id : ''; ?>"/>
                <img src="<?php echo EBD_IMG_URL . 'ajax-loader.gif' ?>" class="ebd-ajax-loader" style="display:none; "/>
            </div>
        </div>
        <div class="ebd-form-message" style="display:none;"></div>
    </form>
</div>