<?php
defined('ABSPATH') or die('No script kiddies please!!');
$active_tab = isset($_GET[ 'tab' ]) ? sanitize_text_field($_GET[ 'tab' ]) : 'general';
?>
<div class="wrap ebd-settings-wrap">
    <div class="ebd-head ebd-clearfix">
        <img src="<?php echo EBD_IMG_URL . 'logo.png' ?>"/>
        <h3><?php _e('Settings', 'everest-business-directory'); ?></h3>
    </div>
    <?php
    if ( isset($_GET[ 'message' ]) && $_GET[ 'message' ] == 1 ) {
        ?>
        <div id="message" class="updated notice notice-success is-dismissible">
            <p><?php _e('Settings saved successfully.', 'everest-business-directory'); ?></p>
        </div>
        <?php
    }
    ?>
    <div class="notice notice-info is-dismissible">
        <p><?php _e(sprintf('If the directory detail and archive layout doesn\'t match your active theme\'s layout then please check the %s Customize %s section of our plugin to adjust the markup as per your theme. You can check more detail on this on our how to use section\'s %s Template Customizations %s.', '<a href="' . admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=customize') . '" target="_blank">', '</a>', '<a href="' . admin_url('edit.php?post_type=ebd&page=ebd-how-to-use#ebd-template-customizations') . '" target="_blank">', '</a>'), 'everest-business-directory'); ?></p>
    </div>
    <h2 class="nav-tab-wrapper wp-clearfix">
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=general'); ?>" class="nav-tab <?php echo ($active_tab == 'general') ? 'nav-tab-active' : ''; ?>"><?php _e('General Settings', 'everest-business-directory'); ?></a>
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=map'); ?>" class="nav-tab <?php echo ($active_tab == 'map') ? 'nav-tab-active' : ''; ?>"><?php _e('Map Settings', 'everest-business-directory'); ?></a>
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=custom_fields'); ?>" class="nav-tab  <?php echo ($active_tab == 'custom_fields') ? 'nav-tab-active' : ''; ?>"><?php _e('Custom Fields', 'everest-business-directory'); ?></a>
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=frontend_form'); ?>" class="nav-tab  <?php echo ($active_tab == 'frontend_form') ? 'nav-tab-active' : ''; ?>"><?php _e('Frontend Form', 'everest-business-directory'); ?></a>
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=directory_detail'); ?>" class="nav-tab  <?php echo ($active_tab == 'directory_detail') ? 'nav-tab-active' : ''; ?>"><?php _e('Directory Detail', 'everest-business-directory'); ?></a>
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=captcha'); ?>" class="nav-tab  <?php echo ($active_tab == 'captcha') ? 'nav-tab-active' : ''; ?>"><?php _e('Captcha Settings', 'everest-business-directory'); ?></a>
        <a href="<?php echo admin_url('edit.php?post_type=ebd&page=ebd-settings&tab=customize'); ?>" class="nav-tab  <?php echo ($active_tab == 'customize') ? 'nav-tab-active' : ''; ?>"><?php _e('Customize', 'everest-business-directory'); ?></a>
    </h2>

    <div class="ebd-settings-options-wrap">
        <?php
        switch ( $active_tab ) {
            case 'general':
                include(EBD_PATH . 'inc/views/backend/settings/general.php');
                break;
            case 'map':
                include(EBD_PATH . 'inc/views/backend/settings/map.php');
                break;
            case 'custom_fields':
                include(EBD_PATH . 'inc/views/backend/settings/custom-fields.php');
                break;
            case 'frontend_form':
                include(EBD_PATH . 'inc/views/backend/settings/frontend-form.php');
                break;
            case 'directory_detail':
                include(EBD_PATH . 'inc/views/backend/settings/directory-detail.php');
                break;
            case 'captcha':
                include(EBD_PATH . 'inc/views/backend/settings/captcha.php');
                break;
            case 'customize':
                include(EBD_PATH . 'inc/views/backend/settings/customize.php');
                break;
            default:
                break;
        }
        $this->print_array($ebd_settings);
        ?>
    </div>
</div>

