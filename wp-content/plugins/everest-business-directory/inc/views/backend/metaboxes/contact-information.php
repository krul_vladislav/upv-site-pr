<div class="ebd-field-wrap">
    <label><?php _e('Email Address', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[contact][email_address]" value="<?php echo esc_attr(get_post_meta($post->ID, '_ebd_email_address', true)); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Website', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[contact][website]" value="<?php echo esc_attr(get_post_meta($post->ID, '_ebd_website', true)); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Phone Number', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[contact][phone_number]" value="<?php echo esc_attr(get_post_meta($post->ID, '_ebd_phone_number', true)); ?>"/>
    </div>
</div>

