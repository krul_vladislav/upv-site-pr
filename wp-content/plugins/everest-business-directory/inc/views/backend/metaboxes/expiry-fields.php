<div class="ebd-field-wrap">
    <div class="ebd-field">
        <input type="text" name="directory_fields[expiry_date]" value="<?php echo esc_attr(get_post_meta($post->ID, '_ebd_expiry_date', true)); ?>" class="ebd-datepicker"/>
    </div>
</div>