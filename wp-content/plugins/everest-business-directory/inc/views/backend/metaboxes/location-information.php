<?php
global $ebd_settings;
if ( !empty( $ebd_settings['map']['map_api_key'] ) ) {
    include(EBD_PATH . 'inc/views/backend/map.php');
} else {
    ?>
    <p class="description"><?php _e( 'Please enter the google map API key in the map settings to enable the geocoding map', 'everest-business-directory' ); ?></p>
    <?php
}
?>
<div class = "ebd-field-wrap">
    <label><?php _e( 'Latitude', 'everest-business-directory' ); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[location][latitude]" id="ebd-latitude" value="<?php echo esc_attr( get_post_meta( $post->ID, '_ebd_latitude', true ) ); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e( 'Longitude', 'everest-business-directory' ); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[location][longitude]" id="ebd-longitude" value="<?php echo esc_attr( get_post_meta( $post->ID, '_ebd_longitude', true ) ); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e( 'Address', 'everest-business-directory' ); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[location][address]" id="ebd-address" value="<?php echo esc_attr( get_post_meta( $post->ID, '_ebd_address', true ) ); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e( 'City', 'everest-business-directory' ); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[location][city]" id="ebd-city" value="<?php echo esc_attr( get_post_meta( $post->ID, '_ebd_city', true ) ); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e( 'Country', 'everest-business-directory' ); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[location][country]" id="ebd-country" value="<?php echo esc_attr( get_post_meta( $post->ID, '_ebd_country', true ) ); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e( 'Postal Code', 'everest-business-directory' ); ?></label>
    <div class="ebd-field">
        <input type="text" name="directory_fields[location][postal_code]" id="ebd-postal-code" value="<?php echo esc_attr( get_post_meta( $post->ID, '_ebd_postal_code', true ) ); ?>"/>
    </div>
</div>

<p class="description"><?php _e( 'Note: You can also manually update the values if autofilled values don\'t match the exact values.', 'everest-business-directory', true ); ?></p>