<div class="ebd-field-wrap">
    <label><?php _e('Facebook URL', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <?php $facebook = get_post_meta($post->ID, '_ebd_facebook', true); ?>
        <input type="text" name="directory_fields[social][facebook]" value="<?php echo esc_url($facebook); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Twitter URL', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <?php $twitter = get_post_meta($post->ID, '_ebd_twitter', true); ?>
        <input type="text" name="directory_fields[social][twitter]" value="<?php echo esc_url($twitter); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Google+ URL', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <?php $googleplus = get_post_meta($post->ID, '_ebd_googleplus', true); ?>
        <input type="text" name="directory_fields[social][googleplus]" value="<?php echo esc_url($googleplus); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Instagram URL', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <?php $instagram = get_post_meta($post->ID, '_ebd_instagram', true); ?>
        <input type="text" name="directory_fields[social][instagram]" value="<?php echo esc_url($instagram); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Youtube URL', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <?php $youtube = get_post_meta($post->ID, '_ebd_youtube', true); ?>
        <input type="text" name="directory_fields[social][youtube]" value="<?php echo esc_url($youtube); ?>"/>
    </div>
</div>
<div class="ebd-field-wrap">
    <label><?php _e('Linkedin URL', 'everest-business-directory'); ?></label>
    <div class="ebd-field">
        <?php $linkedin = get_post_meta($post->ID, '_ebd_linkedin', true); ?>
        <input type="text" name="directory_fields[social][linkedin]" value="<?php echo esc_url($linkedin); ?>"/>
    </div>
</div>