<div class="ebd-field-wrap">
    <div class="ebd-field">
        <input type="checkbox" name="directory_fields[featured_directory]" value="1" <?php checked(get_post_meta($post->ID, '_ebd_featured', true), true); ?>/>

    </div>
</div>
<p class="description"><?php _e('Please check if you want to list this directory in the featured directory listing.', 'everest-business-directory'); ?></p>