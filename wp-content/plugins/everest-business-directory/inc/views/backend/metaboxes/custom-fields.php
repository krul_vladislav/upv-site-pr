<?php
global $ebd_settings;
if ( !empty($ebd_settings[ 'custom_fields' ]) ) {
    foreach ( $ebd_settings[ 'custom_fields' ] as $field_key => $field_details ) {
        $field_value = get_post_meta($post->ID, $field_key, true);
        switch ( $field_details[ 'field_type' ] ) {
            case 'textfield':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <input type="text" name="directory_fields[custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>" value="<?php echo $field_value; ?>"/>
                    </div>
                </div>
                <?php
                break;
            case 'textarea':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <textarea name="directory_fields[custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>"><?php echo esc_attr(get_post_meta($post->ID, $field_key, true)); ?></textarea>
                    </div>
                </div>
                <?php
                break;
            case 'radio':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <div class="ebd-options-wrap"  data-field-key="<?php echo $field_key; ?>">
                            <?php
                            if ( !empty($field_details[ 'option' ]) ) {
                                $option_count = 0;
                                foreach ( $field_details[ 'option' ] as $option ) {
                                    ?>
                                    <label><input type="radio" name="directory_fields[custom_fields][<?php echo $field_key ?>]" value="<?php echo $field_details[ 'value' ][ $option_count ]; ?>" <?php checked($field_value, $field_details[ 'value' ][ $option_count ]); ?>/><?php echo $option; ?></label>
                                    <?php
                                    $option_count++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                break;
            case 'select':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <select name="directory_fields[custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>">
                            <?php
                            if ( !empty($field_details[ 'option' ]) ) {
                                $option_count = 0;
                                foreach ( $field_details[ 'option' ] as $option ) {
                                    ?>
                                    <option value="<?php echo $field_details[ 'value' ][ $option_count ]; ?>" <?php selected($field_value, $field_details[ 'value' ][ $option_count ]); ?>><?php echo $option; ?></option>
                                    <?php
                                    $option_count++;
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <?php
                break;
            case 'checkbox':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <div class="ebd-options-wrap"  data-field-key="<?php echo $field_key; ?>">
                            <?php
                            $field_value = (empty($field_value)) ? array() : $field_value;
                            if ( !empty($field_details[ 'option' ]) ) {
                                $option_count = 0;
                                foreach ( $field_details[ 'option' ] as $option ) {
                                    ?>
                                    <label><input type="checkbox" name="directory_fields[custom_fields][<?php echo $field_key ?>][]" value="<?php echo $field_details[ 'value' ][ $option_count ]; ?>" <?php echo in_array($field_details[ 'value' ][ $option_count ], $field_value) ? 'checked="checked"' : ''; ?>/><?php echo $option; ?></label>
                                    <?php
                                    $option_count++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                break;
            case 'datepicker':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <input type="text" name="directory_fields[custom_fields][<?php echo $field_key ?>]" class="ebd-datepicker" data-field-key="<?php echo $field_key ?>" value="<?php echo $field_value; ?>"/>
                    </div>
                </div>
                <?php
                break;
            case 'url':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <input type="url" name="directory_fields[custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>" value="<?php echo $field_value; ?>"/>
                    </div>
                </div>
                <?php
                break;
            case 'html':
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
                    <div class="ebd-field">
                        <textarea name="directory_fields[custom_fields][<?php echo $field_key ?>]" data-field-key="<?php echo $field_key; ?>"><?php echo $field_value; ?></textarea>
                    </div>
                </div>
                <?php
                break;
        }
    }
}
