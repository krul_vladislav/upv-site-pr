<input type="button" class="button-secondary ebd-gallery-add-trigger" value="<?php _e('Add Gallery', 'everest-business-directory'); ?>"/>
<img src="<?php echo EBD_IMG_URL . 'ajax-loader.gif' ?>" style="display:none;" class="ebd-ajax-loader"/>
<div class="ebd-gallery-items-wrap">
    <?php
    $gallery = get_post_meta($post->ID, '_ebd_gallery', true);
    if ( !empty($gallery) ) {
        foreach ( $gallery as $gallery_item ) {
            $attachment_src = wp_get_attachment_image_src($gallery_item);
            if ( $attachment_src ) {
                ?>
                <div class="ebd-each-gallery">
                    <img src="<?php echo esc_url($attachment_src[ 0 ]); ?>"/>
                    <a href="javascript:void(0);" class="ebd-remove-gallery-item"><span class="dashicons dashicons-no-alt"></span></a>
                    <input type="hidden" name="directory_fields[gallery][]" value="<?php echo intval($gallery_item); ?>"/>
                </div>
                <?php
            }
        }
    }
    ?>
</div>
<?php wp_nonce_field('ebd-metabox-nonce', 'ebd_metabox_nonce'); ?>
