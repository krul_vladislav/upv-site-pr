<form class="ebd-settings-group-form" method="post" action="<?php echo admin_url('admin-post.php'); ?>">
    <input type="hidden" name="action" value="ebd_settings_action"/>
    <input type="hidden" name="tab" value="map"/>
    <?php wp_nonce_field('ebd_settings_nonce', 'ebd_settings_nonce'); ?>
    <div class="ebd-field-wrap">
        <label><?php _e('Google Map API Key', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][map_api_key]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'map_api_key' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_api_key' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please go <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">here</a> to get the google map API key.', 'everest-business-directory'); ?></p>
            <p class="description"><?php _e(sprintf('You can also check step by step tutorial %s here %s to get the API key.', '<a href="https://elfsight.com/blog/2018/06/how-to-get-google-maps-api-key-guide/" target="_blank">', '</a>'), 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Base Address', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][base_address]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'base_address' ])) ? esc_attr($ebd_settings[ 'map' ][ 'base_address' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please enter the valid address that is available in the google map. This will be used for extracting address from map.', 'everest-business-directory'); ?></p>
        </div>
    </div>

    <div class="ebd-field-wrap">
        <label><?php _e('Base Latitude', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][base_latitude]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'base_latitude' ])) ? esc_attr($ebd_settings[ 'map' ][ 'base_latitude' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please find latitude from <a href="https://www.latlong.net/" target="_blank">here</a>. This will be used for map view.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Base Longitude', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][base_longitude]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'base_longitude' ])) ? esc_attr($ebd_settings[ 'map' ][ 'base_longitude' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please find longitude from <a href="https://www.latlong.net/" target="_blank">here</a>. This will be used for map view.', 'everest-business-directory'); ?></p>
        </div>
    </div>

    <div class="ebd-field-wrap">
        <label><?php _e('Zoom Level', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][zoom_level]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'zoom_level' ])) ? esc_attr($ebd_settings[ 'map' ][ 'zoom_level' ]) : ''; ?>"/>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Map Width', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][map_width]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'map_width' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_width' ]) : ''; ?>" placeholder="100%"/>
            <p class="description"><?php _e('Please enter the width of the map in px or %.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Map Height', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[map][map_height]" value="<?php echo (!empty($ebd_settings[ 'map' ][ 'map_height' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_height' ]) : ''; ?>" placeholder="400px"/>
            <p class="description"><?php _e('Please enter the height of the map in px or %.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Choose Default Marker', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $checked_marker = (!empty($ebd_settings[ 'map' ][ 'map_normal_marker' ])) ? intval($ebd_settings[ 'map' ][ 'map_normal_marker' ]) : 1;
            for ( $i = 1; $i <= 10; $i++ ) {
                ?>
                <label class="ebd-image-option <?php echo ($checked_marker == $i) ? 'ebd-selected-option' : ''; ?>">
                    <input type="radio" value="<?php echo $i ?>" name="ebd_settings[map][map_normal_marker]" <?php checked($checked_marker, $i); ?>/>
                    <img src="<?php echo EBD_URL . 'images/markers/normal/map-marker-' . $i . '.png'; ?>"/>
                </label>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Choose Featured Marker', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $checked_marker = (!empty($ebd_settings[ 'map' ][ 'map_featured_marker' ])) ? intval($ebd_settings[ 'map' ][ 'map_featured_marker' ]) : 1;
            for ( $i = 1; $i <= 10; $i++ ) {
                ?>
                <label class="ebd-image-option <?php echo ($checked_marker == $i) ? 'ebd-selected-option' : ''; ?>">
                    <input type="radio" value="<?php echo $i ?>" name="ebd_settings[map][map_featured_marker]" <?php checked($checked_marker, $i); ?>/>
                    <img src="<?php echo EBD_URL . 'images/markers/featured/map-marker-featured-' . $i . '.png'; ?>"/>
                </label>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label></label>
        <div class='ebd-field'>
            <input type="submit" class="button-primary" value="<?php _e('Save Settings', 'everest-business-directory'); ?>"/>
        </div>
    </div>
</form>