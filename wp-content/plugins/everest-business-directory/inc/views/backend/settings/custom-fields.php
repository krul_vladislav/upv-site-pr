<?php defined('ABSPATH') or die('No script kiddies please!!'); ?>
<div class="ebd-custom-field-outer-wrap ebd-clearfix">
    <div class="ebd-custom-fields-adder">
        <div class="ebd-field-wrap">
            <label><?php _e('Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" id="ebd-custom-field-label"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Meta Key', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" id="ebd-custom-field-key"/>
                <p class="description">
                    <?php _e('Note: Please use lower case letters only for meta key and please don\'t use any symbols or space keys.Use underscore( _ ) instead. For example: If you are to add Product Model then meta key can be product_model .', 'everest-business-directory'); ?>
                </p>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Field Type', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <select id="ebd-custom-field-type">
                    <option value="textfield"><?php _e('Textfield', 'everest-business-directory'); ?></option>
                    <option value="textarea"><?php _e('Textarea', 'everest-business-directory'); ?></option>
                    <option value="radio"><?php _e('Radio Button', 'everest-business-directory'); ?></option>
                    <option value="select"><?php _e('Select Option', 'everest-business-directory'); ?></option>
                    <option value="checkbox"><?php _e('Checkbox', 'everest-business-directory'); ?></option>
                    <option value="datepicker"><?php _e('Datepicker', 'everest-business-directory'); ?></option>
                    <option value="url"><?php _e('URL', 'everest-business-directory'); ?></option>
                    <option value="html"><?php _e('HTML', 'everest-business-directory'); ?></option>
                </select>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label></label>
            <div class="ebd-field">
                <input type="button" class="ebd-custom-field-add-trigger button-secondary" value="<?php _e('Add Field', 'everest-business-directory'); ?>"/>
                <p class="description ebd-error" style="display:none"><?php _e('Please enter field label and meta key.', 'everest-business-directory'); ?></p>
            </div>
        </div>
    </div>
    <form class="ebd-settings-group-form" method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <div class="ebd-custom-fields-list ebd-sortable">
            <?php
            $custom_fields = !empty($ebd_settings[ 'custom_fields' ]) ? $ebd_settings[ 'custom_fields' ] : array();
            if ( !empty($custom_fields) ) {
                foreach ( $custom_fields as $custom_field_key => $custom_field_details ) {
                    ?>
                    <div class="ebd-each-custom-field">
                        <div class="ebd-custom-field-head ebd-clearfix">
                            <h4><?php echo esc_attr($custom_field_details[ 'field_label' ]); ?></h4>
                            <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>
                            <span class="dashicons dashicons-trash ebd-remove-custom-field"></span>
                        </div>
                        <div class="ebd-custom-field-inner">
                            <div class="ebd-field-wrap">
                                <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
                                <div class="ebd-field">
                                    <input type="text" name="ebd_settings[custom_fields][<?php echo $custom_field_key; ?>][field_label]" value="<?php echo esc_attr($custom_field_details[ 'field_label' ]); ?>"/>
                                </div>
                            </div>
                            <?php
                            $option_field_types = array( 'radio', 'checkbox', 'select' );
                            if ( in_array($custom_field_details[ 'field_type' ], $option_field_types) ) {
                                ?>
                                <div class="ebd-field-wrap">
                                    <label><?php _e('Options', 'everest-business-directory'); ?></label>
                                    <div class="ebd-field">
                                        <div class="ebd-option-value-wrap ebd-sortable">
                                            <?php
                                            if ( !empty($custom_field_details[ 'option' ]) ) {
                                                $option_count = 0;
                                                foreach ( $custom_field_details[ 'option' ] as $option ) {
                                                    ?>
                                                    <div class="ebd-each-option">
                                                        <input type="text" name="ebd_settings[custom_fields][<?php echo $custom_field_key; ?>][option][]" placeholder="<?php _e('Option', 'everest-business-directory'); ?>" value="<?php echo esc_attr($option); ?>"/>
                                                        <input type="text" name="ebd_settings[custom_fields][<?php echo $custom_field_key; ?>][value][]" placeholder="<?php _e('Value', 'everest-business-directory'); ?>" value="<?php echo $custom_field_details[ 'value' ][ $option_count ]; ?>"/>
                                                        <span class="dashicons dashicons-no-alt ebd-remove-option"></span>
                                                    </div>
                                                    <?php
                                                    $option_count++;
                                                }
                                            }
                                            ?>
                                        </div>
                                        <input type="button" class="ebd-option-adder button-secondary" data-field-key="<?php echo $custom_field_key; ?>" value="<?php _e('Add Option', 'everest-business-directory'); ?>"/>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="ebd-field-wrap">
                                <label><?php _e('Frontend Display', 'everest-business-directory'); ?></label>
                                <div class="ebd-field">
                                    <input type="checkbox" name="ebd_settings[custom_fields][<?php echo $custom_field_key; ?>][frontend_display]" value="1" <?php echo (!empty($custom_field_details[ 'frontend_display' ])) ? 'checked="checked"' : ''; ?>/>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="ebd_settings[custom_fields][<?php echo $custom_field_key; ?>][field_type]" value="<?php echo $custom_field_details[ 'field_type' ]; ?>"/>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <input type="hidden" name="action" value="ebd_settings_action"/>
        <input type="hidden" name="tab" value="custom_fields"/>
        <?php wp_nonce_field('ebd_settings_nonce', 'ebd_settings_nonce'); ?>
        <input type="submit" value="<?php _e('Save Settings', 'everest-business-directory'); ?>" class="button-primary ebd-custom-fields-save"/>

    </form>
</div>