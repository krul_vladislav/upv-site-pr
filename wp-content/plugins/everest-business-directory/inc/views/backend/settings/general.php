<form class="ebd-settings-group-form" method="post" action="<?php echo admin_url('admin-post.php'); ?>">
    <input type="hidden" name="action" value="ebd_settings_action"/>
    <input type="hidden" name="tab" value="general"/>
    <?php wp_nonce_field('ebd_settings_nonce', 'ebd_settings_nonce'); ?>
    <div class="ebd-field-wrap">
        <label><?php _e('Directory Listing Page', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][listing_page]">
                <option value=""><?php _e('Choose Page', 'everest-business-directory'); ?></option>
                <?php
                $listing_page = (!empty($ebd_settings[ 'general' ][ 'listing_page' ])) ? intval($ebd_settings[ 'general' ][ 'listing_page' ]) : '';
                $pages = get_pages();
                foreach ( $pages as $page ) {
                    ?>
                    <option value="<?php echo $page->ID; ?>" <?php selected($listing_page, $page->ID); ?>><?php echo $page->post_title; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Directory Dashboard Page', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][dashboard_page]">
                <option value=""><?php _e('Choose Page', 'everest-business-directory'); ?></option>
                <?php
                $dashboard_page = (!empty($ebd_settings[ 'general' ][ 'dashboard_page' ])) ? intval($ebd_settings[ 'general' ][ 'dashboard_page' ]) : '';
                $pages = get_pages();
                foreach ( $pages as $page ) {
                    ?>
                    <option value="<?php echo $page->ID; ?>" <?php selected($dashboard_page, $page->ID); ?>><?php echo $page->post_title; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Per Page', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input name="ebd_settings[general][per_page]" type="number" min="1" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'per_page' ])) ? intval($ebd_settings[ 'general' ][ 'per_page' ]) : 10; ?>"/>
            <p class="description"><?php _e('Please enter the total number of directories that you want to display in the listing page. Default is 10.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Pagination Template', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][pagination_template]" class="ebd-template-trigger">
                <?php
                $pagination_template = (!empty($ebd_settings[ 'general' ][ 'pagination_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'pagination_template' ]) : 'template-1';
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <option value="template-<?php echo $i; ?>" <?php selected($pagination_template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="ebd-template-previews">
                <?php
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <img src="<?php echo EBD_URL . 'images/previews/pagination/pagination-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($pagination_template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('List Layout', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" value="1" name="ebd_settings[general][list_layout]" <?php echo (!empty($ebd_settings[ 'general' ][ 'list_layout' ])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check to enable list layout in the directory listing page', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('List Layout Template', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][list_layout_template]" class="ebd-template-trigger">
                <?php
                $list_layout_template = (!empty($ebd_settings[ 'general' ][ 'list_layout_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'list_layout_template' ]) : 'template-1';
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <option value="template-<?php echo $i; ?>" <?php selected($list_layout_template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="ebd-template-previews">
                <?php
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <img src="<?php echo EBD_URL . 'images/previews/list/list-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($list_layout_template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Grid Layout', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" value="1" name="ebd_settings[general][grid_layout]" <?php echo (!empty($ebd_settings[ 'general' ][ 'grid_layout' ])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check to enable grid layout in the directory listing page', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Grid Layout Template', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][grid_layout_template]" class="ebd-template-trigger">
                <?php
                $grid_layout_template = (!empty($ebd_settings[ 'general' ][ 'grid_layout_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_layout_template' ]) : 'template-1';
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <option value="template-<?php echo $i; ?>" <?php selected($grid_layout_template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="ebd-template-previews">
                <?php
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <img src="<?php echo EBD_URL . 'images/previews/grid/grid-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($grid_layout_template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Grid Column', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][grid_column]">
                <?php
                $grid_column = (!empty($ebd_settings[ 'general' ][ 'grid_column' ])) ? esc_attr($ebd_settings[ 'general' ][ 'grid_column' ]) : 2;
                ?>
                <option value="2" <?php selected($grid_column, 2); ?>>2</option>
                <option value="3" <?php selected($grid_column, 3); ?>>3</option>
                <option value="4" <?php selected($grid_column, 4); ?>>4</option>
            </select>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Image Original Size', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" name="ebd_settings[general][image_original_size]" value="1" <?php echo (!empty($ebd_settings[ 'general' ][ 'image_original_size' ])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check if you don\'t want to assign a fix height to the grid image.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('View Detail Link', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" name="ebd_settings[general][view_detail_link]" value="1" <?php echo (!empty($ebd_settings[ 'general' ][ 'view_detail_link' ])) ? 'checked = "checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check if you want to display the view detail link in the listing.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('View Detail Link Label', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input name="ebd_settings[general][view_detail_link_label]" type = "text" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'view_detail_link_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'view_detail_link_label' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please enter the label that you want to show for view detail link. Default is "View Detail".', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Map View', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" value="1" name="ebd_settings[general][map_view]" <?php echo (!empty($ebd_settings[ 'general' ][ 'map_view' ])) ? 'checked = "checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check to enable map view of each directory in the directory listing page', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Search Form', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" value="1" name="ebd_settings[general][search_form]" <?php echo (!empty($ebd_settings[ 'general' ][ 'search_form' ])) ? 'checked = "checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check to enable directory search form in the directory listing page', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Search Form Template', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][search_form_template]" class="ebd-template-trigger">
                <?php
                $search_form_template = (!empty($ebd_settings[ 'general' ][ 'search_form_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'search_form_template' ]) : 'template-1';
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <option value="template-<?php echo $i; ?>" <?php selected($search_form_template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="ebd-template-previews">
                <?php
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <img src="<?php echo EBD_URL . 'images/previews/search/search-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($search_form_template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Search Form Components', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php $search_form_components = (!empty($ebd_settings[ 'general' ][ 'search_form_components' ])) ? $ebd_settings[ 'general' ][ 'search_form_components' ] : array(); ?>
            <div class="ebd-each-component">
                <label><input type="checkbox" name="ebd_settings[general][search_form_components][]" value="keyword" <?php echo (in_array('keyword', $search_form_components)) ? 'checked = "checked"' : ''; ?>/>Keyword</label>
                <input type="text" name="ebd_settings[general][keyword_label]" placeholder="<?php _e('Keyword Label', 'everest-business-directory'); ?>" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'keyword_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'keyword_label' ]) : ''; ?>"/>
            </div>
            <div class="ebd-each-component">
                <label><input type="checkbox" name="ebd_settings[general][search_form_components][]" value="location" <?php echo (in_array('location', $search_form_components)) ? 'checked = "checked"' : ''; ?>/>Location</label>
                <input type="text" name="ebd_settings[general][location_label]" placeholder="<?php _e('Location Label', 'everest-business-directory'); ?>" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'location_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'location_label' ]) : ''; ?>"/>
            </div>
            <div class="ebd-each-component">
                <label><input type="checkbox" name="ebd_settings[general][search_form_components][]" value="category" <?php echo (in_array('category', $search_form_components)) ? 'checked = "checked"' : ''; ?>/>Category</label>
                <input type="text" name="ebd_settings[general][category_label]" placeholder="<?php _e('Category Label', 'everest-business-directory'); ?>" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'category_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'category_label' ]) : ''; ?>"/>
            </div>
            <div class="ebd-each-component">
                <label><input type="checkbox" name="ebd_settings[general][search_form_components][]" value="tags" <?php echo (in_array('tags', $search_form_components)) ? 'checked = "checked"' : ''; ?>/>Tags</label>
                <input type="text" name="ebd_settings[general][tags_label]" placeholder="<?php _e('Tags Label', 'everest-business-directory'); ?>" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'tags_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'tags_label' ]) : ''; ?>"/>
            </div>
            
            <!-- CUSTOM_FIELD -->
            <div class="ebd-each-component">
                <label><input type="checkbox" name="ebd_settings[general][search_form_components][]" value="field225073" <?php echo (in_array('field225073', $search_form_components)) ? 'checked = "checked"' : ''; ?>/>225073</label>
                <input type="text" name="ebd_settings[general][tags_label]" placeholder="<?php _e('FIELD 225073 Label', 'everest-business-directory'); ?>" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'field225073_label' ])) ? esc_attr($ebd_settings[ 'general' ][ 'field225073_label' ]) : ''; ?>"/>
            </div>
            
            <!-- CUSTOM_FIELD END-->
            
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Excerpt Length', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input name="ebd_settings[general][excerpt_length]" type="number" min="1" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'excerpt_length' ])) ? intval($ebd_settings[ 'general' ][ 'excerpt_length' ]) : 25; ?>"/>
            <p class="description"><?php _e('Please enter the excerpt length of the directory that you want to display in the listing page. Default is 25 words.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Featured Text', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input name="ebd_settings[general][featured_text]" type = "text" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'featured_text' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_text' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please enter the text that you want to display in the featured directory as the ribbon. Please leave blank if you don\'t want to display the featured ribbon', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Featured Ribbon Template', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[general][featured_ribbon_template]" class="ebd-template-trigger">
                <?php
                $featured_ribbon_template = (!empty($ebd_settings[ 'general' ][ 'featured_ribbon_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'featured_ribbon_template' ]) : 'template-1';
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <option value="template-<?php echo $i; ?>" <?php selected($featured_ribbon_template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="ebd-template-previews">
                <?php
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <img src="<?php echo EBD_URL . 'images/previews/ribbon/ribbon-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($featured_ribbon_template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('No Result Found Text', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input name="ebd_settings[general][no_result_text]" type = "text" value="<?php echo (!empty($ebd_settings[ 'general' ][ 'no_result_text' ])) ? esc_attr($ebd_settings[ 'general' ][ 'no_result_text' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please enter the text that you want to display for no results found. Default is No Results Found.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Directory Expiry', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" value="1" name="ebd_settings[general][directory_expiry]" <?php echo (!empty($ebd_settings[ 'general' ][ 'directory_expiry' ])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php _e('Please check to enable directory expiry feature for directory listing.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label></label>
        <div class='ebd-field'>
            <input type="submit" class="button-primary" value="<?php _e('Save Settings', 'everest-business-directory'); ?>"/>
        </div>
    </div>

</form>