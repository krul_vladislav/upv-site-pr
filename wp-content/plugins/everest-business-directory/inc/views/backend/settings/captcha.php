<form class="ebd-settings-group-form" method="post" action="<?php echo admin_url('admin-post.php'); ?>">
    <input type="hidden" name="action" value="ebd_settings_action"/>
    <input type="hidden" name="tab" value="captcha"/>
    <?php wp_nonce_field('ebd_settings_nonce', 'ebd_settings_nonce'); ?>
    <div class="ebd-field-wrap">
        <label><?php _e('ReCaptcha Site Key', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[captcha][site_key]" value="<?php echo (!empty($ebd_settings[ 'captcha' ][ 'site_key' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'site_key' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please go <a href="https://www.google.com/recaptcha/admin" target="_blank">here</a> to get the google reCaptcha site key.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('ReCaptcha Secret Key', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[captcha][secret_key]" value="<?php echo (!empty($ebd_settings[ 'captcha' ][ 'secret_key' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'secret_key' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please go <a href="https://www.google.com/recaptcha/admin" target="_blank">here</a> to get the google reCaptcha secret key.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Enable ReCaptcha in frontend form', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $frontend_captcha = (!empty($ebd_settings[ 'captcha' ][ 'frontend_captcha' ])) ? 1 : 0;
            ?>
            <input type="checkbox" name="ebd_settings[captcha][frontend_captcha]" value="1" <?php checked($frontend_captcha, true); ?>/>
            <p class="description"><?php _e('Please check to enable to reCaptcha in frontend form.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Enable ReCaptcha in login form', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $login_captcha = (!empty($ebd_settings[ 'captcha' ][ 'login_captcha' ])) ? 1 : 0;
            ?>
            <input type="checkbox" name="ebd_settings[captcha][login_captcha]" value="1" <?php checked($login_captcha, true); ?>/>
            <p class="description"><?php _e('Please check to enable to reCaptcha in login form.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('ReCaptcha Label', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[captcha][captcha_label]" value="<?php echo (!empty($ebd_settings[ 'captcha' ][ 'captcha_label' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'captcha_label' ]) : ''; ?>"/>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('ReCaptcha Error Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[captcha][error_message]" value="<?php echo (!empty($ebd_settings[ 'captcha' ][ 'error_message' ])) ? esc_attr($ebd_settings[ 'captcha' ][ 'error_message' ]) : ''; ?>"/>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label></label>
        <div class='ebd-field'>
            <input type="submit" class="button-primary" value="<?php _e('Save Settings', 'everest-business-directory'); ?>"/>
        </div>
    </div>
</form>