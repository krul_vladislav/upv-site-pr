<?php
defined('ABSPATH') or die('No script kiddies please!!');
?>
<div class="ebd-frontend-form-settings-wrap ebd-clearfix">
    <div class="ebd-frontend-form-navigation">
        <ul>
            <li><a href="javascript:void(0);" data-section-ref="general" class="ebd-ff-trigger ebd-active-ff-trigger"><span class="dashicons dashicons-admin-generic"></span><?php _e('General', 'everest-business-directory'); ?></a></li>
            <li><a href="javascript:void(0);" data-section-ref="email" class="ebd-ff-trigger"><span class="dashicons dashicons-email"></span><?php _e('Email', 'everest-business-directory'); ?></a></li>
            <li><a href="javascript:void(0);" data-section-ref="form-fields" class="ebd-ff-trigger"><span class="dashicons dashicons-exerpt-view"></span><?php _e('Form Fields', 'everest-business-directory'); ?></a></li>
            <li><a href="javascript:void(0);" data-section-ref="form-layout" class="ebd-ff-trigger"><span class="dashicons dashicons-welcome-widgets-menus"></span><?php _e('Form Layout', 'everest-business-directory'); ?></a></li>
        </ul>
    </div>
    <form method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <div class="ebd-frontend-form-options-wrap">
            <input type="hidden" name="action" value="ebd_settings_action"/>
            <input type="hidden" name="tab" value="frontend_form"/>
            <?php wp_nonce_field('ebd_settings_nonce', 'ebd_settings_nonce'); ?>
            <?php include(EBD_PATH . 'inc/views/backend/frontend-form/general.php'); ?>
            <?php include(EBD_PATH . 'inc/views/backend/frontend-form/email.php'); ?>

            <?php include(EBD_PATH . 'inc/views/backend/frontend-form/form-layout.php'); ?>

            <?php include(EBD_PATH . 'inc/views/backend/frontend-form/form-fields.php'); ?>
        </div>
        <input type="submit" value="<?php _e('Save Settings', 'everest-business-directory'); ?>" class="button-primary ebd-custom-fields-save"/>

    </form>
</div>