<form class="ebd-settings-group-form" method="post" action="<?php echo admin_url('admin-post.php'); ?>">
    <input type="hidden" name="action" value="ebd_settings_action"/>
    <input type="hidden" name="tab" value="directory_detail"/>
    <?php wp_nonce_field('ebd_settings_nonce', 'ebd_settings_nonce'); ?>
    <div class="ebd-detail-fields-wrap">
        <div class="ebd-field-wrap">
            <label><?php _e('Directory Detail Template', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <select name="ebd_settings[directory_detail][template]" class="ebd-directory-detail-preview-trigger">
                    <?php
                    $template = (!empty($ebd_settings[ 'directory_detail' ][ 'template' ])) ? esc_attr($ebd_settings[ 'directory_detail' ][ 'template' ]) : 'template-1';
                    for ( $i = 1; $i <= 3; $i++ ) {
                        ?>
                        <option value="template-<?php echo $i; ?>" <?php selected($template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <h3><?php _e('Directory Detail Components', 'everest-business-directory'); ?></h3>
        <div class="ebd-field-wrap">
            <label><?php _e('Featured Image', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][featured_image]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'featured_image' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display featured image in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Directory Gallery', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][directory_gallery]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'directory_gallery' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display gallery images in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Directory Tags', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][directory_tags]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'directory_tags' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display directory tags in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>

        <div class="ebd-field-wrap">
            <label><?php _e('Directory Category', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][directory_category]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'directory_category' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display directory category in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>

        <div class="ebd-field-wrap">
            <label><?php _e('Social Information', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][social_information]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'social_information' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display directory social information in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Location Information', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][location_information]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'location_information' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display directory location information in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Contact Information', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" value="1" name="ebd_settings[directory_detail][contact_information]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'contact_information' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please check to display directory contact information in the directory detail page', 'everest-business-directory'); ?></p>
            </div>
        </div>
        <?php /*
          <div class="ebd-field-wrap">
          <label><?php _e('Map View', 'everest-business-directory'); ?></label>
          <div class="ebd-field">
          <input type="checkbox" value="1" name="ebd_settings[directory_detail][map_view]" <?php echo (!empty($ebd_settings[ 'directory_detail' ][ 'map_view' ])) ? 'checked="checked"' : ''; ?>/>
          <p class="description"><?php _e('Please check to enable map view in the directory detail page', 'everest-business-directory'); ?></p>
          </div>
          </div>
         *
         */ ?>
        <div class="ebd-field-wrap">
            <label></label>
            <div class='ebd-field'>
                <input type="submit" class="button-primary" value="<?php _e('Save Settings', 'everest-business-directory'); ?>"/>
            </div>
        </div>
    </div>
    <div class="ebd-directory-previews">
        <?php
        for ( $i = 1; $i <= 5; $i++ ) {
            ?>
            <img src="<?php echo EBD_URL . 'images/previews/detail/directory-detail-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
            <?php
        }
        ?>
    </div>
    <div class="ebd-clear"></div>
</form>