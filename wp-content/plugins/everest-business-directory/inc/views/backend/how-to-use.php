<div class="wrap ebd-settings-wrap">
    <div class="ebd-head ebd-clearfix">
        <img src="<?php echo EBD_IMG_URL . 'logo.png' ?>"/>
        <h3><?php _e('How to Use', 'everest-business-directory'); ?></h3>
    </div>

    <div class="ebd-content-wrap">
        <h3>You can check our full and detailed documentation <a href="https://accesspressthemes.com/documentation/everest-business-directory/">here</a>.</h3>

        <p>Once you have installed the plugin, you will be able to find Directory Menu in the main WordPress Admin menu. Under which there are various sub menus.</p>
        <ul>
            <li>All Directories</li>
            <li>Add New</li>
            <li>Categories</li>
            <li>Tags</li>
            <li>Settings</li>
            <li>How to Use</li>
            <li>About Us</li>
        </ul>

        <h3>All Directories</h3>
        <p>This contains all the list of added directories.</p>

        <h3>Add New</h3>
        <p>From this section, you can add the new directory and while adding you can configure various parameter of the directory such as <strong>Directory Title,Directory Content, Directory Image, Directory Categories, Directory Tags, Location Information, Social Information, Contact Information and Gallery.</strong></p>

        <h3>Categories</h3>
        <p>In this section you can add various categories for the directory which you can assign while adding the directory.</p>

        <h3>Tags</h3>
        <p>In this section you can add various tags for the directory which you can assign while adding the directory.</p>

        <h3>Settings</h3>
        <p>This is the main configuration page for the directory for setting your site as the directory listing site. There are 6 sub sections which contains individual set of settings of same type.</p>

        <ul>
            <li>
                <strong>General Settings</strong>
                <p>In this section, you can configure all the general settings related with the plugin. You can choose directory listing page with templates, directory dashboard page and some other general configurations.</p>
            </li>
            <li>
                <strong>Map Settings</strong>
                <p>In this section, you can configure all the settings related with the map. You can add Google Map API Key, Base Address, Base Latitude, Map height, width and some other configurations related with map.</p>
            </li>
            <li>
                <strong>Custom Fields</strong>
                <p>In this section, you can add new custom fields if the pre available fields doesn't meet your requirements. You can add any  number of fields which will can be included in the directory submission form and also the received values can be displayed in the directory detail page.</p>
            </li>

            <li>
                <strong>Frontend Form</strong>
                <p>In this section, you can find all the necessary configurations related with directory Submission Form. You can choose which of the fields to include in the frontend submission form along with the email notifications settings and form layout.</p>
            </li>

            <li>
                <strong>Directory Detail</strong>
                <p>In this section you can choose layout for the directory detail page along with individual section to enable or disable in the directory detail page.</p>
            </li>
            <li>
                <strong>Captcha Settings</strong>
                <p>In this section you can configure necessary settings for the captcha.</p>
            </li>
        </ul>

        <h3>Shortcodes</h3>

        <div class="ebd-highlight"><strong>Frontend Form Shortcode</strong><input type="text" readonly="readonly" value="[ebd_frontend_form]" onfocus="this.select();"/></div>
        <div class="ebd-highlight"><strong>Frontend Directory Dashboard Shortcode</strong><input type="text" readonly="readonly" value="[ebd_directory_dashboard]" onfocus="this.select();"/></div>
        <div class="ebd-highlight"><strong>Frontend Directory Listing Shortcode</strong><input type="text" readonly="readonly" value="[ebd_directory_listing]" onfocus="this.select();"/></div>
        <div class="ebd-highlight">
            <strong>Category Wise Frontend Directory Listing Shortcode</strong>
            <input type="text" readonly="readonly" value="[ebd_directory_listing directory_category='slug_of_directory_category']" onfocus="this.select();"/>
            <p class="description">For example: [ebd_directory_listing directory_category='automobile']</p>
        </div>
        <div class="ebd-highlight">
            <strong>Featured Directory Shortcode</strong>
            <input type="text" readonly="readonly" value="[ebd_featured layout='grid/list' template='template-1 to template-5' number='total number of featured directories']" onfocus="this.select();"/>
            <p class="description">For example: [ebd_featured list='list' template='template-1' number='5'] </p>
        </div>
        <div class="ebd-highlight">
            <strong>Specific Directory Listing Shortcode</strong>
            <input type="text" readonly="readonly" value="[ebd_directory_specific_listing directory_category='slug_of_directory_category' directory_tag='slug_of_directory_tag' layout='grid/list' template='template-1 to template-5' number='total number of featured directories']" onfocus="this.select();"/>
            <p class="description">For example: [ebd_directory_specific_listing directory_category='automobile' list='list' template='template-1' number='5'] </p>
        </div>
        <div class="ebd-highlight"><strong>Search Form</strong> <input type='text' readonly="readonly" value="[ebd_search_form]" onfocus="this.select();"/></div>
        <div class="ebd-highlight"><strong>Category Lists</strong> <input type='text' readonly="readonly" value="[ebd_directory_categories_list]" onfocus="this.select();"/></div>
        <div class="ebd-highlight"><strong>Tags Lists</strong> <input type='text' readonly="readonly" value="[ebd_directory_tags_list]" onfocus="this.select();"/></div>

        <h3 id="ebd-template-customizations">Template Customizations</h3>
        <p>The directory detail page and directory archive page layout may or may not match with your existing theme. That is because your active theme might have a different html structure than what we had provided in our plugin. Though we had tried to provide the html structure as standard as possible but it entirely depends upon the theme. We can only fetch the header, footer and sidebar using the get_header, get_footer and get_sidebar funtions provided by WordPress. But there will be other html markups beside generated by header, footer and sidebar.</p>

        <p>Since directory generated by our plugin is custom post type and its details pages shall be loaded from the theme and in WordPress there isn't any specific method to know the exact html structure generated by theme. So we have added a customize section where you can adjust the markup of the our directory templates as per the theme. For this you can check single.php template file of your theme and add necessary markup just above and below the #template_content in the single page template. </p>
        <p>In case you want to customize entire template structure as per your need then that is also possible by simply copying ebd-templates folder inside your active theme. Then you can customize necessary template files from the ebd-template folder copied in the theme which won't get overriden by plugin update but you may need to copy the template files once in while when we do some changes in the templates files itself. We will try to do this less a possible but we may need to update it once in a while to add new features or fix the bugs.</p>
        <p>So in case you have copied the template folder, then we suggest you to delete other files which you won't customize so that you can track the files in which you had done the changes. But please don't do any modifications and don't delete the folder from the plugin.</p>

        <p>We do understand that we have explained this in a bit technical term but we really needed to because the template structure isn't much controllable from the plugin itself. Same happens with the WooCommerce too. That is the reason, themes make it compatible with WooCommerc rather than other way around. And we have also tried to follow the same as much as possible.</p>

    </div>
</div>