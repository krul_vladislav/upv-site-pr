<?php
global $ebd_settings;
if ( $ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ] == 'login_message' ) {
    ?>
    <div class="ebd-login-message-wrap">
        <p><?php echo $frontend_form_settings[ 'general' ][ 'login_message' ]; ?></p>
        <a href="<?php echo esc_url($frontend_form_settings[ 'general' ][ 'login_link_url' ]); ?>"><?php echo esc_attr($frontend_form_settings[ 'general' ][ 'login_link_text' ]); ?></a>
    </div>
    <?php
} else {
    include(EBD_PATH . '/inc/views/frontend/login-form.php');
}