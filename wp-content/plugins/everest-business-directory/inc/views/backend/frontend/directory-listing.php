<div class="ebd-main-wrap">
    <?php
    $data[ 'atts' ] = (!empty($atts)) ? $atts : array();
    include(EBD_PATH . 'inc/cores/current-view.php');
    if ( !empty($ebd_settings[ 'general' ][ 'search_form' ]) ) {
        ebd_load_template_part('archive/search-form', '', $data);
    }
    ebd_load_template_part('archive/directory-view-toggle', '', $data);
    $layout_counter = 0;
    if ( !empty($ebd_settings[ 'general' ][ 'list_layout' ]) ) {
        $layout_counter++;
        include(EBD_PATH . 'inc/views/frontend/list-layout.php');
    }
    if ( !empty($ebd_settings[ 'general' ][ 'grid_layout' ]) ) {
        $layout_counter++;
        include(EBD_PATH . 'inc/views/frontend/grid-layout.php');
    }
    if ( !empty($ebd_settings[ 'general' ][ 'map_view' ]) ) {
        $layout_counter++;
        include(EBD_PATH . 'inc/views/frontend/map-view.php');
    }
    ?>

</div>