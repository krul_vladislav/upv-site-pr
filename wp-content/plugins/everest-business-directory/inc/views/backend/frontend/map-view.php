<?php
$_GET[ 'layout' ] = $layout = 'map';
?>
<div class="ebd-map-view-wrap ebd-col-9 ebd-map-layout ebd-directory-listing-wrap" <?php $this->display_none($current_view, 'map'); ?>>
    <?php include(EBD_PATH . 'inc/views/frontend/common/list-query.php'); ?>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=en&key=<?php echo (!empty($ebd_settings[ 'map' ][ 'map_api_key' ])) ? esc_attr($ebd_settings[ 'map' ][ 'map_api_key' ]) : ''; ?>" type="text/javascript"></script>
    <textarea id="ebd-map-locations" style="display:none;"><?php echo json_encode($directory_array); ?></textarea>
</div>
