<?php
if ( $directory_listing_query->max_num_pages > 1 ) {
    $pagination_template = (!empty($ebd_settings[ 'general' ][ 'pagination_template' ])) ? esc_attr($ebd_settings[ 'general' ][ 'pagination_template' ]) : 'template-1';
    ?>
    <div class="ebd-pagination-wrap ebd-pagination-<?php echo $pagination_template; ?>">
        <?php
        $big = 999999999; // need an unlikely integer
        $translated = __('Page', 'everest-business-directory'); // Supply translatable string
        $page_num_link = explode('?', esc_url(get_pagenum_link($big)));

        if ( !empty($_GET) ) {
            $page_num_link = $page_num_link[ 0 ] . '?' . http_build_query($_GET);
        } else {
            $page_num_link = $page_num_link[ 0 ];
        }

        echo paginate_links(array(
            'base' => str_replace($big, '%#%', $page_num_link),
            'format' => '?%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $directory_listing_query->max_num_pages,
            'before_page_number' => '<span class="screen-reader-text">' . $translated . ' </span>',
            'prev_text' => __('Previous', 'everest-business-directory'),
            'next_text' => __('Next', 'everest-business-directory'),
        ));
        ?>
    </div>
    <?php
}