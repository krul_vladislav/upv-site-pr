<?php
wp_reset_query();
wp_reset_postdata();
global $directory_array;

$directory_array = array();
if ($layout_counter == 1) {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $per_page = (!empty($ebd_settings['general']['per_page'])) ? intval($ebd_settings['general']['per_page']) : 10;
    $directory_listing_args = array('posts_per_page' => $per_page, 'post_status' => 'publish', 'post_type' => 'ebd', 'paged' => $paged);
    if (!empty($atts['directory_category'])) {
        $directory_category = $atts['directory_category'];
        $tax_query = array(array('taxonomy' => 'ebd-categories', 'field' => 'slug', 'terms' => $directory_category));
        $directory_listing_args['tax_query'] = $tax_query;
    }
    global $ebd_directory_expiry;
    if ($ebd_directory_expiry) {
        $directory_listing_args['meta_query'][] = array('key' => '_ebd_expiry_date', 'value' => date('Y-m-d'), 'compare' => '>=', 'type' => 'DATE');
    }
    if (!empty($_GET['search_keyword'])) {
        $search_keyword = sanitize_text_field($_GET['search_keyword']);
        $directory_listing_args['terms'] = $search_keyword;
    }
    if (!empty($_GET['directory_category'])) {
        $directory_category = intval($_GET['directory_category']);
        $tax_query = array(array('taxonomy' => 'ebd-categories', 'field' => 'id', 'terms' => $directory_category));
        $directory_listing_args['radius'] = $directory_category;
    }
    if (!empty($_GET['directory_tag'])) {
        $directory_tag = intval($_GET['directory_tag']);
        $tax_query = array(array('taxonomy' => 'ebd-tags', 'field' => 'id', 'terms' => $directory_tag));
        $directory_listing_args['tax_query'] = $tax_query;
    }
    if (!empty($_GET['location'])) {
        $location = sanitize_text_field($_GET['location']);
        $meta_query = array('key' => '_ebd_address', 'value' => $location, 'compare' => 'LIKE');
        $directory_listing_args['location'] = $location;
    }

    $client = new Teamleader($ebd_settings['map']['map_api_key']);
    $directory_listing_query = false;
    $directory_listing_query_new = $client->getContactsFromDB($directory_listing_args);

    $temp_query = $directory_listing_query;
} else {
    $directory_listing_query = $temp_query;
}

$data['directory_listing_query'] = $directory_listing_query;
global $ebd_directory_display;
include(EBD_PATH . 'inc/views/frontend/common/query-loop.php');
if ($layout == 'map') {
    $map_width = (!empty($ebd_settings['map']['map_width'])) ? esc_attr($ebd_settings['map']['map_width']) : '100%';
    $map_height = (!empty($ebd_settings['map']['map_height'])) ? esc_attr($ebd_settings['map']['map_height']) : '500px';
    if (empty($directory_array)) {
        ?>
        <div id="ebd-map" style="height: <?php echo $map_height; ?>;width:<?php echo $map_width; ?>;"
             data-base-latitude="<?php echo esc_attr($ebd_settings['map']['base_latitude']); ?>"
             data-base-longitude="<?php echo esc_attr($ebd_settings['map']['base_longitude']); ?>"
             data-zoom-level="<?php echo intval($ebd_settings['map']['zoom_level']); ?>"></div>
        <?php
//      
    } else {
        ?>
        <div id="ebd-map" style="height: <?php echo $map_height; ?>;width:<?php echo $map_width; ?>;"
             data-base-latitude="<?php echo esc_attr($directory_array[0][1]); ?>"
             data-base-longitude="<?php echo esc_attr($directory_array[0][2]); ?>"
             data-zoom-level="<?php echo intval($ebd_settings['map']['zoom_level']); ?>"></div>
<?php
     function get_info_for_map(){
         global $directory_array;
?>
                <div style="display: flex; flex-direction: row; flex-wrap: wrap; max-width: 100%; color: transparent">
        <?php
      $count_teamleader=0;
  
      while($count_teamleader++<5000)
      {
          if($directory_array[$count_teamleader][0]!=null)
          {
                ?>
              <div class="ebd-map-info2 " style="width: 33%; height: 150px ">
                  <div class="ebd-info-content-wrap" style="border: 1px solid gray; height: 100%" >
               <?php echo var_dump($directory_array[$count_teamleader][0]);
                           ?>
              </div>
              </div>
             <?php
          }
          else              break;
      }
    ?>
      </div>
<div id="showmore"><div class="showmore-button">Montre plus</div></div>
    <?php

    }
    }
}
//ebd_load_template_part('archive/list/content-list-template-1');
//include(EBD_PATH . 'inc/views/frontend/common/pagination.php');
/**
 * Pagination
 */
ebd_load_template_part('common/pagination', '', $data);
 ?>