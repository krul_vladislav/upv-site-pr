<?php
$directory_id = intval($_GET[ 'directory_id' ]);
$current_user = wp_get_current_user();
$current_user_id = $current_user->ID;
$directory_author = get_post_field('post_author', $directory_id);
if ( $directory_author != $current_user_id ) {
    ?>
    <p class="ebd-invalid-msg"><?php _e('Invalid directory', 'everest-business-directory'); ?></p>
    <?php
} else {
    $directory = get_post($directory_id);
    $directory_title = get_the_title($directory_id);
    $directory_content = $directory->post_content;
    $location_fields = array( 'latitude', 'longitude', 'address', 'city', 'country', 'postal_code' );
    $contact_fields = array( 'email_address', 'website', 'phone_number' );
    $social_fields = array( 'facebook', 'twitter', 'googleplus', 'instagram', 'youtube', 'linkedin' );
    $directory_fields = array( 'location_fields', 'contact_fields', 'social_fields' );
    foreach ( $directory_fields as $directory_field_key ) {
        foreach ( $$directory_field_key as $field_key ) {
            $$field_key = get_post_meta($directory_id, '_ebd_' . $field_key, true);
        }
    }
    $directory_thumbnail_id = get_post_thumbnail_id($directory_id);

    $directory_gallery = get_post_meta($directory_id, '_ebd_gallery', true);
    $directory_categories = get_the_terms($directory_id, 'ebd-categories');
    $directory_catgory_ids = array();
    if ( !empty($directory_categories) ) {
        foreach ( $directory_categories as $directory_category ) {
            $directory_catgory_ids[] = $directory_category->term_id;
        }
    }
    $gallery = get_post_meta($directory_id, '_ebd_gallery', true);
    $gallery_ids = (!empty($gallery)) ? implode(',', $gallery) : '';
    //$this->print_array($directory_categories);
    $directory_tags = get_the_terms($directory_id, 'ebd-tags');
    $directory_tag_ids = array();
    $directory_tag_strings = array();
    if ( !empty($directory_tags) ) {
        foreach ( $directory_tags as $directory_tag ) {
            $directory_tag_ids[] = $directory_tag->term_id;
            $directory_tag_strings[] = $directory_tag->name;
        }
    }
    $directory_tag_strings = implode(',', $directory_tag_strings);

    $image_class = (has_post_thumbnail()) ? 'ebd-yes-image' : 'ebd-no-image';
    $expiry_date = get_post_meta($directory_id, '_ebd_expiry_date', true);
    if ( !empty($expiry_date) ) {
        //  $expiry_date = date('F d,Y', strtotime($expiry_date));
    }
    include(EBD_PATH . 'inc/views/frontend/frontend-form.php');
}
