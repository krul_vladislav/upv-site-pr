<div class="ebd-each-frontend-field ebd-tags-sec">
    <label class="ebd-title-field ebd-tags-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <div data-field-key="directory_tags">
            <?php
            $display_type = isset($field_details[ 'display_type' ]) ? esc_attr($field_details[ 'display_type' ]) : 'dropdown';

            $terms = get_terms('ebd-tags', array( 'hide_empty' => 0 ));
            $categoryHierarchy = array();
            $this->sort_terms_hierarchicaly($terms, $categoryHierarchy, 0);
            $terms_exclude = isset($field_array[ 'exclude_tags' ]) ? explode(',', $field_array[ 'exclude_tags' ]) : array();
            //var_dump($directory_tag_strings);
            switch ( $display_type ) {
                case 'dropdown':
                    ?>
                    <select name="frontend_form[frontend_fields][directory_tags]">
                        <option value=""><?php echo (!empty($field_details[ 'first_label' ])) ? esc_attr($field_details[ 'first_label' ]) : __('Choose Tags', 'everest-business-directory'); ?></option>
                        <?php
                        echo $this->print_option($categoryHierarchy, $terms_exclude, 0, '', 'directory_tags', (!empty($directory_tag_strings)) ? explode(',', $directory_tag_strings) : array());
                        ?>
                    </select>
                    <?php
                    break;
                case 'checkbox':
                    echo $this->print_checkbox($categoryHierarchy, $terms_exclude, 0, '', 'frontend_form[frontend_fields][directory_tags]', (!empty($directory_tag_strings)) ? explode(',', $directory_tag_strings) : array());
                    break;
                case 'textfield':
                    ?>
                    <input type="text" name="frontend_form[frontend_fields][directory_tags]" value="<?php echo (!empty($directory_tag_strings)) ? esc_attr($directory_tag_strings) : ''; ?>"/>
                    <?php
                    break;
            }
            ?>
        </div>
    </div>
</div>