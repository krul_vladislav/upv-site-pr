<div class="ebd-each-frontend-field ebd-category-sec">
    <label class="ebd-title-field ebd-category-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <div data-field-key="directory_category">
            <?php
            $display_type = isset($field_details[ 'display_type' ]) ? esc_attr($field_details[ 'display_type' ]) : 'dropdown';

            $terms = get_terms('ebd-categories', array( 'hide_empty' => 0 ));
            $categoryHierarchy = array();
            $this->sort_terms_hierarchicaly($terms, $categoryHierarchy, 0);
            $terms_exclude = isset($field_array[ 'exclude_categories' ]) ? explode(',', $field_array[ 'exclude_categories' ]) : array();
            //  $this->print_array($directory_catgory_ids);
            switch ( $display_type ) {
                case 'dropdown':
                    ?>
                    <select name="frontend_form[frontend_fields][directory_category]" data-field-key="directory_category">
                        <option value=""><?php echo (!empty($field_details[ 'first_label' ])) ? esc_attr($field_details[ 'first_label' ]) : __('Choose Category', 'everest-business-directory'); ?></option>
                        <?php
                        echo $this->print_option($categoryHierarchy, $terms_exclude, 1, '', 'directory_category', (!empty($directory_catgory_ids)) ? $directory_catgory_ids : array());
                        ?>
                    </select>
                    <?php
                    break;
                case 'checkbox':
                    echo $this->print_checkbox($categoryHierarchy, $terms_exclude, 1, '', 'frontend_form[frontend_fields][directory_category]', (!empty($directory_catgory_ids)) ? $directory_catgory_ids : array(), 'directory_category');
                    break;
            }
            ?>
        </div>
    </div>
</div>