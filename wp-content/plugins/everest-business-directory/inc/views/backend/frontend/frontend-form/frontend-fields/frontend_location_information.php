<div class="ebd-each-frontend-field ebd-location-sec">
    <label class="ebd-title-field ebd-location-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <?php
        if ( isset($field_details[ 'show_map' ]) ) {
            if ( !empty($ebd_settings[ 'map' ][ 'map_api_key' ]) ) {
                $base_address = (!empty($ebd_settings[ 'map' ][ 'base_address' ])) ? esc_attr($ebd_settings[ 'map' ][ 'base_address' ]) : 'Castel Sant\'Angelo, Rome, Italy';
                $edit_address = (!empty($address)) ? $address : $base_address;
                ?>
                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=en&key=<?php echo esc_attr($ebd_settings[ 'map' ][ 'map_api_key' ]); ?>" type="text/javascript"></script>
                <div class="ebd-location-map-wrap">
                    <input type="text" class="ebd-map-address-input" placeholder="<?php _e('Enter address', 'everest-business-directory'); ?>"/>
                    <input type="button" class="ebd-plot-map" value="<?php _e('Plot map', 'everest-business-directory'); ?>"/>
                    <div id="ebd-map-frame" data-map-address="" style="height: 400px;width:100%;" data-zoom-level="<?php echo (!empty($ebd_settings[ 'map' ][ 'zoom_level' ])) ? intval($ebd_settings[ 'map' ][ 'zoom_level' ]) : 16; ?>" data-base-address="<?php echo $edit_address; ?>"></div>
                </div>
                <?php
            } else {
                _e('Map API key missing', 'everest-business-directory');
            }
        }
        ?>
        <div class="ebd-sub-field-wrap">
            <?php
            $sub_fields_array = array( 'latitude' => array( 'label' => __('Latitude', 'everest-business-directory') ),
                'longitude' => array( 'label' => __('Longitude', 'everest-business-directory') ),
                'address' => array( 'label' => __('Address', 'everest-business-directory') ),
                'city' => array( 'label' => __('City', 'everest-business-directory') ),
                'country' => array( 'label' => __('Country', 'everest-business-directory') ),
                'postal_code' => array( 'label' => __('Postal Code', 'everest-business-directory') ),
            );
            foreach ( $sub_fields_array as $sub_field_key => $sub_field_details ) {
                if ( !empty($field_details[ 'fields' ][ $sub_field_key ][ 'show' ]) ) {
                    $field_label = (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ])) ? esc_attr($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ]) : '';
                    ?>
                    <div class="ebd-sub-field">
                        <label><?php echo $field_label; ?></label>
                        <div class="ebd-field">
                            <input type="text" name="frontend_form[frontend_fields][location_information][<?php echo $sub_field_key; ?>]" data-field-key="<?php echo $sub_field_key; ?>" value="<?php echo (!empty($$sub_field_key)) ? $$sub_field_key : ''; ?>"/>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>