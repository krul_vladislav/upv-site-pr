<div class="ebd-each-frontend-field ebd-content-sec">
    <label class="ebd-title-field ebd-content-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">
        <div data-field-key="directory_content">
            <?php
            $editor_type = (!empty($field_details[ 'editor_type' ])) ? $field_details[ 'editor_type' ] : 'rich';
            switch ( $editor_type ) {
                case 'rich':
                    $teeny = false;
                    $show_quicktags = true;
                    break;
                case 'visual':
                    $teeny = false;
                    $show_quicktags = false;
                    break;
                case 'html':
                    $teeny = true;
                    $show_quicktags = true;
                    add_filter('user_can_richedit', create_function('', 'return false;'), 50);
                    break;
            }
            $settings = array(
                'textarea_name' => 'frontend_form[frontend_fields][directory_content]',
                'media_buttons' => false,
                'teeny' => $teeny,
                'wpautop' => true,
                'quicktags' => $show_quicktags,
                'editor_class' => 'ebd-wp-editor',
                'textarea_rows' => 10
            );
            if ( $editor_type == 'simple' ) {
                ?>
                <textarea name="frontend_form[frontend_fields][directory_content]"><?php echo (!empty($directory_content)) ? $directory_content : ''; ?></textarea>
                <?php
            } else {
                wp_editor((!empty($directory_content)) ? $directory_content : '', 'ebd_directory_content', $settings);
            }
            ?>
        </div>
    </div>
</div>
