<div class="ebd-each-frontend-field ebd-social-sec">
    <label class="ebd-title-field ebd-social-field"><?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?></label>
    <div class="ebd-field">

        <?php
        $sub_fields_array = array( 'facebook' => array( 'label' => __('Facebook', 'everest-business-directory') ),
            'twitter' => array( 'label' => __('Twitter', 'everest-business-directory') ),
            'googleplus' => array( 'label' => __('Google+', 'everest-business-directory') ),
            'instagram' => array( 'label' => __('Instagram', 'everest-business-directory') ),
            'youtube' => array( 'label' => __('youtube', 'everest-business-directory') ),
            'linkedin' => array( 'label' => __('LinkedIn', 'everest-business-directory') ),
        );
        foreach ( $sub_fields_array as $sub_field_key => $sub_field_details ) {
            if ( !empty($field_details[ 'fields' ][ $sub_field_key ][ 'show' ]) ) {
                $field_label = (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ])) ? esc_attr($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ]) : '';
                ?>
                <div class="ebd-sub-field ebd-social-<?php echo $sub_field_key; ?>">
                    <label><?php echo $field_label; ?></label>
                    <div class="ebd-field">
                        <input type="text" name="frontend_form[frontend_fields][social_information][<?php echo $sub_field_key; ?>]" data-field-key="<?php echo $sub_field_key; ?>" value="<?php echo (!empty($$sub_field_key)) ? $$sub_field_key : ''; ?>"/>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>