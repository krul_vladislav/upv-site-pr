<?php
global $ebd_settings;
$latitude = get_post_meta($post->ID, '_ebd_latitude', true);
$longitude = get_post_meta($post->ID, '_ebd_longitude', true);
$address = get_post_meta($post->ID, '_ebd_address', true);
$base_address = (!empty($ebd_settings[ 'map' ][ 'base_address' ])) ? esc_attr($ebd_settings[ 'map' ][ 'base_address' ]) : 'Castel Sant\'Angelo, Rome, Italy';
$base_address = (empty($address)) ? $base_address : esc_attr($address);
//$this->print_array($ebd_settings);
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=en&key=<?php echo esc_attr($ebd_settings[ 'map' ][ 'map_api_key' ]); ?>" type="text/javascript"></script>
<div class="ebd-location-map-wrap">
    <div class="ebd-map-search-field-wrap">
        <input type="text" class="ebd-map-address-input button-secondary" placeholder="<?php _e('Enter address', 'everest-business-directory'); ?>"/>
        <input type="button" class="ebd-plot-map" value="<?php _e('Plot map', 'everest-business-directory'); ?>"/>
    </div>
    <div id="ebd-map-frame" data-map-address="" style="height: 400px;width:100%;" data-zoom-level="<?php echo (!empty($ebd_settings[ 'map' ][ 'zoom_level' ])) ? intval($ebd_settings[ 'map' ][ 'zoom_level' ]) : 16; ?>" data-base-address="<?php echo $base_address; ?>"></div>
</div>