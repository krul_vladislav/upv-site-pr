<?php
defined('ABSPATH') or die('No script kiddies please!!');
foreach ( $_POST[ 'attachments' ] as $attachment ) {
    ?>
    <div class="ebd-each-gallery">
        <img src="<?php echo esc_url($attachment[ 'sizes' ][ 'thumbnail' ][ 'url' ]); ?>"/>
        <a href="javascript:void(0);" class="ebd-remove-gallery-item"><span class="dashicons dashicons-no-alt"></span></a>
        <input type="hidden" name="directory_fields[gallery][]" value="<?php echo intval(sanitize_text_field($attachment[ 'id' ])); ?>"/>
    </div>
    <?php
}
