<div class="wrap ebd-settings-wrap">
    <div class="ebd-head ebd-clearfix">
        <img src="<?php echo EBD_IMG_URL . 'logo.png' ?>"/>
        <h3><?php _e('About Us', 'everest-business-directory'); ?></h3>
    </div>
    <div class="ebd-content-wrap">
        <div class="ebd-content-section">
            <h4 class="ebd-content-title">About Plugin</h4>
            <p>Everest Business Directory is a <strong>PREMIUM WordPress Plugin</strong> to build a full fledged business directory site with just few backend configurations.You can add unlimited directories, list them in the site and provide the users a great interface to search directories, submit directories and manage directories from frontend through Directory Dashboard.</p>
            <p>You can choose any of the pre designed layouts for directory listing, directory form to assign the proper layout.</p>
            <p>With simple and easy user interface, you can build a directory site in few minutes.</p>
        </div>
        <div class="ebd-content-section">
            <h4 class="ebd-content-title">About Author</h4>
            <p>AccessPress Themes is a venture of Access Keys - who has developed hundreds of Custom WordPress themes and plugins for its clients over the years.</p>
            <p>Visit our site to know more: <a href="https://accesspressthemes.com" target="_blank">https://accesspressthemes.com</a></p>
        </div>
        <div class="ebd-content-section">
            <h4 class="ebd-content-title">More From AccesPress Themes</h4>
            <div class="ebd-sub-section">
                <div class="ebd-sub-section-title"><a href="http://accesspressthemes.com/plugins" target="_blank">WordPress Free &amp; Premium Plugins</a></div>
                <a href="http://accesspressthemes.com/plugins" target="_blank"><img src="<?php echo EBD_URL; ?>/images/plugin.png" alt="Wordpress Free &amp; Premium Plugins"></a>
            </div>
            <div class="ebd-sub-section">
                <div class="ebd-sub-section-title"><a href="http://accesspressthemes.com/themes" target="_blank">WordPress Free &amp; Premium Themes</a></div>
                <a href="http://accesspressthemes.com/themes" target="_blank"><img src="<?php echo EBD_URL; ?>/images/theme.png" alt="Wordpress Free &amp; Premium Themes"></a>
            </div>
            <div class="ebd-sub-section">
                <div class="ebd-sub-section-title"><a href="http://accesspressthemes.com/contact" target="_blank">WordPress Customization</a></div>
                <a href="http://accesspressthemes.com/contact" target="_blank"><img src="<?php echo EBD_URL; ?>/images/customize.png" alt="Wordpress Free &amp; Premium Plugins"></a>
            </div>
            <div class="ebd-clear"></div>
        </div>
        <div class="ebd-content-section">
            <h4 class="ebd-content-title">Get in Touch</h4>
            <p>If you've any question/feedback, please get in touch: </p>
            <p><strong>Codecanyon Support:</strong> <a href="https://codecanyon.net/user/accesskeys#contact">https://codecanyon.net/user/accesskeys#contact</a><br></p>
            <p><strong>General enquiries:</strong> <a href="mailto:info@accesspressthemes.com">info@accesspressthemes.com</a></p>
            <p><strong>Sales:</strong> <a href="mailto:sales@accesspressthemes.com">sales@accesspressthemes.com</a></p>

        </div>
        <div class="ebd-content-section">
            <h4 class="ebd-content-title">Get Social</h4>
            <p>Get connected with us on social media. Facebook is the best place to find updates on our themes/plugins: </p>

            <p><strong>Like us on facebook:</strong></p>

            <iframe style="border: none; overflow: hidden; width: 600px; height: 250px;" src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FAccessPress-Themes%2F1396595907277967&amp;width=842&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=1411139805828592" width="240" height="150" frameborder="0" scrolling="no"></iframe>
            <div class="ebd-social-block">
                <a href="https://twitter.com/apthemes" target="_blank"><img src="<?php echo EBD_URL; ?>/images/twitter.png"></a>
                <a href="https://plus.google.com/+Accesspressthemesprofile/" target="_blank"><img src="<?php echo EBD_URL; ?>/images/googleplus.png"></a>
                <a href="https://www.pinterest.com/accesspresswp/" target="_blank"><img src="<?php echo EBD_URL; ?>/images/pinterest.png"></a>
                <a href="https://www.flickr.com/photos/accesspressthemes/" target="_blank"><img src="<?php echo EBD_URL; ?>/images/flicker.png"></a>
            </div>
        </div>
    </div>
</div>