<div class="ebd-each-option">
    <input type="text" name="ebd_settings[custom_fields][{{data.field_key}}][option][]" placeholder="<?php _e('Option', 'everest-business-directory'); ?>"/>
    <input type="text" name="ebd_settings[custom_fields][{{data.field_key}}][value][]" placeholder="<?php _e('Value', 'everest-business-directory'); ?>"/>
    <span class="dashicons dashicons-no-alt ebd-remove-option"></span>
</div>