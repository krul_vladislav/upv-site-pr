<div class="ebd-each-custom-field">
    <div class="ebd-custom-field-head ebd-clearfix">
        <h4>{{data.field_label}}</h4>
        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>
        <span class="dashicons dashicons-trash ebd-remove-custom-field"></span>
    </div>
    <div class="ebd-custom-field-inner">
        <div class="ebd-field-wrap">
            <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[custom_fields][{{data.field_key}}][field_label]" value="{{data.field_label}}"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Options', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <div class="ebd-option-value-wrap ebd-sortable">
                    <?php include(EBD_PATH . 'inc/views/backend/js-templates/custom-fields/option-value.php'); ?>
                    <?php include(EBD_PATH . 'inc/views/backend/js-templates/custom-fields/option-value.php'); ?>
                </div>
                <input type="button" class="ebd-option-adder button-secondary" data-field-key="{{data.field_key}}" value="<?php _e('Add Option', 'everest-business-directory'); ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Frontend Display', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[custom_fields][{{data.field_key}}][frontend_display]" value="1"/>
            </div>
        </div>
    </div>
    <input type="hidden" name="ebd_settings[custom_fields][{{data.field_key}}][field_type]" value="{{data.field_type}}"/>
</div>
