<div class="ebd-ff-section" data-section="form-layout" style="display:none;">
    <div class="ebd-field-wrap">
        <label><?php _e('Form Layout', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[frontend_form][form_layout][template]" class="ebd-template-trigger">
                <?php
                $form_layout_template = (!empty($ebd_settings[ 'frontend_form' ][ 'form_layout' ][ 'template' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'form_layout' ][ 'template' ]) : 'template-1';
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <option value="template-<?php echo $i; ?>" <?php selected($form_layout_template, 'template-' . $i); ?>>Template <?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="ebd-template-previews">
                <?php
                for ( $i = 1; $i <= 5; $i++ ) {
                    ?>
                    <img src="<?php echo EBD_URL . 'images/previews/form/frontend-form-temp-' . $i . '.jpg'; ?>" <?php $this->display_none($form_layout_template, 'template-' . $i); ?> data-preview-id="template-<?php echo $i; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>