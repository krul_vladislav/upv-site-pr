<div class="ebd-each-frontend-field">
    <div class="ebd-frontend-field-head ebd-clearfix">
        <h4><?php _e('Directory Content', 'everest-business-directory'); ?></h4>
        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>

    </div>
    <div class="ebd-frontend-field-inner">
        <div class="ebd-field-wrap">
            <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][directory_content][show]" value="1" <?php echo (!empty($field_details[ 'show' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][directory_content][field_label]" value="<?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Required', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][directory_content][required]" value="1" <?php echo (!empty($field_details[ 'required' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Editor Type', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <?php $editor_type = (!empty($field_details[ 'editor_type' ])) ? esc_attr($field_details[ 'editor_type' ]) : 'rich'; ?>
                <select name="ebd_settings[frontend_form][frontend_fields][directory_content][editor_type]">
                    <option value="rich" <?php selected($editor_type, 'rich') ?>><?php _e('Rich Text Editor', 'everest-business-directory'); ?></option>
                    <option value="visual" <?php selected($editor_type, 'visual') ?>><?php _e('Visual Text Editor', 'everest-business-directory'); ?></option>
                    <option value="html" <?php selected($editor_type, 'html') ?>><?php _e('HTML Text Editor', 'everest-business-directory'); ?></option>
                    <option value="simple" <?php selected($editor_type, 'simple') ?>><?php _e('Simple Text Editor', 'everest-business-directory'); ?></option>
                </select>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Content Character Limit', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="number" min="1" name="ebd_settings[frontend_form][frontend_fields][directory_content][content_character_limit]" value="<?php echo (!empty($field_details[ 'content_character_limit' ])) ? intval($field_details[ 'content_character_limit' ]) : ''; ?>"/>
                <p class="description"><?php _e('Please leave blank if you don\'t want character limit.', 'everest-business-directory'); ?></p>
            </div>
        </div>
    </div>
    <input type="hidden" name="ebd_settings[frontend_form][field_order][]" value="directory_content"/>
</div>