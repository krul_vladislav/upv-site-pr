<div class="ebd-each-frontend-field">
    <div class="ebd-frontend-field-head ebd-clearfix">
        <h4><?php _e('Directory Social Information', 'everest-business-directory'); ?></h4>
        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>

    </div>
    <div class="ebd-frontend-field-inner">
        <div class="ebd-field-wrap">
            <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][social_information][show]" value="1" <?php echo (!empty($field_details[ 'show' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][social_information][field_label]" value="<?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-group-wrap">
            <div class="ebd-field-group-title"><?php _e('Social Fields', 'everest-business-directory'); ?></div>
            <?php
            $sub_fields_array = array( 'facebook' => array( 'label' => __('Facebook', 'everest-business-directory') ),
                'twitter' => array( 'label' => __('Twitter', 'everest-business-directory') ),
                'googleplus' => array( 'label' => __('Google+', 'everest-business-directory') ),
                'instagram' => array( 'label' => __('Instagram', 'everest-business-directory') ),
                'youtube' => array( 'label' => __('youtube', 'everest-business-directory') ),
                'linkedin' => array( 'label' => __('LinkedIn', 'everest-business-directory') ),
            );
            foreach ( $sub_fields_array as $sub_field_key => $sub_field_details ) {
                $field_label = (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ])) ? esc_attr($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ]) : '';
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo $sub_field_details[ 'label' ]; ?></label>
                    <div class="ebd-field">
                        <label><input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][social_information][fields][<?php echo $sub_field_key; ?>][show]" value="1"  <?php echo (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'show' ])) ? 'checked="checked"' : ''; ?>/><span><?php _e('Show', 'everest-business-directory'); ?></span></label>
                        <label><input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][social_information][fields][<?php echo $sub_field_key; ?>][required]" <?php echo (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'required' ])) ? 'checked="checked"' : ''; ?>/><span><?php _e('Required', 'everest-business-directory'); ?></span></label>
                        <input type="text" name="ebd_settings[frontend_form][frontend_fields][social_information][fields][<?php echo $sub_field_key; ?>][field_label]" placeholder="<?php _e('Field label', 'everest-business-directory'); ?>" value="<?php echo $field_label; ?>"/>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

    </div>

</div>