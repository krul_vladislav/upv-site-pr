<div class="ebd-each-frontend-field">
    <div class="ebd-frontend-field-head ebd-clearfix">
        <h4><?php _e('Directory Location Information', 'everest-business-directory'); ?></h4>
        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>
    </div>
    <div class="ebd-frontend-field-inner">
        <div class="ebd-field-wrap">
            <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][location_information][show]" value="1" <?php echo (!empty($field_details[ 'show' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][location_information][field_label]" value="<?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Show map', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][location_information][show_map]" value="1" <?php echo (!empty($field_details[ 'show_map' ])) ? 'checked="checked"' : ''; ?>/>
                <p class="description"><?php _e('Please enable it to display map where user can search their location.', 'everest-business-directory'); ?></p>
            </div>
        </div>
        <div class="ebd-field-group-wrap">
            <div class="ebd-field-group-title"><?php _e('Location Fields', 'everest-business-directory'); ?></div>
            <?php
            $sub_fields_array = array( 'latitude' => array( 'label' => __('Latitude', 'everest-business-directory') ),
                'longitude' => array( 'label' => __('Longitude', 'everest-business-directory') ),
                'address' => array( 'label' => __('Address', 'everest-business-directory') ),
                'city' => array( 'label' => __('City', 'everest-business-directory') ),
                'country' => array( 'label' => __('Country', 'everest-business-directory') ),
                'postal_code' => array( 'label' => __('Postal Code', 'everest-business-directory') ),
            );
            foreach ( $sub_fields_array as $sub_field_key => $sub_field_details ) {
                $field_label = (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ])) ? esc_attr($field_details[ 'fields' ][ $sub_field_key ][ 'field_label' ]) : '';
                ?>
                <div class="ebd-field-wrap">
                    <label><?php echo $sub_field_details[ 'label' ]; ?></label>
                    <div class="ebd-field">
                        <label><input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][location_information][fields][<?php echo $sub_field_key; ?>][show]" value="1"  <?php echo (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'show' ])) ? 'checked="checked"' : ''; ?>/><span><?php _e('Show', 'everest-business-directory'); ?></span></label>
                        <label><input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][location_information][fields][<?php echo $sub_field_key; ?>][required]" <?php echo (!empty($field_details[ 'fields' ][ $sub_field_key ][ 'required' ])) ? 'checked="checked"' : ''; ?>/><span><?php _e('Required', 'everest-business-directory'); ?></span></label>
                        <input type="text" name="ebd_settings[frontend_form][frontend_fields][location_information][fields][<?php echo $sub_field_key; ?>][field_label]" placeholder="<?php _e('Field label', 'everest-business-directory'); ?>" value="<?php echo $field_label; ?>"/>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

    </div>

</div>