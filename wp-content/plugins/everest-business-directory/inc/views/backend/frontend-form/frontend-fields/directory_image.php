<div class="ebd-each-frontend-field">
    <div class="ebd-frontend-field-head ebd-clearfix">
        <h4><?php _e('Directory Image', 'everest-business-directory'); ?></h4>
        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>

    </div>
    <div class="ebd-frontend-field-inner">
        <div class="ebd-field-wrap">
            <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][directory_image][show]" value="1" <?php echo (!empty($field_details[ 'show' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Required', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][directory_image][required]" value="1" <?php echo (!empty($field_details[ 'required' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][directory_image][field_label]" value="<?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Uploader Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][directory_image][uploader_label]" value="<?php echo (!empty($field_details[ 'uploader_label' ])) ? esc_attr($field_details[ 'uploader_label' ]) : ''; ?>" placeholder="<?php _e('Upload image', 'everest-business-directory'); ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Size Limit', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input min="1" type="number" name="ebd_settings[frontend_form][frontend_fields][directory_image][size_limit]" value="<?php echo (!empty($field_details[ 'size_limit' ])) ? esc_attr($field_details[ 'size_limit' ]) : ''; ?>"/>&nbsp;&nbsp;<i><?php _e('in MB', 'everest-business-directory'); ?></i>
                <p class="description"><?php _e('Please leave blank if you don\'t want to set size limit.', 'everest-business-directory'); ?></p>
            </div>
        </div>
    </div>
    <input type="hidden" name="ebd_settings[frontend_form][field_order][]" value="directory_image"/>
</div>