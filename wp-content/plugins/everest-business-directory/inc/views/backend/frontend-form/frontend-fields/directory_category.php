<div class="ebd-each-frontend-field">
    <div class="ebd-frontend-field-head ebd-clearfix">
        <h4><?php _e('Directory Category', 'everest-business-directory'); ?></h4>
        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>

    </div>
    <div class="ebd-frontend-field-inner">
        <div class="ebd-field-wrap">
            <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][directory_category][show]" value="1" <?php echo (!empty($field_details[ 'show' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Field Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][directory_category][field_label]" value="<?php echo (!empty($field_details[ 'field_label' ])) ? esc_attr($field_details[ 'field_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Required', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][directory_category][required]" value="1"  <?php echo (!empty($field_details[ 'required' ])) ? 'checked="checked"' : ''; ?>/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Display Type', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <?php $display_type = (!empty($field_details[ 'display_type' ])) ? esc_attr($field_details[ 'display_type' ]) : 'dropdown'; ?>
                <select name="ebd_settings[frontend_form][frontend_fields][directory_category][display_type]">
                    <option value="dropdown" <?php selected($display_type, 'dropdown'); ?>>Dropdown</option>
                    <option value="checkbox" <?php selected($display_type, 'checkbox'); ?>>Checkbox</option>
                </select>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Exclude Categories', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][frontend_fields][directory_category][exclude_categories]" value="<?php echo (!empty($field_details[ 'exclude_categories' ])) ? esc_attr($field_details[ 'exclude_categories' ]) : ''; ?>"/>
                <p class="description"><?php _e('Please enter the comma separated slugs of the categories that you want to exclude from the list.', 'everest-business-directory'); ?></p>
            </div>
        </div>
    </div>
    <input type="hidden" name="ebd_settings[frontend_form][field_order][]" value="directory_category"/>
</div>