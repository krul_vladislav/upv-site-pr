<?php
$custom_field_key = str_replace('_custom_field_', '', $field_key);
if ( !empty($ebd_settings[ 'custom_fields' ][ $custom_field_key ]) ) {
    ?>
    <div class="ebd-each-frontend-field">
        <div class="ebd-frontend-field-head ebd-clearfix">
            <h4><?php echo esc_attr($ebd_settings[ 'custom_fields' ][ $custom_field_key ][ 'field_label' ]); ?></h4>
            <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>
            <input type="hidden" name="ebd_settings[frontend_form][frontend_fields][<?php echo $field_key; ?>][field_label]" value="<?php echo esc_attr($ebd_settings[ 'custom_fields' ][ $custom_field_key ][ 'field_label' ]); ?>"/>
            <input type="hidden" name="ebd_settings[frontend_form][included_custom_fields][]" value="<?php echo esc_attr(str_replace('_custom_field_', '', $field_key)); ?>"/>
        </div>
        <div class="ebd-frontend-field-inner">
            <div class="ebd-field-wrap">
                <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
                <div class="ebd-field">
                    <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][<?php echo $field_key; ?>][show]" value="1" <?php echo (!empty($field_details[ 'show' ])) ? 'checked="checked"' : ''; ?>/>
                </div>
            </div>
            <div class="ebd-field-wrap">
                <label><?php _e('Required', 'everest-business-directory'); ?></label>
                <div class="ebd-field">
                    <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][<?php echo $field_key; ?>][required]" value="1"  <?php echo (!empty($field_details[ 'required' ])) ? 'checked="checked"' : ''; ?>/>
                </div>
            </div>
        </div>
    </div>
    <?php
}?>