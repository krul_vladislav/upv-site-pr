<div class="ebd-ff-section" data-section="general">
    <div class="ebd-field-wrap">
        <label><?php _e('Directory Status', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[frontend_form][general][directory_status]">
                <?php
                $directory_status = (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'directory_status' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'directory_status' ]) : 'draft';
                $post_statuses = get_post_statuses();
                foreach ( $post_statuses as $post_status => $post_status_label ) {
                    ?>
                    <option value="<?php echo $post_status; ?>" <?php selected($directory_status, $post_status) ?>><?php echo $post_status_label; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Field Required Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][field_required_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'field_required_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'field_required_message' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('File Extension Error Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][file_extension_error_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'file_extension_error_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'file_extension_error_message' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('File Upload Limit Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][file_upload_limit_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'file_upload_limit_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'file_upload_limit_message' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('File Size Limit Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][file_size_limit_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'file_size_limit_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'file_size_limit_message' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Success Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][success_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'success_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'success_message' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Form Error Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][form_error_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'form_error_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'form_error_message' ]) : ''; ?>">
        </div>
    </div>

    <div class="ebd-field-wrap">
        <label><?php _e('Redirect URL', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="url" name="ebd_settings[frontend_form][general][redirect_url]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'redirect_url' ])) ? esc_url($ebd_settings[ 'frontend_form' ][ 'general' ][ 'redirect_url' ]) : ''; ?>"/>
            <p class="description"><?php _e('Please enter the URL where you want to redirect after successful form submission. Leave blank if you don\'t want to redirect.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Check Login', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" name="ebd_settings[frontend_form][general][check_login]" value="1" <?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'check_login' ])) ? 'checked="checked"' : ''; ?> class="ebd-field-group-trigger" data-group-ref="check-login"/>
            <p class="description"><?php _e('Please check if you want to login to submit the directory.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Login Type', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[frontend_form][general][login_type]">
                <?php $login_type = (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ]) : 'login_message'; ?>
                <option value="login_message" <?php selected($login_type, 'login_message'); ?>><?php _e('Show login message', 'everest-business-directory'); ?></option>
                <option value="login_form" <?php selected($login_type, 'login_form'); ?>><?php _e('Show login form', 'everest-business-directory'); ?></option>
            </select>
        </div>
    </div>
    <div class="ebd-field-group" data-group-id="login_message" <?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ]) && $ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ] == 'login_message') ? '' : 'style="display:none;"'; ?>>
        <div class="ebd-field-wrap">
            <label><?php _e('Login Message', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <textarea name="ebd_settings[frontend_form][general][login_message]"><?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_message' ])) ? $ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_message' ] : ''; ?></textarea>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Login Link Text', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][general][login_link_text]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_link_text' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_link_text' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Login Link URL', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="url" name="ebd_settings[frontend_form][general][login_link_url]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_link_url' ])) ? esc_url($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_link_url' ]) : ''; ?>"/>
            </div>
        </div>
    </div>

    <div class="ebd-field-group" <?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ]) && $ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_type' ] == 'login_form') ? '' : 'style="display:none;"'; ?> data-group-id="login_form">
        <div class="ebd-field-wrap">
            <label><?php _e('Username Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][general][login_username_label]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_username_label' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_username_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Password Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][general][login_password_label]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_password_label' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_password_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Remember Me Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][general][login_remember_label]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_remember_label' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_remember_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Submit Label', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][general][login_submit_label]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_submit_label' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_submit_label' ]) : ''; ?>"/>
            </div>
        </div>
        <div class="ebd-field-wrap">
            <label><?php _e('Login error message', 'everest-business-directory'); ?></label>
            <div class="ebd-field">
                <input type="text" name="ebd_settings[frontend_form][general][login_error_message]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_error_message' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'login_error_message' ]) : ''; ?>"/>
            </div>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Assign Category', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[frontend_form][general][assign_category]">
                <option value=""><?php _e('Choose Category', 'everest-business-directory'); ?></option>
                <?php
                $directory_categories = get_terms('ebd-categories', array( 'orderby' => 'name', 'hide_empty' => false ));
                $assign_category = (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'assign_category' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'assign_category' ]) : '';
                foreach ( $directory_categories as $directory_category ) {
                    ?>
                    <option value="<?php echo $directory_category->slug; ?>" <?php selected($directory_category->slug, $assign_category); ?>><?php echo $directory_category->name; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Assign Author', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <select name="ebd_settings[frontend_form][general][assign_author]">
                <?php
                $assign_author = (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'assign_author' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'assign_author' ]) : '';
                $users = get_users();
                foreach ( $users as $user ) {
                    ?>
                    <option value="<?php echo $user->ID; ?>" <?php selected($user->ID, $assign_author); ?>><?php echo $user->data->user_nicename; ?></option>
                    <?php
                }
                ?>
            </select>
            <p class="description"><?php _e('Logged in user will be assigned as the author if any user submits the form by logging in.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Submit Button Label', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][general][submit_button_label]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'general' ][ 'submit_button_label' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'general' ][ 'submit_button_label' ]) : ''; ?>"/>
        </div>
    </div>

</div>