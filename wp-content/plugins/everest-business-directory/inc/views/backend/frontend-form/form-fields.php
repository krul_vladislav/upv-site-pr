<div class="ebd-ff-section ebd-sortable" data-section="form-fields" style="display:none;">
    <?php
    $frontend_fields = (!empty($ebd_settings[ 'frontend_form' ][ 'frontend_fields' ])) ? $ebd_settings[ 'frontend_form' ][ 'frontend_fields' ] : $this->get_frontend_fields();
    if ( !isset($frontend_fields[ 'directory_expiry_date' ]) ) {
        $frontend_fields[ 'directory_expiry_date' ] = array();
    }
    //   $this->print_array($frontend_fields);
    foreach ( $frontend_fields as $field_key => $field_details ) {
        if ( strpos($field_key, '_custom_field_') !== false ) {
            $frontend_field_file = 'custom-field.php';
        } else {

            $frontend_field_file = $field_key . '.php';
        }

        if ( file_exists(EBD_PATH . 'inc/views/backend/frontend-form/frontend-fields/' . $frontend_field_file) ) {
            include(EBD_PATH . 'inc/views/backend/frontend-form/frontend-fields/' . $frontend_field_file);
        }
    }

    $included_custom_fields = (!empty($ebd_settings[ 'frontend_form' ][ 'included_custom_fields' ])) ? $ebd_settings[ 'frontend_form' ][ 'included_custom_fields' ] : array();
    if ( !empty($ebd_settings[ 'custom_fields' ]) ) {
        foreach ( $ebd_settings[ 'custom_fields' ] as $custom_field_key => $custom_field_details ) {
            if ( !in_array($custom_field_key, $included_custom_fields) ) {
                ?>
                <div class="ebd-each-frontend-field">
                    <div class="ebd-frontend-field-head ebd-clearfix">
                        <h4><?php echo esc_attr($custom_field_details[ 'field_label' ]); ?></h4>
                        <span class="dashicons dashicons-arrow-down ebd-field-display-ref"></span>
                        <input type="hidden" name="ebd_settings[frontend_form][frontend_fields][_custom_field_<?php echo $custom_field_key; ?>][field_label]" value="<?php echo esc_attr($custom_field_details[ 'field_label' ]); ?>"/>
                        <input type="hidden" name="ebd_settings[frontend_form][included_custom_fields][]" value="<?php echo esc_attr($custom_field_key); ?>"/>
                    </div>
                    <div class="ebd-frontend-field-inner">
                        <div class="ebd-field-wrap">
                            <label><?php _e('Show on form', 'everest-business-directory'); ?></label>
                            <div class="ebd-field">
                                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][_custom_field_<?php echo $custom_field_key; ?>][show]" value="1"/>
                            </div>
                        </div>
                        <div class="ebd-field-wrap">
                            <label><?php _e('Required', 'everest-business-directory'); ?></label>
                            <div class="ebd-field">
                                <input type="checkbox" name="ebd_settings[frontend_form][frontend_fields][_custom_field_<?php echo $custom_field_key; ?>][required]" value="1"/>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }
    ?>
</div>