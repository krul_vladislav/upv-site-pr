<div class="ebd-ff-section" data-section="email" style="display:none;">
    <div class="ebd-field-wrap">
        <label><?php _e('Admin Notification', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" name="ebd_settings[frontend_form][email][admin_notification]" <?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification' ])) ? 'checked="checked"' : ''; ?>>
            <p class="description"><?php _e('Please check if you want to send notification to the site admin after each directory submission.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Admin Notification Subject', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][email][admin_notification_subject]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_subject' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_subject' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Admin Notification Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $default_admin_notification_message = __(sprintf('Hello There,

A new directory has been submitted via Everest Business Directory plugin in your %s website. Please find details below:

Directory Title: #directory_title

_____

To take action (approve/reject) - please go here:
#directory_admin_link

Thank you', esc_attr(get_bloginfo('name'))), 'everest-business-directory');
            $admin_notification_message = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_message' ])) ? $this->output_converting_br($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_notification_message' ]) : $default_admin_notification_message;
            ?>
            <textarea name="ebd_settings[frontend_form][email][admin_notification_message]"><?php echo $admin_notification_message; ?></textarea>
            <p class="description"><?php _e('Please use #directory_title, #directory_admin_link to replace the title and directory admin link in the admin notification message.', 'everest-business-directory'); ?></p>

        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Admin Email', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][email][admin_email]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_email' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'admin_email' ]) : ''; ?>">
            <p class="description"><?php _e('Please enter the email in which you want to receive the admin notification.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Directory Publish Notification', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" name="ebd_settings[frontend_form][email][directory_publish_notification]" <?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'directory_publish_notification' ])) ? 'checked="checked"' : ''; ?>>
            <p class="description"><?php _e('Please check if you want to send notification to the directory submitter.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Publish Notification Subject', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][email][publish_notification_subject]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_subject' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_subject' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Publish Notification Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $publish_default_notification_message = __(sprintf('Hello There,

                                                                Your directory has been published in our %s website. Please find details below:

                                                                Directory Title: #directory_title
                                                                Directory Link: #directory_link

                                                                Thank you', esc_attr(get_bloginfo('name'))), 'everest-business-directory');
            $publish_notification_message = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_message' ])) ? $this->output_converting_br($ebd_settings[ 'frontend_form' ][ 'email' ][ 'publish_notification_message' ]) : $publish_default_notification_message;
            ?>
            <textarea name="ebd_settings[frontend_form][email][publish_notification_message]"><?php echo $publish_notification_message; ?></textarea>
            <p class="description"><?php _e('Please use #directory_title, #directory_link to replace the title and directory link in the publish notification message.', 'everest-business-directory'); ?></p>

        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Directory Rejection Notification', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="checkbox" name="ebd_settings[frontend_form][email][directory_reject_notification]" <?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'directory_reject_notification' ])) ? 'checked="checked"' : ''; ?>>
            <p class="description"><?php _e('Please check if you want to send rejection notification to the directory submitter.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Reject Notification Subject', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][email][reject_notification_subject]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_subject' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_subject' ]) : ''; ?>">
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Rejection Notification Message', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <?php
            $reject_default_notification_message = __(sprintf('Hello There,

                                                                Your directory has been rejected in our %s website. Please find details below:

                                                                Directory Title: #directory_title

                                                                Thank you', esc_attr(get_bloginfo('name'))), 'everest-business-directory');
            $reject_notification_message = (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_message' ])) ? $this->output_converting_br($ebd_settings[ 'frontend_form' ][ 'email' ][ 'reject_notification_message' ]) : $reject_default_notification_message;
            ?>
            <textarea name="ebd_settings[frontend_form][email][reject_notification_message]"><?php echo $reject_notification_message; ?></textarea>
            <p class="description"><?php _e('Please use #directory_title, #directory_link to replace the title and directory link in the publish notification message.', 'everest-business-directory'); ?></p>

        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Email From Name', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][email][from_name]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_name' ]) : ''; ?>">
            <p class="description"><?php _e('Please enter the from name which will be used to send the notification email. Please enter some value which won\'t resember real person\'s real name such as No Reply.', 'everest-business-directory'); ?></p>
        </div>
    </div>
    <div class="ebd-field-wrap">
        <label><?php _e('Email From Email', 'everest-business-directory'); ?></label>
        <div class="ebd-field">
            <input type="text" name="ebd_settings[frontend_form][email][from_email]" value="<?php echo (!empty($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ])) ? esc_attr($ebd_settings[ 'frontend_form' ][ 'email' ][ 'from_email' ]) : ''; ?>">
            <p class="description"><?php _e(sprintf('Please enter the from name which will be used to send the notification email. Please enter some value which won\'t resember real person\'s real name such as noreply@yoursiteurl.'), 'everest-business-directory'); ?></p>
        </div>
    </div>
</div>