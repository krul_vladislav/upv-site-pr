jQuery(document).ready(function ($) {

    function reverse_geocoding() {

        var lat = parseFloat($('#ebd-latitude').val());
        var lng = parseFloat($('#ebd-longitude').val());
        var latlng = new google.maps.LatLng(lat, lng);

        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                //    console.log('Reverse Geocoding:');
                //     console.dir(results);

                $('#ebd-address').val(results[0].formatted_address);

                // address components: recupero di tutti gli elementi
                //   console.log('Address components:');
                //  console.dir(results[0].address_components);

                var temp = [];
                for (var i = 0; i < results[0].address_components.length; i++) {

                    //    console.log(results[0].address_components[i]);

                    if ($.inArray(results[0].address_components[i].long_name, temp) === -1) { // evita potenziali duplicati
                        temp.push(results[0].address_components[i].long_name);
                        if (results[0].address_components[i].types[0] == 'locality') {
                            $('#ebd-city').val(results[0].address_components[i].long_name);
                        }
                        if (results[0].address_components[i].types[0] == 'country') {
                            $('#ebd-country').val(results[0].address_components[i].long_name);
                        }
                        if (results[0].address_components[i].types[0] == 'postal_code') {
                            $('#ebd-postal-code').val(results[0].address_components[i].long_name);
                        }
                    }
                    if ($.inArray(results[0].address_components[i].short_name, temp) === -1) { // evita potenziali duplicati
                        temp.push(results[0].address_components[i].short_name);
                    }
                }

                //    console.log('Address components (elaborati):');
                //  console.dir(temp);

            } else {
                alert("Geocoder failed due to: " + status);
            }
        });
    }
    var mediaUploader;
    $('.ebd-gallery-add-trigger').click(function () {
        // If the uploader object has already been created, reopen the dialog
        if (mediaUploader) {
            mediaUploader.open();
            return;
        }
        // Extend the wp.media object
        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Images',
            button: {
                text: 'Insert into Gallery'
            }, multiple: true});

        // When a file is selected, grab the URL and perform necessary task to display as the gallery items
        mediaUploader.on('select', function () {
            attachment = mediaUploader.state().get('selection').toJSON();


            var data = {
                action: 'ebd_gallery_images_action',
                _wpnonce: ebd_backend_js_obj.ajax_nonce,
                attachments: attachment
            };

            $.ajax({
                type: 'post',
                url: ebd_backend_js_obj.ajax_url,
                data: data,
                beforeSend: function (xhr) {
                    $('.ebd-ajax-loader').show();
                },
                success: function (res) {
                    $('.ebd-ajax-loader').hide();
                    $('.ebd-gallery-items-wrap').append(res);
                }
            });


            //$('#image-url').val(attachment.url);
        });
        // Open the uploader dialog
        mediaUploader.open();
    });
    function initMap() {
        var map = new google.maps.Map(document.getElementById('ebd-map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
        });

        var input = document.getElementById('ebd-pac-input');

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('ebd-infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            // Set the position of the marker using the place ID and location.
            marker.setPlace({
                placeId: place.place_id,
                location: place.geometry.location
            });
            marker.setVisible(true);

            infowindowContent.children['ebd-place-name'].textContent = place.name;
            infowindowContent.children['ebd-place-address'].textContent =
                    place.formatted_address;
            infowindow.open(map, marker);
            var filtered_array = place.address_components.filter(function (address_component) {
                return address_component.types.includes("country");
            });
            var country = filtered_array.length ? filtered_array[0].long_name : "";
            var filtered_array = place.address_components.filter(function (address_component) {
                return address_component.types.includes("postal_code");
            });

            var postal_code = filtered_array.length ? filtered_array[0].long_name : "";
            var filtered_array = place.address_components.filter(function (address_component) {
                return address_component.types.includes("locality");
            });

            var city = filtered_array.length ? filtered_array[0].long_name : "";
            $('#ebd-latitude').val(place.geometry.location.lat());
            $('#ebd-longitude').val(place.geometry.location.lng());
            $('#ebd-address').val(place.formatted_address);
            $('#ebd-country').val(country);
            $('#ebd-city').val(city);
            $('#ebd-postal-code').val(postal_code);
        });
    }
    if ($('#ebd-map').length > 0) {
        initMap();
    }
    $('#ebd-pac-input').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    $('body').on('click', '.ebd-remove-gallery-item', function () {
        $(this).parent().remove();
    });

    $('.ebd-custom-field-add-trigger').click(function () {
        $(this).parent().find('.ebd-error').hide();
        var label = $('#ebd-custom-field-label').val();
        var meta_key = $('#ebd-custom-field-key').val();
        var field_type = $('#ebd-custom-field-type').val();
        if (label == '' || meta_key == '') {
            $(this).parent().find('.ebd-error').show();
        } else {
            var data = {field_label: label, field_key: meta_key, field_type: field_type};
            switch (field_type) {
                case 'textfield':
                case 'textarea':
                case 'datepicker':
                case 'url':
                case 'html':
                    var field_template = wp.template('ebd-textfield');
                    break;
                case 'radio':
                case 'checkbox':
                case 'select':
                    var field_template = wp.template('ebd-radio');
                    break;


            }
            $('.ebd-custom-fields-list').append(field_template(data));
            $('#ebd-custom-field-label,#ebd-custom-field-key').val('');
        }
    });

    $('#ebd-custom-field-label,#ebd-custom-field-key').keyup(function () {
        $(this).closest('.ebd-custom-fields-adder').find('.ebd-error').hide();
    });

    $('body').on('click', '.ebd-custom-field-head h4', function () {
        var selector = $(this);

        $(this).closest('.ebd-each-custom-field').find('.ebd-custom-field-inner').slideToggle('500', function () {
            if (selector.closest('.ebd-custom-field-head').find('.ebd-field-display-ref').hasClass('dashicons-arrow-down')) {
                selector.closest('.ebd-custom-field-head').find('.ebd-field-display-ref').removeClass('dashicons-arrow-down').addClass('dashicons-arrow-up');
            } else {
                selector.closest('.ebd-custom-field-head').find('.ebd-field-display-ref').removeClass('dashicons-arrow-up').addClass('dashicons-arrow-down');
            }
        });
    });
    $('body').on('click', '.ebd-frontend-field-head h4', function () {
        var selector = $(this);

        $(this).closest('.ebd-each-frontend-field').find('.ebd-frontend-field-inner').slideToggle('500', function () {
            if (selector.closest('.ebd-frontend-field-head').find('.ebd-field-display-ref').hasClass('dashicons-arrow-down')) {
                selector.closest('.ebd-frontend-field-head').find('.ebd-field-display-ref').removeClass('dashicons-arrow-down').addClass('dashicons-arrow-up');
            } else {
                selector.closest('.ebd-frontend-field-head').find('.ebd-field-display-ref').removeClass('dashicons-arrow-up').addClass('dashicons-arrow-down');
            }
        });
    });

    $('body').on('click', '.ebd-option-adder', function () {
        var field_key = $(this).data('field-key');
        var field_template = wp.template('ebd-option-value');
        var data = {field_key: field_key};
        $(this).closest('.ebd-field').find('.ebd-option-value-wrap').append(field_template(data));
    });

    $('body').on('click', '.ebd-remove-custom-field', function () {
        $(this).closest('.ebd-each-custom-field').fadeOut(500, function () {
            $(this).remove();
        });
    });
    $('body').on('click', '.ebd-remove-frontend-field', function () {
        $(this).closest('.ebd-each-frontend-field').fadeOut(500, function () {
            $(this).remove();
        });
    });
    $('body').on('click', '.ebd-remove-option', function () {
        $(this).closest('.ebd-each-option').fadeOut(500, function () {
            $(this).remove();
        })
    });

    $('body').on('click', '.ebd-field-display-ref', function () {
        $(this).parent().find('h4').click();
    });

    $('.ebd-sortable').sortable();

    $('.ebd-ff-trigger').click(function () {
        var section = $(this).data('section-ref');
        $('.ebd-ff-section').hide();
        $('.ebd-ff-section[data-section="' + section + '"]').show();
        $('.ebd-ff-trigger').removeClass('ebd-active-ff-trigger');
        $(this).addClass('ebd-active-ff-trigger');
    });

    $('.ebd-field-group-trigger').click(function () {
        if ($(this).attr('type') == 'checkbox') {
            var group_ref = $(this).attr('data-group-ref');
            if ($(this).is(':checked')) {
                $('.ebd-field-group[data-group-id="' + group_ref + '"]').show();
            } else {
                $('.ebd-field-group[data-group-id="' + group_ref + '"]').hide();
            }
        }
    });
    /**
     * Google Map initialization
     *
     * @since 1.0.0
     */
    if ($('#ebd-map-frame').length > 0) {
        var geocoder;
        var map;
        var base_address = $('#ebd-map-frame').data('base-address');
        var zoom_level = $('#ebd-map-frame').data('zoom-level');

        $('.ebd-plot-map').click(function () {
            var address = $('.ebd-map-address-input').val();
            address = (address == '') ? base_address : address;

            geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //   console.log('geocoder results:');
                    //   console.dir(results);

                    var mapOptions = {
                        zoom: zoom_level,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.SMALL
                        },
                        //streetViewControl: false,
                        center: results[0].geometry.location
                    }

                    map = new google.maps.Map(document.getElementById('ebd-map-frame'), mapOptions);
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $('#ebd-latitude').val(latitude);
                    $('#ebd-longitude').val(longitude);


                    //map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        draggable: true,
                        animation: google.maps.Animation.DROP
                    });
                    reverse_geocoding();

                    google.maps.event.addListener(marker, 'dragend', function () {

                        marker.setAnimation(google.maps.Animation.DROP);

                        var marker_pos = marker.getPosition();

                        $('#ebd-latitude').val(marker_pos.lat());
                        $('#ebd-longitude').val(marker_pos.lng());
                        reverse_geocoding();
                    });

                } else {
                    alert('Invalid address (' + status + ')');
                }
            });

        })
                .trigger('click');
    }
    var dateToday = new Date();
    $('.ebd-datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: dateToday
    });

    if ($('.ebd-datepicker-1').length > 0) {
        $('.editinline').click(function () {
            setTimeout(function () {
                $('.ebd-datepicker-1').each(function () {
                    $(this).datepicker("destroy");
                    $(this).datepicker({
                        dateFormat: 'yy-mm-dd'
                    });


                });
            }, 500);

        })
    }

    $('.ebd-template-trigger').change(function () {
        var template = $(this).val();
        $(this).closest('.ebd-field').find('.ebd-template-previews img').hide();
        $(this).closest('.ebd-field').find('.ebd-template-previews img[data-preview-id="' + template + '"]').show();
    });

    $('.ebd-directory-detail-preview-trigger').change(function () {
        var template = $(this).val();
        $('.ebd-directory-previews img').hide();
        $('.ebd-directory-previews img[data-preview-id="' + template + '"]').show();
    });

    $('#ebd-restore-markup').click(function (e) {
        //   e.preventDefault();
        $('#ebd-single-markup').val($('#ebd-single-default-markup').val());
        $('#ebd-archive-markup').val($('#ebd-single-default-markup').val());
        $('#ebd-sidebar-markup').val($('#ebd-sidebar-default-markup').val());

    });
});