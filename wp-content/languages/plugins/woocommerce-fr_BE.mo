��                        @        N  &   Z     �     �     �     �     �     �     �     �     �                    '     3     <     M     \     u     �     �     �     �     �  	   �     �  #   �  
   �  �        �     �  +        7  
   G     R     k  1   p     �     �     �     �  
   �     �     �     �  	   
          #     9     U  
   n     y          �     �     �     �  '   �     �   %s has been added to your cart. %s have been added to your cart. %s removed. Apartment, suite, unit etc. (optional) Cart totals Category House number and street name Item New passwords do not match. Order notes Price Proceed to checkout Product Product ID. Product IDs Product SKU. Product URL Quantity Related products Return to shop Select an option&hellip; Select options Subtotal Total Total: Undo? Update cart View cart Your cart is currently empty. Your current password is incorrect. Your order PO-Revision-Date: 2019-06-26 12:43+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Loco https://localise.biz/
Language: fr_BE
Project-Id-Version: Plugins - WooCommerce - Stable (latest release)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-06-24 11:32+0000
Last-Translator: Admin <petrusha.dv@gmail.com>
Language-Team: Français de Belgique
X-Loco-Version: 2.3.0; wp-4.9.10 a été ajouté à votre panier  %s retiré. appartement, suite, unité, etc (optionnel) Total du panier Catégorie nom et numéro de la rue Type  Les nouveaux mots de passe ne correspondent pas. Détail de la commande
 Prix Confirmer mon achat Produit Produit ID Produit IDs Produit SKU Produit URL Quantité Produits liés Retour à la boutique sélectionner une option... Sélectionner une option Sous-total Total Total: Annuler? Mettre à jour le panier Voir le panier Votre panier est vide. Votre mot de passe actuel est incorrect Votre commande 